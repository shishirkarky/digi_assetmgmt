<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/11/20
  Time: 8:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/asset" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">BILL DETAILS</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Bill Number</label>
                            <input type="text" name="billNumber" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Bill Date(B.S)</label>
                            <input type="text" name="billDateNp" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Bill Date(A.D.)</label>
                            <input type="text" name="billDateEn" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Company</label>
                            <select name="refCompanyId" class="form-control">
                                <option value="">Select Company</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Bill Image</label>
                            <input type="file" name="file" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<asset:formButtonSection/>
<ui:loadTableBtnSection/>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">Bill Details</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none"
               id="dataTable" style="width: 100%">
            <thead>
            <tr>
                <th>Bill Number</th>
                <th>Bill Date(B.S.)</th>
                <th>Bill Date(A.D.)</th>
                <th>Company Name</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>
    </div>
</section>
<ui:footer/>
<script>
    $(document).ready(function () {
        getCompanyByType('COMPANY');
    });
    /**
     * Save Bill
     */
    $("#saveFormBtn").click(function () {
        let formData = {
            "billDateNp": $("[name='billDateNp']").val(),
            "billDateEn": $("[name='billDateEn']").val(),
            "billNumber": $("[name='billNumber']").val(),
            "refCompanyId": $("[name='refCompanyId']").val(),
        }
        let data=new FormData();
        data.append("file", $("input[name='file']")[0].files[0]);
        data.append("billJson", JSON.stringify(formData));
        console.log(data);
        let url = "${pageContext.request.contextPath }/bill";
        ajaxMultipart(url, data);
    });
    /**
     * Load Bill
     */
    $("#loadTableBtn").click(function () {
        loadTable();
    });

    function loadTable() {
        let url = "${pageContext.request.contextPath }/bill/bills";
        $.get(url, function (data, status) {
            let json = data.data;
            let table = $('#dataTable').DataTable({
                "data": json,
                "columns": [
                    {data: "billNumber"},
                    {data: "billDateNp"},
                    {data: "billDateEn"},
                    {data: "refCompanyId"},
                    {
                        "data": "Action",
                        "orderable": false,
                        "searchable": false,
                        "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                            let actionBtn = '<div class="btn-group"><button type="button" class="mb-xs mt-xs mr-xs btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-h" aria-hidden="true"></i> <span class="caret"></span></button><ul class="dropdown-menu" role="menu">' +
                                '<li><a class="modal-with-form" onclick="edit(' + row.id + ')"><i class="fa fa-search"></i> BILL  IMAGE </a></li>' +
                                '</ul></div>';
                            return actionBtn;
                        }
                    }

                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                console.log("Bill data not found");
                $('#dataTable').DataTable();
            });
    }
</script>