<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/2/20
  Time: 7:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">Bill Details</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none"
               id="dataTable" style="width: 100%">
            <thead>
            <tr>
                <th>Bill Number</th>
                <th>Bill Date(B.S.)</th>
                <th>Bill Date(A.D.)</th>
                <th>Company Name</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>
    </div>
</section>
<ui:footer/>
<script>
    $(document).ready(function () {
        loadTable();
    });

    function loadTable() {
        let url = "${pageContext.request.contextPath }/bill/bills";
        $.get(url, function (data, status) {
            let json = data.data;
            let table = $('#dataTable').DataTable({
                "data": json,
                "columns": [
                    {data: "billNumber"},
                    {data: "billDateNp"},
                    {data: "billDateEn"},
                    {data: "refCompanyId"},
                    {
                        "data": "Action",
                        "orderable": false,
                        "searchable": false,
                        "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                            let actionBtn = '<div class="btn-group"><button type="button" class="mb-xs mt-xs mr-xs btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-h" aria-hidden="true"></i> <span class="caret"></span></button><ul class="dropdown-menu" role="menu">' +
                                '<li><a class="modal-with-form" onclick="edit(' + row.id + ')"><i class="fa fa-search"></i> BILL  IMAGE </a></li>' +
                                '</ul></div>';
                            return actionBtn;
                        }
                    }

                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                console.log("Bill data not found");
                $('#dataTable').DataTable();
            });
    }
</script>