<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/4/20
  Time: 10:49 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">Pending Transfers</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none"
               id="dataTable" style="width: 100%">
            <thead>
            <tr>
                <th>Item Code</th>
                <th>From Branch</th>
                <th>From Staff</th>
                <th>To Branch</th>
                <th>To Staff</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>
    </div>
</section>
<ui:footer/>
<script>
    $(document).ready(function () {
        loadTable();
    });

    function loadTable() {
        let url = "${pageContext.request.contextPath }/transfer-issue/transfers/pending";
        $.get(url, function (data, status) {
            let json = data.data;
            let table = $('#dataTable').DataTable({
                "data": json,
                "columns": [
                    {data: "itemCode"},
                    {data: "fromBranchCode"},
                    {data: "fromStaffCode"},
                    {data: "toBranchCode"},
                    {data: "toStaffCode"},
                    {data: "status"},
                    {
                        "data": "Action",
                        "orderable": false,
                        "searchable": false,
                        "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                            let approveTransfer = "approveTransfer('" + row.itemCode + "')";
                            let disapproveTransfer = "disapproveTransfer('" + row.itemCode + "')";
                            let approveBtn = '<a class="btn btn-sm btn-success" data-toggle="tooltip" title="Approve Transfer" onclick="' + approveTransfer + '"><i class="fa fa-check"></i> </a>    ';
                            let disapproveBtn = '<a class="btn btn-sm btn-danger" data-toggle="tooltip" title="Decline Transfer" onclick="' + disapproveTransfer + '"><i class="fa fa-times"></i> </a>    ';
                            let detailBtn = '<a class="btn btn-sm btn-info" data-toggle="tooltip" title="Details" href="${pageContext.request.contextPath }/assets/detail?itemCode=' + row.itemCode + '"><i class="fa fa-info-circle"></i> </a>';
                            return approveBtn + disapproveBtn + detailBtn;
                        }
                    }
                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                alert('No Data Found!');
                $('#dataTable').DataTable();
            });
    }

    function approveTransfer(itemCode) {
        let url = "${pageContext.request.contextPath }/transfer-issue/transfers/items/" + itemCode + "/approves";
        approveDisapproveTransfer(url);
    }

    function disapproveTransfer(itemCode) {
        let url = "${pageContext.request.contextPath }/transfer-issue/transfers/items/" + itemCode + "/disapproves";
        approveDisapproveTransfer(url);
    }

    function approveDisapproveTransfer(url) {
        $.ajax({
            url: url,
            method: "PATCH",
            contentType: "application/json",
            beforeSend: function () {
                return confirm("Are you sure?");
            },
            success: function (data) {
                toastNotification('Success', data.message, 'success');
                $('#dataTable').DataTable().destroy();
                loadTable();
                $('#dataTable').DataTable().draw();
            },
            error: function (err) {
                triggerOnError(err);
            }
        });
    }
</script>