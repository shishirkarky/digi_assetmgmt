<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/4/20
  Time: 10:49 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">Transfer Issue Log</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none"
               id="dataTable" style="width: 100%">
            <thead>
            <tr>
                <th>Status</th>
                <th>Item Code</th>
                <th>Transaction DateTime</th>
                <th>From Branch</th>
                <th>From Staff</th>
                <th>To Branch</th>
                <th>To Staff</th>
            </tr>
            </thead>
        </table>
    </div>
</section>
<ui:footer/>
<script>
    $(document).ready(function () {
        loadTable();
    });

    function loadTable() {
        let url = "${pageContext.request.contextPath }/transfer-issue";
        $.get(url, function (data, status) {
            let json = data.data;
            let table = $('#dataTable').DataTable({
                "data": json,
                "columns": [
                    {data: "status"},
                    {data: "itemCode"},
                    {data: "createdAt"},
                    {data: "fromBranchCode"},
                    {data: "fromStaffCode"},
                    {data: "toBranchCode"},
                    {data: "toStaffCode"}
                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                alert('No Data Found!');
                $('#dataTable').DataTable();
            });
    }
</script>