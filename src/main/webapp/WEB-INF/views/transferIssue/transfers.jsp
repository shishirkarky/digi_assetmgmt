<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/8/20
  Time: 6:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/asset" %>
<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel" id="transferIssuePanel">
            <header class="panel-heading">
                <h2 class="panel-title">Transfer Details</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">To Branch</label>
                            <select name="toBranchCode" id="toBranchCode" class="form-control isnumber">
                                <option value="" selected>
                                    Select Branch
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">To Staff Code</label>
                            <select name="toStaffCode" id="toStaffCode" class="form-control">
                                <option value="">
                                    Select Staff
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Select assets to transfer</h2>
            </header>
            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none"
                       id="dataTable" style="width: 100%">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Item Code</th>
                        <th>Sub-item Code</th>
                        <th>Staff Code</th>
                        <th>Item Status</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </section>
    </div>
</div>
<asset:formButtonSection/>
<ui:footer/>
<script>
    $(document).ready(function () {
        loadTable();
        fetchOfficeBranchInSelect('toBranchCode');
        $('#toBranchCode').select2();
    });
    $("#toBranchCode").change(function () {
        fetchStaffInSelectByBranchCode('toStaffCode', $(this).val());
        $('#toStaffCode').select2();
    });
    function loadTable() {
        let url = "${pageContext.request.contextPath }/assets/subassets";
        $.get(url, function (data, status) {
            let json = data.data;
            let table = $('#dataTable').DataTable({
                "data": json,
                "paging": false,
                "scrollY": 500,
                "columns": [
                    {
                        "data": "Action",
                        "orderable": false,
                        "searchable": false,
                        "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                            let a = '<div class="checkbox"><label><input name="itemCode" type="checkbox" value="' + row.subItemCode + '"></label></div>';
                            return a;
                        }
                    },
                    {data: "itemCode"},
                    {data: "subItemCode"},
                    {data: "staffCode"},
                    {data: "currentAssetCondition"}
                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                alert('No Data Found!');
                $('#dataTable').DataTable();
            });
    }

    $('#saveFormBtn').click(function () {
        let json = {
            toStaffCode:$("#toStaffCode").val(),
            toBranchCode:$("#toBranchCode").val()
        }
        let itemCodes = [];
        $('input[name="itemCode"]:checked').each(function () {
            itemCodes.push(this.value);
        });
        json['itemCodes'] = itemCodes;
        console.log(json);
        let url = "${pageContext.request.contextPath}/transfer-issue/transfers";
        ajaxPut(url, json);
    })
</script>
