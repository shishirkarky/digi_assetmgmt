<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<div class="row">
    <div class="col-md-12">
        <div class="tabs">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#company" data-toggle="tab">Company</a></li>
                <li><a href="#fundSource" data-toggle="tab"> Fund Source</a></li>
                <li><a href="#receivedSource" data-toggle="tab"> Received Source </a></li>
                <li><a href="#assetGroup" data-toggle="tab"> Group </a></li>
                <li><a href="#assetCondition" data-toggle="tab"> Asset Condition </a></li>
                <li><a href="#branch" data-toggle="tab"> Branch </a></li>
                <li><a href="#officeUnit" data-toggle="tab"> Office Unit </a></li>
            </ul>
            <div class="tab-content">
                <div id="company" class="tab-pane active">
                    <jsp:include page="company.jsp"/>
                </div>
                <div id="fundSource" class="tab-pane">
                    <jsp:include page="fundSource.jsp"/>
                </div>
                <div id="receivedSource" class="tab-pane">
                    <jsp:include page="receivedSource.jsp"/>
                </div>
                <div id="assetGroup" class="tab-pane">
                    <jsp:include page="assetGroup.jsp"/>
                </div>
                <div id="assetCondition" class="tab-pane">
                    <jsp:include page="assetCondition.jsp"/>
                </div>
                <div id="branch" class="tab-pane">
                    <jsp:include page="branch.jsp"/>
                </div>
                <div id="officeUnit" class="tab-pane">
                    <jsp:include page="officeUnit.jsp"/>
                </div>
            </div>
        </div>
    </div>
</div>
<ui:footer/>
<script>
    /**
     * Save Company
     */
    $("#saveCompanyBtn").click(function () {
        let formData = {
            "vatPan": $("[name='vatPan']").val(),
            "name": $("[name='name']").val(),
            "address": $("[name='address']").val(),
            "contactNumber": $("[name='contactNumber']").val(),
            "email": $("[name='email']").val(),
            "contactPerson": $("[name='contactPerson']").val(),
            "companyType": $("[name='companyType']").val()
        }
        console.log(formData);
        let url = "${pageContext.request.contextPath }/settings/company";
        ajaxPost(url, formData);
    });
    /**
     * Company DataTable
     */
    $("#loadCompanyDataBtn").click(function () {
        let url = "${pageContext.request.contextPath }/settings/company/get-all";
        $.get(url, function (data, status) {
            let json = data.data;

            let table = $("#companyTable").DataTable({
                "data": json,
                "columns": [
                    {data: "vatPan"},
                    {data: "name"},
                    {data: "address"},
                    {data: "contactNumber"},
                    {data: "email"},
                    {data: "contactPerson"},
                    {data: "companyType"}
                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                $("#companyTable").empty();
            });
    });

    /**
     * Save Fund Source
     */
    $("#saveFundSourceBtn").click(function () {
        let formData = {
                "name": $("[name='fundSourceName']").val()
        }
        console.log(formData);
        let url = "${pageContext.request.contextPath }/settings/fund-source";
        ajaxPost(url, formData);
    });
    /**
     * Fund Source DataTable
     */
    $("#loadFundSourceBtn").click(function () {
        let url = "${pageContext.request.contextPath }/settings/fund-source/get-all";
        $.get(url, function (data, status) {
            let json = data.data;

            let table = $("#fundSourceTable").DataTable({
                "data": json,
                "columns": [
                    {data: "name"},
                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                $("#fundSourceTable").empty();
            });
    });

    /**
     * Received Fund Source
     */
    $("#saveReceivedSourceBtn").click(function () {
        let formData = {
            "name": $("[name='receivedSourceName']").val()
        }
        console.log(formData);
        let url = "${pageContext.request.contextPath }/settings/received-source";
        ajaxPost(url, formData);
    });
    /**
     * Received Source DataTable
     */
    $("#loadReceivedSourceBtn").click(function () {
        let url = "${pageContext.request.contextPath }/settings/received-source/get-all";
        $.get(url, function (data, status) {
            let json = data.data;

            let table = $("#receivedSourceTable").DataTable({
                "data": json,
                "columns": [
                    {data: "name"},
                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                $("#fundSourceTable").empty();
            });
    });
    /**
     * Save Group
     */
    $("#saveAssetGroupBtn").click(function () {
        let formData = {
            "name": $("[name='assetGroupName']").val(),
            "code": $("[name='assetGroupCode']").val()
        }
        console.log(formData);
        let url = "${pageContext.request.contextPath }/settings/group";
        ajaxPost(url, formData);
    });
    /**
     * Group DataTable
     */
    $("#loadAssetGroupBtn").click(function () {
        let url = "${pageContext.request.contextPath }/settings/group/groups";
        $.get(url, function (data, status) {
            let json = data.data;

            let table = $("#assetGroupTable").DataTable({
                "data": json,
                "columns": [
                    {data: "name"},
                    {data: "code"},
                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                $("#assetGroupTable").empty();
            });
    });
    /**
     * Save Branch
     */
    $("#saveBranchBtn").click(function () {
        let formData = {
            "branchCode": $("[name='branchCode']").val(),
            "hoCode": $("[name='hoCode']").val(),
            "roCode": $("[name='roCode']").val(),
            "name": $("[name='branchName']").val(),
            "address": $("[name='branchAddress']").val(),
            "officeLevel": $("[name='officeLevel']").val()
        }
        let url = "${pageContext.request.contextPath }/office";
        ajaxPost(url, formData);
    });
    /**
     * Branch DataTable
     */
    $("#loadBranchBtn").click(function () {
        let url = "${pageContext.request.contextPath }/office";
        $.get(url, function (data, status) {
            let json = data.data;

            let table = $("#branchTable").DataTable({
                "data": json,
                "columns": [
                    {data: "hoCode",columnDefault:""},
                    {data: "roCode",columnDefault:""},
                    {data: "branchCode",columnDefault:""},
                    {data: "officeLevel",columnDefault:""},
                    {data: "name",columnDefault:""},
                    {data: "address",columnDefault:""},
                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                $("#branchTable").empty();
            });
    });
    /**
     * Save Branch
     */
    $("#saveOfficeUnitBtn").click(function () {
        let formData = getJsonByBlockId('#officeUnitPanel');
        let url = "${pageContext.request.contextPath }/office-unit";
        ajaxPost(url, formData);
    });
    /**
     * Branch DataTable
     */
    $("#loadOfficeUnitBtn").click(function () {
        let url = "${pageContext.request.contextPath }/office-unit";
        $.get(url, function (data, status) {
            let json = data.data;
            let table = $("#officeUnitTable").DataTable({
                "data": json,
                "columns": [
                    {data: "refBranchCode"},
                    {data: "code"},
                    {data: "roomNo"},
                    {data: "name"},
                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                $("#officeUnitTable").empty();
            });
    });
    /**
     * Save Group
     */
    $("#saveAssetConditionbtn").click(function () {
        let formData = {
            "name": $("[name='assetConditionName']").val()
        }
        console.log(formData);
        let url = "${pageContext.request.contextPath }/settings/asset-condition";
        ajaxPost(url, formData);
    });
    /**
     * Asset Condition DataTable
     */
    $("#loadAssetConditionBtn").click(function () {
        let url = "${pageContext.request.contextPath }/settings/asset-condition/asset-conditions";
        $.get(url, function (data, status) {
            let json = data.data;

            let table = $("#assetConditionTable").DataTable({
                "data": json,
                "columns": [
                    {data: "name"}
                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                $("#assetConditionTable").empty();
            });
    });
    fetchOfficeBranchInSelect('refBranchCode');
</script>