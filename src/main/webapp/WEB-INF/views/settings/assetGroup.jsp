<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/15/20
  Time: 9:26 PM
  To change this template use File | Settings | File Templates.
--%>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">New Group</h2>
    </header>
    <div class="panel-body">

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label">Code</label>
                    <input type="text" name="assetGroupCode" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label">Name</label>
                    <input type="text" name="assetGroupName" class="form-control">
                </div>
            </div>
        </div>

    </div>
</section>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <button class="btn btn-info">Reset Data</button>
                        <button class="btn btn-primary">Validate</button>
                        <button class="btn btn-success" id="saveAssetGroupBtn">Submit Data</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-primary" id="loadAssetGroupBtn"><i class="fa fa-refresh"
                                                                                  aria-hidden="true"></i> Reload Data
                        </button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </footer>
        </section>
    </div>
</div>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Group Details</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none"
               id="assetGroupTable" style="width: 100%">
            <thead>
            <tr>
                <td>Code</td>
                <td>Name</td>
            </tr>
            </thead>
        </table>
    </div>
</section>
