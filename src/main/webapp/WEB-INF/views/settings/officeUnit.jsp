<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/19/20
  Time: 9:27 PM
  To change this template use File | Settings | File Templates.
--%>
<section class="panel" id="officeUnitPanel">
    <header class="panel-heading">
        <h2 class="panel-title">New Office Unit</h2>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="code">Branch Code</label>
                    <select class="form-control" id="refBranchCode" name="refBranchCode">
                        <option value="">
                            Select Branch Code
                        </option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="code">Unit Code (unique)</label>
                    <input type="text" class="form-control" id="code" name="code">
                </div>
            </div>

            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="roomNo">Room Number</label>
                    <input type="text" class="form-control" id="roomNo" name="roomNo">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="name">Unit Name</label>
                    <input type="text" class="form-control" id="name" name="name">
                </div>
            </div>
        </div>
    </div>
</section>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <button class="btn btn-info">Reset Data</button>
                        <button class="btn btn-primary">Validate</button>
                        <button class="btn btn-success" id="saveOfficeUnitBtn">Submit Data</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-primary" id="loadOfficeUnitBtn"><i class="fa fa-refresh"
                                                                                  aria-hidden="true"></i> Reload Data
                        </button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </footer>
        </section>
    </div>
</div>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Office Unit Details</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none"
               id="officeUnitTable" style="width: 100%">
            <thead>
            <tr>
                <td>Branch Code</td>
                <td>Unit Code</td>
                <td>Room Number</td>
                <td>Name</td>
            </tr>
            </thead>
        </table>
    </div>
</section>

