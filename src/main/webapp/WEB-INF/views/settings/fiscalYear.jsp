<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">New Fiscal Year</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label for="name" class="control-label">Name</label>
                            <input type="text" name="name" id="name" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Start Date(B.S.)</label>
                            <input type="text" name="startDateNp" id="startDateNp" class="form-control"
                                   onblur="nepaliToEnglishDC('#startDateNp','#startDateEn')">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Start Date(A.D.)</label>
                            <input type="text" name="startDateEn" id="startDateEn" class="form-control"
                                   onblur="englishToNepaliDC('#startDateEn','#startDateNp')">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">End Date(B.S.)</label>
                            <input type="text" name="endDateNp" id="endDateNp" class="form-control"
                                   onblur="nepaliToEnglishDC('#endDateNp','#endDateEn')">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">End Date(A.D.)</label>
                            <input type="text" name="endDateEn" id="endDateEn" class="form-control"
                                   onblur="englishToNepaliDC('#endDateEn','#endDateNp')">
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-md-6"></div>
                            <div class="col-md-6">
                                <button class="btn btn-info">Reset Data</button>
                                <button class="btn btn-primary">Validate</button>
                                <button class="btn btn-success" id="fiscalYearSaveBtn">Submit Data</button>
                            </div>
                        </div>
                    </footer>
                </section>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <section class="panel">
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-primary" id="loadFiscalYearBtn"><i class="fa fa-refresh"
                                                                                          aria-hidden="true"></i>
                                    Reload Data
                                </button>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                    </footer>
                </section>
            </div>
        </div>
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Fiscal Years</h2>
            </header>
            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none"
                       id="fiscalYearTbl" style="width: 100%">
                    <thead>
                    <tr>
                        <td>Name</td>
                        <td>Start Date (B.S.)</td>
                        <td>Start Date (A.D.)</td>
                        <td>End Date (B.S.)</td>
                        <td>End Date (A.D.)</td>
                        <td>Closed</td>
                        <td>Depreciation Calculated</td>
                        <td>Action</td>
                    </tr>
                    </thead>
                </table>
            </div>
        </section>
    </div>
</div>
<ui:footer/>
<script>
    /**
     * Fiscal Year create action
     * **/
    $("#fiscalYearSaveBtn").click(function () {
        let json = {
            name: $("#name").val(),
            startDateNp: $("#startDateNp").val(),
            startDateEn: $("#startDateEn").val(),
            endDateNp: $("#endDateNp").val(),
            endDateEn: $("#endDateEn").val()
        }
        let url = "${pageContext.request.contextPath }/fiscalYears";
        ajaxPost(url, json);
    });
    /**
     * Fiscal Year DataTable
     */
    $("#loadFiscalYearBtn").click(function () {
        let url = "${pageContext.request.contextPath }/fiscalYears";
        $.get(url, function (data, status) {
            let json = data.data;

            let table = $("#fiscalYearTbl").DataTable({
                "data": json,
                "columns": [
                    {data: "name", columnDefault:""},
                    {data: "startDateNp", columnDefault:""},
                    {data: "startDateEn", columnDefault:""},
                    {data: "endDateNp", columnDefault:""},
                    {data: "endDateEn", columnDefault:""},
                    {data: "closed", columnDefault:""},
                    {data: "depreciationCalculated", columnDefault:""},
                    {
                        "data": "Action",
                        "orderable": false,
                        "searchable": false,
                        "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                            let a = '<a>Closing</a>&nbsp;&nbsp;&nbsp;<a>Calculate Depreciation</a>';
                            return a;
                        }
                    },
                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                $("#fiscalYearTbl").empty();
            });
    });
</script>