<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/12/20
  Time: 1:58 PM
  To change this template use File | Settings | File Templates.
--%>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">New Company</h2>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label">Type</label>
                    <select  name="companyType" class="form-control">
                        <option value="COMPANY" selected>COMPANY</option>
                        <option value="SUPPLIER">SUPPLIER</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label">Vat Pan</label>
                    <input type="text" name="vatPan" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label">Name</label>
                    <input type="text" name="name" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label">Address</label>
                    <input type="text" name="address" class="form-control">
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label">Contact Number</label>
                    <input type="text" name="contactNumber" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label">Email</label>
                    <input type="text" name="email" class="form-control">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label">Contact Person</label>
                    <input type="text" name="contactPerson" class="form-control">
                </div>
            </div>
        </div>
    </div>
</section>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <button class="btn btn-info">Reset Data</button>
                        <button class="btn btn-primary">Validate</button>
                        <button class="btn btn-success" id="saveCompanyBtn">Submit Data</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-primary" id="loadCompanyDataBtn"><i class="fa fa-refresh" aria-hidden="true"></i> Reload Data</button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </footer>
        </section>
    </div>
</div>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Company Details</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none"
               id="companyTable" style="width: 100%">
            <thead>
            <tr>
                <td>VatPan</td>
                <td>Name</td>
                <td>Address</td>
                <td>Contact No.</td>
                <td>Email</td>
                <td>Contact Person</td>
                <td>Company Type</td>
            </tr>
            </thead>
        </table>
    </div>
</section>
