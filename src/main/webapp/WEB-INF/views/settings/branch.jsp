<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/19/20
  Time: 11:22 AM
  To change this template use File | Settings | File Templates.
--%>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">New Branch</h2>
    </header>
    <div class="panel-body">
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="officeLevel">Office
                        Level</label>
                    <select class="form-control input-sm mb-md" name="officeLevel" id="officeLevel">
                        <option value="ho">Head</option>
                        <option value="ro">Regional</option>
                        <option value="br">Branch</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="ro_code">RO
                        Code</label>
                    <input type="text" class="form-control" id="ro_code" name="roCode">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="ro_code">HO
                        Code</label>
                    <input type="text" class="form-control" id="ho_code" name="hoCode">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="ro_code">Branch Code</label>
                    <input type="text" class="form-control" id="branch_code" name="branchCode">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="name">Name</label>
                    <input type="text" class="form-control" id="name" name="branchName">
                </div>
            </div>
            <div class="col-sm-4">
                <div class="form-group">
                    <label class="control-label" for="address">Address</label>
                    <input type="text" class="form-control" id="address" name="branchAddress">
                </div>
            </div>
        </div>

    </div>
</section>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <button class="btn btn-info">Reset Data</button>
                        <button class="btn btn-primary">Validate</button>
                        <button class="btn btn-success" id="saveBranchBtn">Submit Data</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-6">
                        <button class="btn btn-primary" id="loadBranchBtn"><i class="fa fa-refresh"
                                                                              aria-hidden="true"></i> Reload Data
                        </button>
                    </div>
                    <div class="col-md-6"></div>
                </div>
            </footer>
        </section>
    </div>
</div>
<section class="panel">
    <header class="panel-heading">
        <h2 class="panel-title">Branch Details</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none"
               id="branchTable" style="width: 100%">
            <thead>
            <tr>
                <td>HO Code</td>
                <td>RO Code</td>
                <td>Branch Code</td>
                <td>Office Level</td>
                <td>Name</td>
                <td>Address</td>
            </tr>
            </thead>
        </table>
    </div>
</section>
