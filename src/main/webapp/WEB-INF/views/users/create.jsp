<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ui:header/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="fa fa-caret-down"></a>
                </div>

                <h2 class="panel-title">Create User</h2>
            </header>
            <div class="panel-body">
                <form class="form-horizontal form-bordered" method="post" name="userform" id="userform">
                    <input type="hidden" id="id" value="0">
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label" for="staffid">Staff ID</label>
                            <select class="form-control" id="staffid" name="staffid" required>
                                <c:forEach items="${staffs }" var="s">
                                    <option value="${s.code }">${s.code }-${s.firstName } ${s.lastName }</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="uname">User Name</label>
                            <input type="text" class="form-control" id="uname" name="uname">
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="uname">Roles</label>
                            <select class="form-control" id="roles" name="roles" multiple>
                                <option value="1">SUPER ADMIN</option>
                                <option value="2">ADMIN</option>
                                <option value="3">AUTHORIZER</option>
                                <option value="4">INPUTTER</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <label class="control-label" for="password">Password</label>
                            <input type="password" class="form-control" id="password" name="password">
                        </div>
                        <div class="col-md-4">
                            <label class="control-label" for="repass">Confirm Password</label>
                            <input type="password" class="form-control" id="repass" name="repass">
                        </div>
                    </div>
                    <br/>
                    <footer class="panel-footer">
                        <div class="row">
                            <div class="col-sm-9 col-sm-offset-3">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn btn-default">Reset</button>
                            </div>
                        </div>
                    </footer>
                </form>
            </div>
        </section>
    </div>
</div>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">User Details</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none"
               id="userTable">
            <thead>
            <tr>
                <th>User name</th>
                <th>Staff code</th>
                <th>First name</th>
                <th>Last name</th>
                <th>Status</th>
            </tr>
            </thead>
        </table>
    </div>
</section>

<ui:footer/>
<script>
    $(document).ready(function () {
        loadUserTable();
        $('#staffid').select2();
        $('#roles').select2();

        // SUBMIT FORM
        $("#userform").submit(function (event) {
            // Prevent the form from submitting via the browser.
            event.preventDefault();
            let password = $('#password').val();
            let repass = $('#repass').val();
            //pass values only when the passwords match
            if (password == repass) {
                //get multiple values from roles select option
                let roles_list = $("#roles").val();
                let array = [];
                //convert roles selected values to json array object
                $(roles_list).each(function (index, element) {
                    let data = {"id": element};
                    array.push(data);
                });

                let formData = {
                    "username": $("#uname").val(),
                    "password": password,
                    "confirmpassword": repass,
                    "status": true,
                    "staffs": {
                        "code": $('#staffid').val()
                    }
                };
                formData['roles'] = array;
                //formDara.roles=array;
                console.log(formData);
                let url = "${pageContext.request.contextPath }/users";
                ajaxPost(url, formData);
            } else {
                alert('Password Mismatch!');
            }
        });

    });

    function loadUserTable() {
        let url = "${pageContext.request.contextPath }/users";
        $.get(url, function (data, status) {
            let json = data.data;
            $('#userTable').DataTable({
                "data": json,
                "columns": [
                    {data: "username", defaultContent:""},
                    {data: "staffs.code", defaultContent:""},
                    {data: "staffs.firstName", defaultContent:""},
                    {data: "staffs.lastName", defaultContent:""},
                    {data: "status", defaultContent:""},
                ]
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                $('#userTable').DataTable();
            });
    }
</script> 
