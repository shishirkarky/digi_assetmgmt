<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/26/20
  Time: 2:29 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">Asset Details (${assetType})</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none"
               id="dataTable" style="width: 100%">
            <thead>
            <tr>
                <th>Item Code</th>
                <th>Item Name</th>
                <th>Decision Date (B.S.)</th>
                <th>Purchase Date (B.S.)</th>
                <th>Bill Number</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>
    </div>
</section>
<ui:footer/>
<script>
    $(document).ready(function () {
        let assetType="${assetType}";
        loadTable(assetType);
    });

    function loadTable(assetType) {
        let url = "${pageContext.request.contextPath }/assets/get-by-asset-type/"+assetType;
        $.get(url, function (data, status) {
            let json = data.data.assetDetail;
            let table = $('#dataTable').DataTable({
                "data": json,
                "columns": [
                    {data: "itemCode"},
                    {data: "itemName"},
                    {data: "decisionDateNp"},
                    {data: "purchaseDateNp"},
                    {data: "billNumber"},
                    {
                        "data": "Action",
                        "orderable": false,
                        "searchable": false,
                        "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                            let action = '<a class="btn btn-primary modal-with-form" href="${pageContext.request.contextPath }/assets/detail?itemCode='+row.itemCode+'"><i class="fa fa-edit"></i></a> &nbsp;&nbsp; <a class="btn btn-info modal-with-form" target="_blank" href="${pageContext.request.contextPath }/qr-code/' + row.itemCode + '"><i class="fa fa-qrcode"></i></a></li>'
                            return action;
                        }
                    }

                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                alert('No Data Found!');
                $('#dataTable').DataTable();
            });
    }
</script>
