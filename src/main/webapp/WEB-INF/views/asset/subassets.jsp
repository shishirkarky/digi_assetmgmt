<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/2/20
  Time: 7:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">

        </div>

        <h2 class="panel-title">New Asset Details </h2>

    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none"
               id="dataTable" style="width: 100%">
            <thead>
            <tr>
                <th>Item Code</th>
                <th>Sub-item Code</th>
                <th>Staff Code</th>
                <th>Asset Condition</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>
    </div>
</section>
<ui:footer/>
<script>
    $(document).ready(function () {
        let itemCode = '${itemCode}';
        loadTable(itemCode);
    });

    function loadTable(itemCode) {
        let url = "${pageContext.request.contextPath }/assets/subassets?itemCode="+itemCode;
        $.get(url, function (data, status) {
            let json = data.data;
            let table = $('#dataTable').DataTable({
                "data": json,
                "columns": [
                    {data: "itemCode", defaultContent:""},
                    {data: "subItemCode", defaultContent:""},
                    {data: "staffCode",defaultContent: ""},
                    {data: "currentAssetCondition", defaultContent: ""},
                    {data: "status",defaultContent: ""},
                    {
                        "data": "Action",
                        "orderable": false,
                        "searchable": false,
                        "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                            let qrCodeBtn = '<a class="btn btn-sm btn-success" target="_blank" href="${pageContext.request.contextPath }/qr-code/' + row.itemCode + '" data-toggle="tooltip" title="QR-CODE"><i class="fa fa-qrcode"></i></a>';
                            return qrCodeBtn;
                        }
                    }

                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                alert('No Data Found!');
                $('#dataTable').DataTable();
            });
    }
</script>