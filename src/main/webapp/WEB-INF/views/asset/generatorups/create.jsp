<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/asset" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ui:header/>
<asset:coreDetails/>
<asset:assetDetail/>
<div class="row">
    <div class="col-lg-12">
        <section class="panel" id="generatorUpsDetailPanel">
            <header class="panel-heading">
                <h2 class="panel-title">${pagetitle} DETAILS</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Fuel Type</label>
                            <select type="text" name="fuelType" class="form-control">
                                <option value="OTHER">
                                    OTHER
                                </option>
                                <option value="PETROL">
                                    PETROL
                                </option>
                                <option value="DIESEL">
                                    DIESEL
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Capacity</label>
                            <input type="text" name="capacity" class="form-control">
                        </div>
                    </div>

                </div>
            </div>
        </section>
    </div>
</div>
<asset:formButtonSection/>
<ui:footer/>
<script>
    $(document).ready(function () {
        let itemCode = '${itemCode}';
        fillDataByItemCode(itemCode);
        $("#saveFormBtn").click(function () {
            let blockIds = [
                '#coreDetailPanel',
                '#assetDetailPanel',
                '#generatorUpsDetailPanel'
            ];
            let json = getJsonByBlockList(blockIds);
            if (itemCode.length > 0) {
                json['itemCode'] = itemCode;
            }
            json['assetType'] = 'GENERATOR_UPS';

            let url = "${pageContext.request.contextPath }/assets/save";
            ajaxPost(url, json, true, 'ASSETS');
        });
    });

    function doWork(data) {
        let json = data.data;
        fillDataInBlockInputSelect(json);
    }
</script>
<asset:assetFooter/>