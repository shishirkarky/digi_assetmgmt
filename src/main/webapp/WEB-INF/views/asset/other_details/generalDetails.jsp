<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/19/20
  Time: 3:03 PM
  To change this template use File | Settings | File Templates.
--%>
<div class="row" id="coreDetailPanel">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">CORE DETAILS</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Item Code</label>
                            <input type="text" id="itemCode" readonly name="itemCode" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
