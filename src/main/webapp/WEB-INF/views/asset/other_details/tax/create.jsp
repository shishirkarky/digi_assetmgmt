<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/19/20
  Time: 2:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/asset" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ui:header/>
<jsp:include page="../generalDetails.jsp"/>
<asset:taxDetail/>
<asset:formButtonSection/>
<asset:dataTableAssetOtherDetails/>
<ui:footer/>
<script>
    $(document).ready(function () {
        loadTableForAssetOtherDetails();
    });
    $("#validateBtn").click(function () {
        let itemCode = $("#itemCode").val();
        if (itemCode.length > 0) {
            let url = "${pageContext.request.contextPath }/assets/tax?itemCode=" + itemCode;
            ajaxGetUrlCall(url);
        }
    });

    function doWork(data) {
        fillDataInBlockInputSelect(data.data);
    }

    $("#saveFormBtn").click(function () {
        let blockIds = [
            '#taxDetailPanel'
        ];
        let json = getJsonByBlockList(blockIds);
        json['itemCode'] = $("#itemCode").val();
        let url = "${pageContext.request.contextPath }/assets/tax";
        ajaxPost(url, json);
    });
</script>