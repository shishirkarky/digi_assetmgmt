<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/26/20
  Time: 10:38 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<div class="col-md-12 col-lg-12 col-xl-12">
    <div class="row">
        <div class="col-md-12 col-lg-4 col-xl-4">
            <section class="panel panel-featured-left panel-featured-primary">
                <div class="panel-body">
                    <div class="widget-summary">
                        <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon bg-primary">
                                <i class="fa fa-globe"></i>
                            </div>
                        </div>
                        <div class="widget-summary-col">
                            <div class="summary">
                                <a href="<c:url value="/assets/other/create-page"/>"><h4 class="title">General
                                    Assets</h4></a>
                                <div class="info">
                                    <strong class="amount" id="generalCount"></strong>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a href="<c:url value="/assets/detail?assetType=OTHER"/>"
                                   class="text-muted text-uppercase">(view all)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-12 col-lg-4 col-xl-4">
            <section class="panel panel-featured-left panel-featured-secondary">
                <div class="panel-body">
                    <div class="widget-summary">
                        <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon bg-secondary">
                                <i class="fa fa-twich" aria-hidden="true"></i>
                            </div>
                        </div>
                        <div class="widget-summary-col">
                            <div class="summary">
                                <a href="<c:url value="/assets/generatorups/create-page"/>"><h4 class="title">Generator
                                    UPS</h4></a>
                                <div class="info">
                                    <strong class="amount" id="generatorUpsCount"></strong>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a href="<c:url value="/assets/detail?assetType=GENERATOR_UPS"/>"
                                   class="text-muted text-uppercase">(view all)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="col-md-12 col-lg-4 col-xl-4">
            <section class="panel panel-featured-left panel-featured-tertiary">
                <div class="panel-body">
                    <div class="widget-summary">
                        <div class="widget-summary-col widget-summary-col-icon">
                            <div class="summary-icon bg-tertiary">
                                <i class="fa fa-laptop"></i>
                            </div>
                        </div>
                        <div class="widget-summary-col">
                            <div class="summary">
                                <a href="<c:url value="/assets/hardware/create-page"/>"><h4 class="title">Hardware</h4>
                                </a>
                                <div class="info">
                                    <strong class="amount" id="hardwareCount"></strong>
                                </div>
                            </div>
                            <div class="summary-footer">
                                <a href="<c:url value="/assets/detail?assetType=HARDWARE"/>"
                                   class="text-muted text-uppercase">(view all)</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <div class="col-md-12 col-lg-4 col-xl-4">
        <section class="panel panel-featured-left panel-featured-primary">
            <div class="panel-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-primary">
                            <i class="fa fa-circle-o"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <a href="<c:url value="/assets/software/create-page"/>"><h4 class="title">Software</h4></a>
                            <div class="info">
                                <strong class="amount" id="softwareCount"></strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <a href="<c:url value="/assets/detail?assetType=SOFTWARE"/>"
                               class="text-muted text-uppercase">(view all)</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-12 col-lg-4 col-xl-4">
        <section class="panel panel-featured-left panel-featured-secondary">
            <div class="panel-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-secondary">
                            <i class="fa fa-car"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <a href="<c:url value="/assets/motor_vehicle/create-page"/>"><h4 class="title">Motor
                                Vehicle</h4></a>
                            <div class="info">
                                <strong class="amount" id="motorVehicleCount"></strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <a href="<c:url value="/assets/detail?assetType=MOTOR_VEHICLE"/>"
                               class="text-muted text-uppercase">(view all)</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-12 col-lg-4 col-xl-4">
        <section class="panel panel-featured-left panel-featured-tertiary">
            <div class="panel-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-tertiary">
                            <i class="fa fa-cogs"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <a href="<c:url value="/assets/machinery/create-page"/>"><h4 class="title">Machinery</h4>
                            </a>
                            <div class="info">
                                <strong class="amount" id="machineryCount"></strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <a href="<c:url value="/assets/detail?assetType=MACHINERY"/>"
                               class="text-muted text-uppercase">(view all)</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-12 col-lg-4 col-xl-4">
        <section class="panel panel-featured-left panel-featured-primary">
            <div class="panel-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-primary">
                            <i class="fa fa-road"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <a href="<c:url value="/assets/land/create-page"/>"><h4 class="title">Land</h4></a>
                            <div class="info">
                                <strong class="amount" id="landCount"></strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <a href="<c:url value="/assets/detail?assetType=LAND"/>" class="text-muted text-uppercase">(view all)</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-12 col-lg-4 col-xl-4">
        <section class="panel panel-featured-left panel-featured-secondary">
            <div class="panel-body">
                <div class="widget-summary">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-secondary">
                            <i class="fa fa-building-o"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <a href="<c:url value="/assets/land_and_building/create-page"/>"><h4 class="title">Land &
                                Building</h4></a>
                            <div class="info">
                                <strong class="amount" id="landBuildingCount"></strong>
                            </div>
                        </div>
                        <div class="summary-footer">
                            <a href="<c:url value="/assets/detail?assetType=LAND_BUILDING"/>" class="text-muted text-uppercase">(view all)</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<ui:footer/>
<script>
    $(document).ready(function () {
        let url = "${pageContext.request.contextPath}/assets/count";
        getCount(url);
    })

    function getCount(url) {
        $.ajax({
            url: url,
            method: "GET",
            contentType: "application/json",
            success: function (data) {
                let json = data.data;
                $("#generalCount").html(json.generalCount);
                $("#generatorUpsCount").html(json.generatorUpsCount);
                $("#landCount").html(json.landCount);
                $("#landBuildingCount").html(json.landBuildingCount);
                $("#buildingCount").html(json.buildingCount);
                $("#hardwareCount").html(json.hardwareCount);
                $("#softwareCount").html(json.softwareCount);
                $("#motorVehicleCount").html(json.motorVehicleCount);
                $("#machineryCount").html(json.machineryCount);
            },
            error: function (err) {
                toastNotification("Error", 'Count fetch error', 'error');
            }
        });
    }
</script>
