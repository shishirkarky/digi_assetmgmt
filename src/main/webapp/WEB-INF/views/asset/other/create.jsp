<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/asset" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ui:header/>
<asset:coreDetails/>
<asset:assetDetail/>
<asset:formButtonSection/>
<ui:footer/>
<asset:assetFooter/>
<script>
    $(document).ready(function () {
        let itemCode = '${itemCode}';
        fillDataByItemCode(itemCode);
    $("#saveFormBtn").click(function () {
        let blockIds = [
            '#coreDetailPanel',
            '#assetDetailPanel'
        ];
        let json = getJsonByBlockList(blockIds);
        if (itemCode.length > 0) {
            json['itemCode'] = itemCode;
        }
        json['assetType'] = 'OTHER';
        let url = "${pageContext.request.contextPath }/assets/save";
        ajaxPost(url, json, true, 'ASSETS');
    });
    });

    function doWork(data) {
        let json = data.data;
        fillDataInBlockInputSelect(json);
    }
</script>
