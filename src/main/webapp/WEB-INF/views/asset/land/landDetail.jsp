<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 3/26/20
  Time: 7:16 PM
--%>
<div class="row" >
    <div class="col-lg-12">
        <section class="panel" id="landDetailPanel">
            <header class="panel-heading">
                <h2 class="panel-title">LAND DETAILS</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">District</label>
                            <select name="refDistrictId" class="form-control isnumber">
                                <option value="0">Select District</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">VDC/Mun</label>
                            <select name="refVdcMunId" class="form-control isnumber">
                                <option value="0">Select VDC/MUN</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Ward Number</label>
                            <input type="number" name="wardNumber" class="form-control isnumber">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Legacy VDC/Mun</label>
                            <input type="text" name="legacyVdcMun" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Legacy Ward Number</label>
                            <input type="number" name="legacyWardNumber" class="form-control isnumber">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Tole</label>
                            <input type="text" name="tole" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Longitude</label>
                            <input type="text" name="longitude" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Latitude</label>
                            <input type="text" name="latitude" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Area Unit</label>
                            <select name="landSizeUnit" class="form-control">
                                <option value="SQ_METER">
                                    SQ METER
                                </option>
                                <option value="BIGHA">
                                    BIGHA
                                </option>
                                <option value="ROPANI">
                                    ROPANI
                                </option>
                                <option value="SQ_FEET">
                                    SQ_FEET
                                </option>
                                <option value="OTHER" selected>
                                    OTHER
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Land Size</label>
                            <input type="text" value="0" name="landSize" class="form-control isdouble">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Plot Number</label>
                            <input type="text" name="plotNumber" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Moth Number</label>
                            <input type="text" name="mothNumber" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Sheet Number</label>
                            <input type="text" name="sheetNumber" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Used Status</label>
                            <select name="usedStatus" class="form-control">
                                <option value="SELF_USED">
                                    SELF_USED
                                </option>
                                <option value="RENTED">
                                    RENTED
                                </option>
                                <option value="PUBLIC_CAPTURED">
                                    PUBLIC_CAPTURED
                                </option>
                                <option value="GOVERNMENT_CAPTURED">
                                    GOVERNMENT_CAPTURED
                                </option>
                                <option value="OTHER">
                                    OTHER
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
