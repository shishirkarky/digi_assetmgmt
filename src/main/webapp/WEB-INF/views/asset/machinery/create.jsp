<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/asset" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ui:header/>
<asset:coreDetails/>
<asset:assetDetail/>
<div class="row" id="machineryDetailPanel">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">${pagetitle} DETAILS</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Type</label>
                            <input type="text" name="type" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<asset:formButtonSection/>
<ui:footer/>
<asset:assetFooter/>
<script>
    $(document).ready(function () {
        let itemCode = '${itemCode}';
        fillDataByItemCode(itemCode);
        $("#saveFormBtn").click(function () {
            let blockIds = [
                '#coreDetailPanel',
                '#assetDetailPanel',
                '#machineryDetailPanel'
            ];
            let json = getJsonByBlockList(blockIds);
            if (itemCode.length > 0) {
                json['itemCode'] = itemCode;
            }
            json['assetType'] = 'MACHINERY';
            let url = "${pageContext.request.contextPath }/assets/save";
            ajaxPost(url, json, true, 'ASSETS');
        });
    });

    function doWork(data) {
        let json = data.data;
        fillDataInBlockInputSelect(json);
    }
</script>