<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/asset" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ui:header/>
<asset:coreDetails/>
<div id="landBuildingDetailPanel">
    <jsp:include page="../land/landDetail.jsp"/>
    <div class="row">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">BUILDING DETAILS</h2>
                </header>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Building Area Unit</label>
                                <select name="buildingAreaUnit" class="form-control">
                                    <option value="SQ_METER">
                                        SQ METER
                                    </option>
                                    <option value="BIGHA">
                                        BIGHA
                                    </option>
                                    <option value="ROPANI">
                                        ROPANI
                                    </option>
                                    <option value="SQ_FEET">
                                        SQ_FEET
                                    </option>
                                    <option value="OTHER" selected>
                                        OTHER
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Building Area</label>
                                <input type="text" name="buildingArea" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Construction Date(B.S.)</label>
                                <input type="text" name="constructionDateNp" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Construction Date(A.D.)</label>
                                <input type="text" name="constructionDateEn" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Construction Type</label>
                                <select name="constructionType" class="form-control">
                                    <option value="MUDDY_AND_BRICK">
                                        MUDDY AND BRICK
                                    </option>
                                    <option value="FRAME_STRUCTURE">
                                        FRAME STRUCTURE
                                    </option>
                                    <option value="WALL_STRUCTURE">
                                        WALL STRUCTURE
                                    </option>
                                    <option value="SHADE_STRUCTURE">
                                        SHADE STRUCTURE
                                    </option>
                                    <option value="OTHER">
                                        OTHER
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Map Approved</label>
                                <select name="mapApproved" class="form-control">
                                    <option value="YES">
                                        YES
                                    </option>
                                    <option value="NO">
                                        NO
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Map Approved Date(B.S.)</label>
                                <input type="text" name="mapApprovedDateNp" class="form-control">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Map Approved Date(A.D.)</label>
                                <input type="text" name="mapApprovedDateEn" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<asset:formButtonSection/>
<ui:footer/>
<asset:assetFooter/>
<script>
    $(document).ready(function () {
        let itemCode = '${itemCode}';
        fillDataByItemCode(itemCode);
        $("#saveFormBtn").click(function () {
            let blockIds = [
                '#coreDetailPanel',
                '#landBuildingDetailPanel'
            ];
            let json = getJsonByBlockList(blockIds);
            if (itemCode.length > 0) {
                json['itemCode'] = itemCode;
            }
            json['assetType'] = 'LAND_BUILDING';
            let url = "${pageContext.request.contextPath }/assets/save";
            ajaxPost(url, json);
        });
    });

    function doWork(data) {
        let json = data.data;
        fillDataInBlockInputSelect(json);
    }
</script>