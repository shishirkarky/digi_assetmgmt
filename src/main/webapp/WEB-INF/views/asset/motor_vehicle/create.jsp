<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/asset" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ui:header/>
<asset:coreDetails/>
<div class="row" id="motorVehicleDetailPanel">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">${pagetitle} DETAILS</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Vehicle Type</label>
                            <select type="text" name="vehicleType" class="form-control">
                                <option value="CAR">
                                    CAR
                                </option>
                                <option value="VAN">
                                    VAN
                                </option>
                                <option value="JEEP">
                                    JEEP
                                </option>
                                <option value="MOTORCYCLE">
                                    MOTORCYCLE
                                </option>
                                <option value="TRUCK">
                                    TRUCK
                                </option>
                                <option value="CASH_VAN">
                                    CASH VAN
                                </option>
                                <option value="OTHER" selected>
                                    OTHER
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Wheel Type</label>
                            <select type="text" name="wheelType" class="form-control">
                                <option value="TWO_WHEEL">
                                    TWO WHEEL
                                </option>
                                <option value="FOUR_WHEEL">
                                    FOUR WHEEL
                                </option>
                                <option value="OTHER" selected>
                                    OTHER
                                </option>

                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Vehicle Number</label>
                            <input type="text" name="vehicleNumber" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Chassis Number</label>
                            <input type="text" name="chassisNumber" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Engine Number</label>
                            <input type="text" name="engineNumber" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Working Condition</label>
                            <input type="text" name="workingCondition" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<asset:formButtonSection/>
<ui:footer/>
<asset:assetFooter/>
<script>
    $(document).ready(function () {
        let itemCode = '${itemCode}';
        fillDataByItemCode(itemCode);
        $("#saveFormBtn").click(function () {
            let blockIds = [
                '#coreDetailPanel',
                '#assetDetailPanel',
                '#motorVehicleDetailPanel'
            ];
            let json = getJsonByBlockList(blockIds);
            if (itemCode.length > 0) {
                json['itemCode'] = itemCode;
            }
            json['assetType'] = 'MOTOR_VEHICLE';
            let url = "${pageContext.request.contextPath }/assets/save";
            ajaxPost(url, json);
        });
    });

    function doWork(data) {
        let json = data.data;
        fillDataInBlockInputSelect(json);
    }
</script>