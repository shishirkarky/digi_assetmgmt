<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/26/20
  Time: 3:15 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">Motor Vehicle Details</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none"
               id="dataTable" style="width: 100%">
            <thead>
            <tr>
                <th>Item Code</th>
                <th>Vehicle Type</th>
                <th>Wheel Type</th>
                <th>Vehicle Number</th>
                <th>Chassis Number</th>
                <th>Engine Number</th>
                <th>Working Condition</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>
    </div>
</section>
<ui:footer/>
<script>
    $(document).ready(function () {
        loadTable();
    });

    function loadTable(assetType) {
        let url = "${pageContext.request.contextPath }/assets/get-by-asset-type/MOTOR_VEHICLE";
        $.get(url, function (data, status) {
            let json = data.data.motorVehicleDetail;
            let table = $('#dataTable').DataTable({
                "data": json,
                "columns": [
                    {data: "itemCode"},
                    {data: "vehicleType"},
                    {data: "wheelType"},
                    {data: "vehicleNumber"},
                    {data: "chassisNumber"},
                    {data: "engineNumber"},
                    {data: "workingCondition"},
                    {
                        "data": "Action",
                        "orderable": false,
                        "searchable": false,
                        "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                            let actionBtn = '<div class="btn-group"><button type="button" class="mb-xs mt-xs mr-xs btn btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-ellipsis-h" aria-hidden="true"></i> <span class="caret"></span></button><ul class="dropdown-menu" role="menu">' +
                                '<li><a class="modal-with-form" href="${pageContext.request.contextPath }/assets/detail?itemCode='+row.itemCode+'"><i class="fa fa-search"></i> DETAIL </a></li>' +
                                '<li><a class="modal-with-form" target="_blank" href="${pageContext.request.contextPath }/qr-code/' + row.itemCode + '"><i class="fa fa-qrcode"></i> QR-CODE </a></li>' +
                                '</ul></div>';
                            return actionBtn;
                        }
                    }

                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                alert('No Data Found!');
                $('#dataTable').DataTable();
            });
    }
</script>
