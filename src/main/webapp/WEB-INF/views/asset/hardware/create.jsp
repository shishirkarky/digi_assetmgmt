<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="asset" tagdir="/WEB-INF/tags/asset" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<ui:header/>
<asset:coreDetails/>
<asset:assetDetail/>
<div class="row" id="hardwareDetailPanel">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">${pagetitle} DETAILS</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Security Tested</label>
                            <select type="text" name="securityTested" class="form-control">
                                <option value="YES">
                                    YES
                                </option>
                                <option value="NO">
                                    NO
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">License Number</label>
                            <input type="text" name="licenseNumber" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">MAC Address</label>
                            <input type="text" name="macAddress" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">License Expiry Date(A.D.)</label>
                            <input type="text" name="licenseExpiryDateEn" placeholder="yyyy-MM-dd" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">License Expiry Date(B.S.)</label>
                            <input type="text" name="licenseExpiryDateNp" placeholder="yyyy-MM-dd" class="form-control">
                        </div>
                    </div>
                </div>
            </div>

        </section>
    </div>
</div>
<asset:formButtonSection/>
<ui:footer/>
<asset:assetFooter/>
<script>
    $(document).ready(function () {
        let itemCode = '${itemCode}';
        fillDataByItemCode(itemCode);
        $("#saveFormBtn").click(function () {
            let blockIds = [
                '#coreDetailPanel',
                '#assetDetailPanel',
                '#hardwareDetailPanel'
            ];
            let json = getJsonByBlockList(blockIds);
            if (itemCode.length > 0) {
                json['itemCode'] = itemCode;
            }
            json['assetType'] = 'HARDWARE';

            let url = "${pageContext.request.contextPath }/assets/save";
            ajaxPost(url, json, true, 'ASSETS');
        });
    });

    function doWork(data) {
        let json = data.data;
        fillDataInBlockInputSelect(json);
    }
</script>