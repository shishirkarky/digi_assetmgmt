<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/2/20
  Time: 7:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<ui:header/>
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="fa fa-caret-down"></a>
        </div>

        <h2 class="panel-title">Asset Details</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped mb-none"
               id="dataTable" style="width: 100%">
            <thead>
            <tr>
                <th>Item Code</th>
                <th>Asset Type</th>
                <th>Staff Code</th>
                <th>Asset Condition</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
        </table>
    </div>
</section>
<ui:footer/>
<script>
    $(document).ready(function () {
        loadTable();
    });

    function loadTable() {
        let url = "${pageContext.request.contextPath }/assets/get-all";
        $.get(url, function (data, status) {
            let json = data.data;
            let table = $('#dataTable').DataTable({
                "data": json,
                "columns": [
                    {
                        "data": "ItemCode",
                        "render": function (data, type, row, meta) {
                            return '<a href="${pageContext.request.contextPath }/assets/subassets/view-page?itemCode="' + row.itemCode + '>' + row.itemCode + '</a>';
                        }
                    },
                    {data: "assetType"},
                    {data: "staffCode"},
                    {data: "currentAssetCondition"},
                    {data: "status"},
                    {
                        "data": "Action",
                        "orderable": false,
                        "searchable": false,
                        "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                            let detailsBtn = '<a class="btn btn-sm btn-info" href="${pageContext.request.contextPath }/assets/detail?itemCode=' + row.itemCode + '" target="_blank" data-toggle="tooltip" title="Asset Details"><i class="fa fa-info-circle"></i> </a>     ';
                            let qrCodeBtn = '<a class="btn btn-sm btn-success" target="_blank" href="${pageContext.request.contextPath }/qr-code/' + row.itemCode + '" data-toggle="tooltip" title="QR-CODE"><i class="fa fa-qrcode"></i></a>';
                            return detailsBtn + qrCodeBtn;
                        }
                    }

                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                alert('No Data Found!');
                $('#dataTable').DataTable();
            });
    }
</script>