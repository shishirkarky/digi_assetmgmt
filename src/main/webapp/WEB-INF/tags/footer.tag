<%@ taglib prefix="ui" tagdir="/WEB-INF/tags" %>
</section>
</div>

<aside id="sidebar-right" class="sidebar-right">
    <div class="nano">
        <div class="nano-content">
            <a href="#" class="mobile-close visible-xs">
                Collapse <i class="fa fa-chevron-right"></i>
            </a>

            <div class="sidebar-right-wrapper">

                <div class="sidebar-widget widget-calendar">
                    <h6>English Calendar</h6>
                    <div data-plugin-datepicker data-plugin-skin="dark"></div>
                </div>
            </div>
        </div>
    </div>
</aside>
</section>
<ui:errorResponse/>
<ui:js/>
<script>
    function nepaliToEnglishDC(nepaliDate, successId) {
        let API_DATE_CONVERT_N_TO_E = `${pageContext.request.contextPath }/impl/api/v1/conversions/ntoe/{nepaliDate}`;
        let url = API_DATE_CONVERT_N_TO_E.replace("{nepaliDate}", $(nepaliDate).val());
        $.ajax({
            url: url,
            method: "get",
            contentType: "application/json",
            success: function (data) {
                $(successId).val(data.data);
            }
        });
    }

    function englishToNepaliDC(englishDate, successId) {
        let API_DATE_CONVERT_E_TO_N = `${pageContext.request.contextPath }/impl/api/v1/conversions/eton/{englishDate}`;
        let url = API_DATE_CONVERT_E_TO_N.replace("{englishDate}", $(englishDate).val());
        $.ajax({
            url: url,
            method: "get",
            contentType: "application/json",
            success: function (data) {
                $(successId).val(data.data);
            }
        });
    }
</script>
</body>
</html>