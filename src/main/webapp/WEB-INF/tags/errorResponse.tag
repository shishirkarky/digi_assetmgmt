<!-- Full Height Modal Right -->
<div class="modal fade right" id="errorResponse" tabindex="-1"
     role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">

    <!-- Add class .modal-full-height and then add class .modal-right (or other classes from list above) to set a position to the modal -->
    <div class="modal-dialog modal-full-height modal-right" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title w-100" id="myModalLabel">Error</h4>
            </div>
            <div class="modal-body">
                <p>
                    Status Code: <span id="statusCode"></span>
                </p>
                <p>
                    Message: <span id="msg"></span>
                </p>
                <h5>Errors:</h5>
                <div id="errors" style="color: red"></div>
            </div>
            <div class="modal-footer justify-content-center">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Full Height Modal Right -->
