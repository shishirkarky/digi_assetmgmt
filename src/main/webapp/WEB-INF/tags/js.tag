<!-- Vendor -->
<script src="${pageContext.request.contextPath }/resources/assets/vendor/jquery/jquery.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/bootstrap/js/bootstrap.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/nanoscroller/nanoscroller.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/magnific-popup/magnific-popup.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/jquery-placeholder/jquery.placeholder.js"></script>

<!-- Specific Page Vendor -->
<script src="${pageContext.request.contextPath }/resources/assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/jquery-appear/jquery.appear.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/jquery-easypiechart/jquery.easypiechart.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/flot/jquery.flot.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/flot-tooltip/jquery.flot.tooltip.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/flot/jquery.flot.pie.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/flot/jquery.flot.categories.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/flot/jquery.flot.resize.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/jquery-sparkline/jquery.sparkline.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/raphael/raphael.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/morris/morris.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/gauge/gauge.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/snap-svg/snap.svg.js"></script>
<!-- Specific Page Vendor -->
<script src="${pageContext.request.contextPath }/resources/assets/vendor/select2/select2.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/jquery-datatables/media/js/jquery.dataTables.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/vendor/jquery-datatables-bs3/assets/js/datatables.js"></script>


<!-- Theme Base, Components and Settings -->
<script src="${pageContext.request.contextPath }/resources/assets/javascripts/theme.js"></script>

<!-- Theme Custom -->
<script src="${pageContext.request.contextPath }/resources/assets/javascripts/theme.custom.js"></script>

<!-- Theme Initialization Files -->
<script src="${pageContext.request.contextPath }/resources/assets/javascripts/theme.init.js"></script>


<!-- Examples -->
<script src="${pageContext.request.contextPath }/resources/assets/javascripts/dashboard/examples.dashboard.js"></script>
<!-- Examples -->
<script src="${pageContext.request.contextPath }/resources/assets/javascripts/tables/examples.datatables.editable.js"></script>


<script src="${pageContext.request.contextPath }/resources/assets/vendor/jquery-maskedinput/jquery.maskedinput.js"></script>
<script src="${pageContext.request.contextPath }/resources/assets/external/toast/jquery.toast.min.js"></script>

<!-- Nepali Date Picker -->
<script src="${pageContext.request.contextPath }/resources/assets/datepicker/nepali-date-picker.min.js"></script>
<script>
    $(function () {
        $('.nepali-date-picker').nepaliDatePicker();
    });
</script>

<script>
    function ajaxMultipart(url, data) {
        $.ajax({
            url: url,
            method: "post",
            data: data,
            enctype: "multipart/form-data",
            headers: {
                "Accept": "application/json"
            },
            processData: false,
            contentType: false,
            timeout: 600000,
            cache: false,
            beforeSend: function () {
                return confirm("Are you sure?");
            },
            success: function (data) {
                alert('message:' + data.message);
            },
            error: function (err) {
                alert(err.status);
            }
        });
    }

    function ajaxPost(url, formData, redirect, dataType) {
        // PREPARE FORM DATA
        $.ajax({
            url: url,
            method: "post",
            data: JSON.stringify(formData),
            contentType: "application/json",
            beforeSend: function () {
                return confirm("Are you sure?");
            },
            success: function (data) {
                toastNotification('Success', data.message, 'success');

                if (redirect) {
                    setTimeout(function () {
                        if (dataType != null && dataType === 'ASSETS') {
                            let itemCode = data.data.itemCode;
                            window.location.href = "${pageContext.request.contextPath }/assets/subassets/view-page?itemCode=" + itemCode;
                        }
                    }, 1000);
                }
            },
            error: function (xhr) {
                triggerOnError(xhr);
            }
        });
    }

    function ajaxPatchOnlyUrl(url) {
        $.ajax({
            url: url,
            method: "PATCH",
            contentType: "application/json",
            beforeSend: function () {
                return confirm("Are you sure?");
            },
            success: function (data) {
                toastNotification('Success', data.message, 'success');
            },
            error: function (err) {
                triggerOnError(err);
            }
        });
    }

    function ajaxPut(url, formData) {
        // PREPARE FORM DATA
        $.ajax({
            url: url,
            method: "put",
            data: JSON.stringify(formData),
            contentType: "application/json",
            beforeSend: function () {
                return confirm("Are you sure?");
            },
            success: function (data) {
                toastNotification('Success', data.message, 'success');
            },
            error: function (err) {
                triggerOnError(err);
            }
        });
    }

    function ajaxPostOnlyUrl(url) {
        $.ajax({
            url: url,
            method: "post",
            contentType: "application/json",
            beforeSend: function () {
                $('.loading').css('z-index', '999');
            },
            success: function (data) {
                toastNotification('Success', data.message, 'success');
            },
            error: function (err) {
                triggerOnError(err);
            },
            complete: function () {
                $('.loading').css('z-index', '-1');
            }
        });
    }

    function imagePost(url, data) {

        // PREPARE FORM DATA

        $.ajax({
            url: url,
            method: "post",
            data: data,
            enctype: "multipart/form-data",
            processData: false,
            contentType: false,
            timeout: 600000,
            cache: false,
            beforeSend: function () {
                return confirm("Are you sure?");
            },
            success: function (data) {
                alert('message:' + data.message);
            },
            error: function (err) {
                alert(err.status);
            }
        });
    }


    //DELETE DATA

    function deletedata(url) {
        $.ajax({
            url: url,
            type: 'DELETE',
            beforeSend: function () {
                return confirm("Are you sure?");
            },
            success: function (data) {
                alert(data.message);
                location.reload(true);
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Status: " + textStatus);
            }
        });
    }

    function ajaxGetUrlCall(url) {
        $.ajax({
            url: url,
            method: "get",
            contentType: "application/json",
            beforeSend: function () {
                $('.loading').css('z-index', '999');
            },
            success: function (data) {
                doWork(data);
            },
            error: function (xhr) {
                triggerOnError(xhr)
            },
            complete: function () {
                $('.loading').css('z-index', '-1');
            }
        });
    }
</script>
<script>
    function getJsonByBlockList(blockIds) {
        let json = {}
        $(blockIds).each(function (index, value) {
            let jsonDetails = getJsonByBlockId(value);
            let keyName = value.replace('#', '').replace('Panel', '');
            json[keyName] = jsonDetails;
        });
        return json;
    }

    function getJsonByBlockId(panelId) {
        let panelJson = {}
        $(panelId + ' input, ' + panelId + ' select').each(function () {
            let jsonKey = $(this).attr("name");
            let jsonValue;
            if ($(this).is('.isnumber')) {
                jsonValue = parseInt($(this).val());
            } else if ($(this).is('.isdouble')) {
                jsonValue = parseFloat($(this).val());
            } else {
                jsonValue = $(this).val();
            }
            panelJson[jsonKey] = jsonValue;
        });
        return panelJson;
    }
</script>
<script>
    /**
     * Utilities
     */
    function getCompanyByType(companyType, refName) {
        let supplierUrl = "${pageContext.request.contextPath }/settings/company/type/" + companyType;
        $.get(supplierUrl, function (data, status) {
            let json = data.data;
            for (let i = 0; i <= json.length; i++) {
                let id = json[i].id;
                let name = json[i].name;
                let optionDetail = "<option value='" + id + "'>" + name + "</option>";
                $("[name=" + refName + "]").append(optionDetail);
            }
        });
    }

    function getAssetGroup() {
        let supplierUrl = "${pageContext.request.contextPath }/settings/group/groups";
        $.get(supplierUrl, function (data, status) {
            let json = data.data;
            for (let i = 0; i <= json.length; i++) {
                let id = json[i].id;
                let code = json[i].code;
                let name = json[i].name;
                let codeConcatName = code + "-" + name;
                let optionDetail = "<option value='" + id + "'>" + codeConcatName + "</option>";
                $("[name=refGroupFourId]").append(optionDetail);
            }
        });
    }

    function getFundSourceInSelect() {
        let supplierUrl = "${pageContext.request.contextPath }/settings/fund-source/get-all";
        $.get(supplierUrl, function (data, status) {
            let json = data.data;
            for (let i = 0; i <= json.length; i++) {
                let id = json[i].id;
                let name = json[i].name;
                let optionDetail = "<option value='" + id + "'>" + name + "</option>";
                $("[name=refFundSourceId]").append(optionDetail);
            }
        });
    }

    function getReceivedSourceInSelect() {
        let supplierUrl = "${pageContext.request.contextPath }/settings/received-source/get-all";
        $.get(supplierUrl, function (data, status) {
            let json = data.data;
            for (let i = 0; i <= json.length; i++) {
                let id = json[i].id;
                let name = json[i].name;
                let optionDetail = "<option value='" + id + "'>" + name + "</option>";
                $("[name=refReceivedSourceId]").append(optionDetail);
            }
        });
    }

    function fetchStaffInSelect(refName, staffsUrl) {
        $("[name=" + refName + "]").empty().append("<option value='' selected>Select Staff</option>");
        $.get(staffsUrl, function (data, status) {
            let json = data.data;
            for (let i = 0; i <= json.length; i++) {
                let code = json[i].code;
                let firstName = json[i].firstName;
                let lastName = json[i].lastName;
                let codeConcatName = code + "-" + firstName + " " + lastName;
                let optionDetail = "<option value='" + code + "'>" + codeConcatName + "</option>";
                $("[name=" + refName + "]").append(optionDetail);
            }
        });
    }

    function fetchCurrentBranchStaffInSelect(refName) {
        let url = "${pageContext.request.contextPath }/staffs/branches/selects";
        fetchStaffInSelect(refName, url);
    }

    function fetchStaffInSelectByBranchCode(refName, branchCode) {
        let url = "${pageContext.request.contextPath }/staffs/branches/" + branchCode + "/selects";
        fetchStaffInSelect(refName, url);
    }

    function fetchOfficeUnitInSelect(refName, branchId) {
        let supplierUrl = "${pageContext.request.contextPath }/office-unit/branches/" + branchId + "/selects";
        $("[name=" + refName + "]").empty().append("<option value='' selected>Select Office Unit</option>");
        $.get(supplierUrl, function (data, status) {
            let json = data.data;
            for (let i = 0; i <= json.length; i++) {
                let id = json[i].id;
                let code = json[i].code;
                let name = json[i].name;
                let codeConcatName = code + "-" + name;
                let optionDetail = "<option value='" + id + "'>" + codeConcatName + "</option>";
                $("[name=" + refName + "]").append(optionDetail);
            }
        });
    }

    function fetchOfficeBranchInSelect(refName) {
        let supplierUrl = "${pageContext.request.contextPath }/office";
        $.get(supplierUrl, function (data, status) {
            let json = data.data;
            for (let i = 0; i <= json.length; i++) {
                let branchCode = json[i].branchCode;
                let name = json[i].name;
                let codeConcatName = branchCode + "-" + name;
                let optionDetail = "<option value='" + branchCode + "'>" + codeConcatName + "</option>";
                $("[name=" + refName + "]").append(optionDetail);
            }
        });
    }

    function getAssetConditionInSelect() {
        let assetConditionUrl = "${pageContext.request.contextPath }/settings/asset-condition/asset-conditions";
        $.get(assetConditionUrl, function (data, status) {
            let json = data.data;
            for (let i = 0; i <= json.length; i++) {
                let id = json[i].id;
                let name = json[i].name;
                let optionDetail = "<option value='" + id + "'>" + name + "</option>";
                $("[name=refAssetConditionId]").append(optionDetail);
            }
        });
    }
</script>
<%--ERROR RESPONSE--%>
<script>
    function triggerOnError(xhr) {
        let data = JSON.parse(xhr.responseText);
        $('#errorResponse #statusCode').html(data.statusCode);
        $('#errorResponse #msg').html(data.message);
        $('#errorResponse #errors').empty()
        $(data.errors).each(
            function (index, value) {
                $("#errorResponse #errors").append(
                    $('<span>' + value
                        + '</span><br>'));
            });
        $('#errorResponse').modal('show');
    }
</script>
<%--TOAST NOTIFICATIONS--%>
<script>
    function toastNotification(heading, message, type) {
        $.toast({
            heading: heading,
            text: message,
            icon: type,
            hideAfter: 5000,
            position: 'bottom-center'
        })
    }
</script>
<%--FILL DATA IN FORMS--%>
<script>
    function fillDataByItemCode(itemCode) {
        if (itemCode.length > 0) {
            let url = "${pageContext.request.contextPath }/assets/" + itemCode;
            ajaxGetUrlCall(url);
        }
    }

    function fillDataInBlockInputSelect(json) {
        $.each(json, function (key, data) {
            let keyId = '#' + key + 'Panel';
            console.log(keyId);
            let nextData = data;
            if (typeof data === "string") {
            } else {
                $.each(nextData, function (key, data) {
                    if ($(keyId + ' input[name="' + key + '"]').length) {
                        $(keyId + ' input[name="' + key + '"]').val(data);
                    }
                    if ($(keyId + ' select[name="' + key + '"]').length) {
                        $(keyId + ' select[name="' + key + '"]').val(data).trigger('change');
                    }
                });
            }
        });
    }
</script>
<script>
    function loadTableForAssetOtherDetails() {
        let url = "${pageContext.request.contextPath }/assets/get-all";
        $.get(url, function (data, status) {
            let json = data.data;
            let table = $('#dataTableForAssetOtherDetails').DataTable({
                "data": json,
                "paging": false,
                "scrollY": 500,
                "columns": [
                    {
                        "data": "Action",
                        "orderable": false,
                        "searchable": false,
                        "render": function (data, type, row, meta) { // render event defines the markup of the cell text
                            let loadItemCode = "loadItemCodeValueInInput('" + row.itemCode + "')";
                            let a = '<a onclick="' + loadItemCode + '">' + row.itemCode + '</a>';
                            return a;
                        }
                    },
                    {data: "assetType"},
                    {data: "staffCode"},
                    {data: "currentAssetCondition"}
                ],
                "destroy": true
            });
        })
            .done(function (data) {
            })
            .fail(function (jqxhr, settings, ex) {
                alert('No Data Found!');
                $('#dataTable').DataTable();
            });
    }

    function loadItemCodeValueInInput(itemCode) {
        $('#itemCode').val(itemCode);
        alert("Item Code changed to: " + itemCode);
    }
</script>