<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Select asset</h2>
            </header>
            <div class="panel-body">
                <table class="table table-bordered table-striped mb-none"
                       id="dataTableForAssetOtherDetails" style="width: 100%">
                    <thead>
                    <tr>
                        <th>Item Code</th>
                        <th>Asset Type</th>
                        <th>Staff Code</th>
                        <th>Item Status</th>
                    </tr>
                    </thead>
                </table>
            </div>
        </section>
    </div>
</div>