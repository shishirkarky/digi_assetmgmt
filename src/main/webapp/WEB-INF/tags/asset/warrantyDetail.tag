<div class="row" id="warrantyDetailPanel">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">WARRANTY DETAILS</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Warranty Start Date (B.S.)</label>
                            <input type="text" name="warrantyStartDateNp" id="warrantyStartDateNp" class="form-control"
                                   onblur="nepaliToEnglishDC('#warrantyStartDateNp','#warrantyStartDateEn')"
                            >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Warranty Start Date (A.D.)</label>
                            <input type="text" name="warrantyStartDateEn" id="warrantyStartDateEn" class="form-control"
                            onblur="englishToNepaliDC('#warrantyStartDateEn','#warrantyStartDateNp')">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Warranty End Date (B.S.)</label>
                            <input type="text" name="warrantyEndDateNp" id="warrantyEndDateNp" class="form-control"
                                   onblur="nepaliToEnglishDC('#warrantyEndDateNp','#warrantyEndDateEn')"
                            >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Warranty End Date (A.D.)</label>
                            <input type="text" name="warrantyEndDateEn" class="form-control"
                                   onblur="englishToNepaliDC('#warrantyEndDateEn','#warrantyEndDateNp')"
                            >
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>