<div class="row" id="insuranceDetailPanel">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">INSURANCE DETAILS</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Insurance Start Date (B.S.)</label>
                            <input type="text" name="insuranceStartDateNp" id="insuranceStartDateNp" class="form-control"
                            onblur="nepaliToEnglishDC('#insuranceStartDateNp','#insuranceStartDateEn')"
                            >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Insurance Start Date (A.D.)</label>
                            <input type="text" name="insuranceStartDateEn" id="insuranceStartDateEn" class="form-control"
                                   onblur="englishToNepaliDC('#insuranceStartDateEn','#insuranceStartDateNp')"
                            >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Insurance End Date (B.S.)</label>
                            <input type="text" name="insuranceEndDateNp" id="insuranceEndDateNp" class="form-control"
                                   onblur="nepaliToEnglishDC('#insuranceEndDateNp','#insuranceEndDateEn')"
                            >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Insurance End Date (A.D.)</label>
                            <input type="text" name="insuranceEndDateEn" id="insuranceEndDateEn" class="form-control"
                                   onblur="englishToNepaliDC('#insuranceEndDateEn','#insuranceEndDateNp')"
                            >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Insured Amount</label>
                            <input type="text" name="insuredAmount" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Premium Amount</label>
                            <input type="text" name="premiumAmount" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Company</label>
                            <select  name="refCompanyId" class="form-control">
                                <option value="">Select Company</option>
                            </select>
                        </div>
                    </div>
                </div>

            </div>
        </section>
    </div>
</div>