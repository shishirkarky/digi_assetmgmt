<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 4/19/20
  Time: 10:46 AM
  To change this template use File | Settings | File Templates.
--%>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">CORE DETAILS</h2>
            </header>
                <div class="panel-body" id="coreDetailPanel">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Group</label>
                                <select name="refGroupFourId" class="form-control isnumber">
                                    <option value="0">
                                        Select Group
                                    </option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label class="control-label">Current Condition</label>
                                <select name="currentAssetCondition" class="form-control">
                                    <option value="NEW">
                                        NEW
                                    </option>
                                    <option value="MAINTENANCE">
                                        MAINTENANCE
                                    </option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
        </section>
    </div>
</div>
