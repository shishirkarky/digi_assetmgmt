<div class="row">
    <div class="col-lg-12">
        <section class="panel" id="assetDetailPanel">
            <header class="panel-heading">
                <h2 class="panel-title">GENERAL DETAILS</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Legacy Id</label>
                            <input type="text" name="legacyId" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Item Name</label>
                            <input type="text" name="itemName" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Item Model</label>
                            <input name="itemModel" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Received Source</label>
                            <select name="refReceivedSourceId" class="form-control isnumber">
                                <option value="0">
                                    Select Received Source
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Fund Source</label>
                            <select name="refFundSourceId" class="form-control isnumber">
                                <option value="0">Select Fund Source</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Bill Number</label>
                            <input name="billNumber" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Decision Date(A.D)</label>
                            <input id="decisionDateEn" name="decisionDateEn" placeholder="yyyy-MM-dd"
                                   class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Decision Date(B.S)</label>
                            <input type="text" name="decisionDateNp" placeholder="yyyy-MM-dd" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Asset Condition</label>
                            <select name="refAssetConditionId" class="form-control">
                                <option value="0">
                                    Select Condition
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Purchased Date(A.D)</label>
                            <input type="text" name="purchaseDateEn" placeholder="yyyy-MM-dd" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Purchased Date(B.S)</label>
                            <input type="text" name="purchaseDateNp" placeholder="yyyy-MM-dd" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Purchase Type</label>
                            <select name="purchaseType" class="form-control">
                                <option value="PURCHASED">
                                    PURCHASED
                                </option>
                                <option value="LEASED">
                                    LEASED
                                </option>
                                <option value="GAIN_SHARING_BASIS">
                                    GAIN SHARING BASIS
                                </option>
                                <option value="RENTED">
                                    RENTED
                                </option>
                                <option value="OTHER" selected>
                                    OTHER
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Quantity</label>
                            <input type="number" name="quantity" class="form-control isnumber">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Rate</label>
                            <input type="number" name="rate" class="form-control isnumber">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Amount</label>
                            <input type="text" name="amount" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Depreciation Rate</label>
                            <input type="text" name="depreciationRate" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Supplier</label>
                            <select name="refSupplierId" class="form-control isnumber">
                                <option value="0">Select Supplier</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Remarks</label>
                            <input type="text" name="remarks" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
