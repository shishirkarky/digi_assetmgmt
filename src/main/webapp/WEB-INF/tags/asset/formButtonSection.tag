<%--
  Created by IntelliJ IDEA.
  User: shishir
  Date: 3/26/20
  Time: 7:10 PM
  To change this template use File | Settings | File Templates.
--%>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <footer class="panel-footer">
                <div class="row">
                    <div class="col-md-6"></div>
                    <div class="col-md-6">
                        <button class="btn btn-info">Reset Data</button>
                        <button class="btn btn-primary" id="validateBtn">Validate</button>
                        <button class="btn btn-success" id="saveFormBtn">Submit Data</button>
                    </div>
                </div>
            </footer>
        </section>
    </div>
</div>
