<div class="row" >
    <div class="col-lg-12">
        <section class="panel" >
            <header class="panel-heading">
                <h2 class="panel-title">TAX DETAILS</h2>
            </header>
            <div class="panel-body" id="taxDetailPanel">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Tax Paid Year</label>
                            <input type="number" name="taxPaidYear" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Tax Amount</label>
                            <input type="text" name="taxAmount" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Tax Paid Date (B.S.)</label>
                            <input type="text" name="taxPaidDateNp" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Tax Paid Date (A.D.)</label>
                            <input type="text" name="taxPaidDateEn" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Due Date (B.S.)</label>
                            <input type="text" name="dueDateNp" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Due Date (A.D.)</label>
                            <input type="text" name="dueDateEn" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>