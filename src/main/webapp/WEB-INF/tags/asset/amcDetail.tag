<div class="row" id="amcDetailPanel">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">AMC DETAILS</h2>
            </header>
            <div class="panel-body">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">AMC Start Date (B.S.)</label>
                            <input type="text" name="amcStartDateNp" id="amcStartDateNp" class="form-control"
                            onblur="nepaliToEnglishDC('#amcStartDateNp','#amcStartDateEn')"
                            >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">AMC Start Date (A.D.)</label>
                            <input type="text" name="amcStartDateEn" id="amcStartDateEn"  class="form-control"
                            onblur="englishToNepaliDC('#amcStartDateEn','#amcStartDateNp')"
                            >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">AMC End Date (B.S.)</label>
                            <input type="text" name="amcEndDateNp" id="amcEndDateNp" class="form-control"
                            onblur="nepaliToEnglishDC('#amcEndDateNp','#amcEndDateEn')"
                            >
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">AMC End Date (A.D.)</label>
                            <input type="text" name="amcEndDateEn" id="amcEndDateEn" class="form-control"
                            onblur="englishToNepaliDC('#amcEndDateEn','#amcEndDateNp')"
                            >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">AMC Cost</label>
                            <input type="text" name="amcCost" class="form-control">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="form-group">
                            <label class="control-label">Company</label>
                            <select name="refCompanyId" class="form-control">
                                <option value="">Select Company</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>