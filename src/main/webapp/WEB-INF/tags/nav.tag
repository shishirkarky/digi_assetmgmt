<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<nav id="menu" class="nav-main" role="navigation">
    <ul class="nav nav-main">
        <li class="nav-active"><a href="<c:url value="/dashboard"/>">
            <i class="fa fa-home" aria-hidden="true"></i> <span>Dashboard</span>
        </a></li>
        <li><a href="<c:url value="/assets/dashboard"/>"><i class="fa fa-shopping-cart" aria-hidden="true"></i> <span>Assets Dashboard</span></a>
        </li>
        <%--        <sec:authorize access="hasAuthority('ADMIN')">--%>
        <%--SETTINGS--%>
        <li class="nav-parent"><a> <i class="fa fa-cogs"
                                      aria-hidden="true"></i> <span>Settings</span>
        </a> <!-- office settings -->
            <ul class="nav nav-children">
                <li><a href="<c:url value="/settings/create-page"/>"><i
                        class="fa fa-cogs" aria-hidden="true"></i> Admin Settings</a></li>
            </ul>
            <!-- staff settings -->
            <ul class="nav nav-children">

                <li class="nav-parent"><a><i
                        class="fa fa-users" aria-hidden="true"></i> Staff</a>
                    <ul class="nav nav-children">

                        <li><a href="<c:url value="/staffs/create-page"/>"><i
                                class="fa fa-edit" aria-hidden="true"></i> Add/Update
                            Staff</a></li>
                        <li><a href="<c:url value="/staffs/view-page"/>"><i
                                class="fa fa-search" aria-hidden="true"></i> View
                            Staff</a></li>
                    </ul>
                </li>

            </ul>
            <%-- ./STAFF SETTINGS--%>
            <!-- User settings -->
            <ul class="nav nav-children">
                <li class="nav-parent"><a><i
                        class="fa fa-user" aria-hidden="true"></i> User</a>
                    <ul class="nav nav-children">
                        <li><a href="<c:url value="/users/create-page"/>"><i
                                class="fa fa-edit" aria-hidden="true"></i> Add/Update
                            User</a></li>
                        <li><a href="<c:url value="/users/view-page"/>"><i
                                class="fa fa-search" aria-hidden="true"></i> View User</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <%--./USER SETTINGS--%>
            <%--            FISCAL YEAR--%>
            <ul class="nav nav-children">
                <li><a href="<c:url value="/fiscalYears/create-page"/>"><i
                        class="fa fa-calendar" aria-hidden="true"></i> Fiscal Year</a>
                </li>
            </ul>
            <%--            ./FISCAL YEAR--%>
        </li>
        <%--./SETTINGS--%>
        <%--        </sec:authorize>--%>
        <!-- Asset Creation and details -->
        <li class="nav-parent"><a> <i class="fa fa-plus" aria-hidden="true"></i> <span>Assets</span>
        </a>
            <ul class="nav nav-children">
                <li><a href="<c:url value="/assets/warranty/create-page"/>"><i
                        class="fa fa-plus-circle" aria-hidden="true"></i> Warranty Detail</a></li>
                <li><a href="<c:url value="/assets/insurance/create-page"/>"><i
                        class="fa fa-plus-circle" aria-hidden="true"></i> Insurance Detail</a></li>
                <li><a href="<c:url value="/assets/amc/create-page"/>"><i
                        class="fa fa-plus-circle" aria-hidden="true"></i> AMC Detail</a></li>
                <li><a href="<c:url value="/assets/tax/create-page"/>"><i
                        class="fa fa-plus-circle" aria-hidden="true"></i> Tax Detail</a></li>
            </ul>
        </li>
        <li class="nav-parent"><a> <i class="fa fa-search"
                                      aria-hidden="true"></i> <span>Assets Details</span>
        </a>
            <ul class="nav nav-children">
                <li><a href="<c:url value="/assets/view-page"/>"><i
                        class="fa fa-info-circle" aria-hidden="true"></i> Assets List</a></li>
            </ul>
        </li>
        <li class="nav-parent"><a> <i class="fa fa-exchange"
                                      aria-hidden="true"></i> <span>Transfer/Issue</span>
        </a> <!-- Transfer Issue -->
            <ul class="nav nav-children">
                <li><a href="<c:url value="/transfer-issue/transfers/create-page"/>"><i
                        class="fa fa-exchange" aria-hidden="true"></i>Transfer Assets</a></li>
                <li><a href="<c:url value="/transfer-issue/issues/create-page"/>"><i
                        class="fa fa-exchange" aria-hidden="true"></i>Issue Assets</a></li>
                <li><a href="<c:url value="/transfer-issue/transfers/pending/view-page"/>"><i
                        class="fa fa-clock-o" aria-hidden="true"></i>Pending Transfers</a></li>
                <li><a href="<c:url value="/transfer-issue/view-page"/>"><i
                        class="fa fa-database" aria-hidden="true"></i> Transfer/Issue Log</a></li>
            </ul>
        </li>
        <li class="nav-parent"><a> <i class="fa fa-file-text"
                                      aria-hidden="true"></i> <span>Bill</span>
        </a> <!-- Transfer Issue -->
            <ul class="nav nav-children">
                <li><a href="<c:url value="/bill/create-page"/>"><i
                        class="fa fa-plus" aria-hidden="true"></i>New Bill</a></li>
                <li><a href="<c:url value="/bill/view-page"/>"><i
                        class="fa fa-database" aria-hidden="true"></i> Bill Details</a></li>
            </ul>
        </li>
    </ul>

</nav>