package com.peepalsoft.app.user.controller;

import com.peepalsoft.app.exception.ValidationErrorException;
import com.peepalsoft.app.responsemessage.HttpResponses;
import com.peepalsoft.app.user.Office;
import com.peepalsoft.app.user.repository.OfficeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("office")
public class OfficeController {

    private OfficeRepo officeRepo;

    @Autowired
    public void setOfficeRepo(OfficeRepo officeRepo) {
        this.officeRepo = officeRepo;
    }

    @GetMapping
    public ResponseEntity<?> read() {
        List<Office> list = officeRepo.findAll();
        return new ResponseEntity<>(HttpResponses.fetched(list), HttpStatus.OK);
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<?> read(@PathVariable int id) {
        Optional<Office> staffsOfficeOptional = officeRepo.findById(id);
        if (staffsOfficeOptional.isPresent()) {
            return new ResponseEntity<>(HttpResponses.fetched(staffsOfficeOptional.get()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpResponses.notfound(), HttpStatus.NOT_FOUND);

    }

    @PostMapping
    public ResponseEntity<?> create(@Validated @RequestBody Office staffsOffice, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ValidationErrorException(bindingResult);
        }
        Office savedOffice = officeRepo.save(staffsOffice);
        return new ResponseEntity<>(HttpResponses.created(savedOffice), HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<?> delete(@PathVariable int id) {

        if (id > 0) {
            Optional<Office> staffsOfficeOptional = officeRepo.findById(id);
            if (staffsOfficeOptional.isPresent()) {
                try {
                    officeRepo.deleteById(id);
                    return new ResponseEntity<>(HttpResponses.received(), HttpStatus.OK);
                } catch (Exception e) {
                    return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST);
                }
            }
        }
        return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST);

    }
}
