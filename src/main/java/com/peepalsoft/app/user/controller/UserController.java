package com.peepalsoft.app.user.controller;

import com.peepalsoft.app.responsemessage.HttpResponses;
import com.peepalsoft.app.responsemessage.ResponseMessage;
import com.peepalsoft.app.user.Users;
import com.peepalsoft.app.user.enums.RolesEnum;
import com.peepalsoft.app.user.repository.StaffsRepo;
import com.peepalsoft.app.user.repository.UsersRepo;
import com.peepalsoft.app.user.service.UsersService;
import com.peepalsoft.app.util.UtilitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("users")
public class UserController {
    private UtilitiesService utilitiesService;
    private UsersRepo usersRepo;
    private StaffsRepo staffsRepo;
    private UsersService usersService;

    @Autowired
    public void setUtilitiesService(UtilitiesService utilitiesService) {
        this.utilitiesService = utilitiesService;
    }

    @Autowired
    public void setUsersRepo(UsersRepo usersRepo) {
        this.usersRepo = usersRepo;
    }

    @Autowired
    public void setStaffsRepo(StaffsRepo staffsRepo) {
        this.staffsRepo = staffsRepo;
    }

    @Autowired
    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping(path = "/create-page")
    public ModelAndView createpage() {
        ModelAndView model = new ModelAndView("users/create");
        model.addObject("pagetitle", "USERS");
        model.addObject("staffs", staffsRepo.findAll());
        return model;
    }


    @GetMapping(path = "/specificUsers")
    public ResponseEntity<?> read(@ModelAttribute("username") String currentUsername) {
        ResponseEntity<?> returntype = usersService.findByCurrentUsersStaffsOffice(currentUsername);
        return returntype;

    }

    @GetMapping()
    public ResponseEntity<?> readAllUsers(@ModelAttribute("username") String currentUsername) {
        return new ResponseEntity<>(HttpResponses.fetched(usersRepo.findAll()), HttpStatus.OK);

    }

    @GetMapping(path = "/{username}")
    public ResponseEntity<ResponseMessage> readOne(@PathVariable String username) {
        try {
            Optional<Users> usersOptional = usersRepo.findByUsername(username);
            if (usersOptional.isPresent()) {
                return new ResponseEntity<>(HttpResponses.fetched(usersOptional.get()), HttpStatus.ACCEPTED);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return new ResponseEntity<>(HttpResponses.notfound(), HttpStatus.NOT_FOUND);
    }

    @PostMapping
    public ResponseEntity<ResponseMessage> create(@RequestBody Users users, Principal principal) {
        List<String> currentUserRoles = utilitiesService.currentUserRoles(principal);
        List<String> requiredRoles = new ArrayList<>();
        requiredRoles.add(RolesEnum.SUPERADMIN.getName());
        requiredRoles.add(RolesEnum.ADMIN.getName());
        if (CollectionUtils.containsAny(currentUserRoles, requiredRoles)) {
            try {
                Users savedUsers = usersService.changedPwdUsers(users);

                if (savedUsers != null) {
                    return new ResponseEntity<>(HttpResponses.created(savedUsers), HttpStatus.OK);
                }
            } catch (Exception e) {
            }
        }
        return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<ResponseMessage> delete(@PathVariable int id) {
        Optional<Users> usersOptional = usersRepo.findById(id);
        if (usersOptional.isPresent()) {
            usersRepo.deleteById(id);
            return new ResponseEntity<>(HttpResponses.received(), HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<>(HttpResponses.notfound(), HttpStatus.NOT_FOUND);
        }
    }

}
