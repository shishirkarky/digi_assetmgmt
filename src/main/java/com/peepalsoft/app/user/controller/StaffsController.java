package com.peepalsoft.app.user.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.peepalsoft.app.assembler.StaffsAssemblerService;
import com.peepalsoft.app.config.ErrorMessageConstants;
import com.peepalsoft.app.dto.StaffsSelectDto;
import com.peepalsoft.app.exception.CustomValidationException;
import com.peepalsoft.app.exception.ValidationErrorException;
import com.peepalsoft.app.responsemessage.HttpResponses;
import com.peepalsoft.app.responsemessage.ResponseMessage;
import com.peepalsoft.app.user.Staffs;
import com.peepalsoft.app.user.repository.OfficeRepo;
import com.peepalsoft.app.user.repository.StaffsRepo;
import com.peepalsoft.app.user.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.util.Assert;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("staffs")
public class StaffsController {

    private StaffsRepo staffsRepo;
    private OfficeRepo officeRepo;
    private UsersService usersService;
    private StaffsAssemblerService staffsAssemblerService;

    @Autowired
    public void setStaffsAssemblerService(StaffsAssemblerService staffsAssemblerService) {
        this.staffsAssemblerService = staffsAssemblerService;
    }

    @Autowired
    public void setStaffsRepo(StaffsRepo staffsRepo) {
        this.staffsRepo = staffsRepo;
    }

    @Autowired
    public void setOfficeRepo(OfficeRepo officeRepo) {
        this.officeRepo = officeRepo;
    }

    @Autowired
    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    @ModelAttribute
    public void models(Model model) {
        model.addAttribute("offices", officeRepo.findAll());
    }

    @GetMapping(path = "/create-page")
    public ModelAndView createpage() {
        ModelAndView model = new ModelAndView("staff/create");
        model.addObject("pagetitle", "STAFFS");

        return model;
    }

    @GetMapping(path = "/view-page")
    public ModelAndView viewpage() {
        ModelAndView model = new ModelAndView("staff/view");
        model.addObject("pagetitle", "STAFFS");
        model.addObject("staffs", staffsRepo.findAll());
        return model;
    }

    @GetMapping
    public ResponseEntity<?> read() {
        List<Staffs> list = staffsRepo.findAll();
        return new ResponseEntity<>(HttpResponses.fetched(list), HttpStatus.OK);
    }

    @GetMapping("/selects")
    public ResponseEntity<?> findStaffsForSelect() {
        List<Staffs> list = staffsRepo.findAll();
        List<StaffsSelectDto> staffsSelectDtos = new ArrayList<>();
        list.forEach(staffs -> {
            StaffsSelectDto staffsSelectDto = new StaffsSelectDto();
            staffsSelectDto.setCode(staffs.getCode());
            staffsSelectDto.setFirstName(staffs.getFirstName());
            staffsSelectDto.setLastName(staffs.getLastName());
            staffsSelectDtos.add(staffsSelectDto);
        });
        return new ResponseEntity<>(HttpResponses.fetched(staffsSelectDtos), HttpStatus.OK);
    }

    @GetMapping("/branches/{branchCode}/selects")
    public ResponseEntity<?> findStaffsByBranchCodeForSelect(@NotNull @PathVariable String branchCode) {
        List<Staffs> list = staffsRepo.findByOfficeBranchCode(branchCode);
        List<StaffsSelectDto> staffsSelectDtos = staffsAssemblerService.toSelectDtos(list);
        return new ResponseEntity<>(HttpResponses.fetched(staffsSelectDtos), HttpStatus.OK);
    }

    @GetMapping("/branches/selects")
    public ResponseEntity<?> findCurrentBranchStaffs(Principal principal) {
        String branchCode = usersService.getBranchCode(principal.getName());
        Assert.notNull(branchCode, "Current user branchCode cannot be null.");
        List<Staffs> list = staffsRepo.findByOfficeBranchCode(branchCode);
        List<StaffsSelectDto> staffsSelectDtos = staffsAssemblerService.toSelectDtos(list);
        return new ResponseEntity<>(HttpResponses.fetched(staffsSelectDtos), HttpStatus.OK);
    }

    @GetMapping(path = "/{code}")
    public ResponseEntity<?> read(@PathVariable String code) {
        try {
            Staffs staffs = staffsRepo.findById(code).get();
            if (staffs != null) {
                return new ResponseEntity<ResponseMessage>(HttpResponses.fetched(staffs), HttpStatus.OK);
            }
        } catch (Exception e) {
        }
        return new ResponseEntity<ResponseMessage>(HttpResponses.notfound(), HttpStatus.NOT_FOUND);

    }

    @PostMapping
    public ResponseEntity<?> create(@Validated @RequestBody Staffs staffs, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ValidationErrorException(bindingResult);
        } else {
            if (staffs.getOffice() != null && officeRepo.findById(staffs.getOffice().getId()).isPresent()) {
                Staffs savedStaffs = staffsRepo.save(staffs);
                return new ResponseEntity<>(HttpResponses.created(savedStaffs), HttpStatus.CREATED);
            } else {
                throw new CustomValidationException(Collections.singletonList(ErrorMessageConstants.INVALID_REQUEST));
            }
        }
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<ResponseMessage> delete(@PathVariable String id) {
        boolean staff_present = staffsRepo.findById(id).isPresent();

        if (staff_present) {
            staffsRepo.deleteById(id);
            return new ResponseEntity<ResponseMessage>(HttpResponses.received(), HttpStatus.ACCEPTED);
        } else {
            return new ResponseEntity<ResponseMessage>(HttpResponses.notfound(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(path = "/image", consumes = {"multipart/form-data"})
    public ResponseEntity<?> saveImage(@RequestParam(required = true, value = "file") MultipartFile file,
                                       @RequestParam(required = true, value = "jsondata") String jsondata) {
        try {
            byte[] image_bytes = file.getBytes();

            Staffs staff_from_json = new ObjectMapper().readValue(jsondata, Staffs.class);
            String code = staff_from_json.getCode();

            Staffs tobesaved = staffsRepo.findById(code).get();
            tobesaved.setPic(image_bytes);

            Staffs savedstaff = staffsRepo.save(tobesaved);
            if (savedstaff != null) {
                return new ResponseEntity<ResponseMessage>(HttpResponses.created(savedstaff), HttpStatus.CREATED);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        return new ResponseEntity<ResponseMessage>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST);
    }
}
