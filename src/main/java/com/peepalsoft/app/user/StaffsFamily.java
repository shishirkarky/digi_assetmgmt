package com.peepalsoft.app.user;

import com.peepalsoft.app.entity.BaseEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table
@Getter
@Setter
public class StaffsFamily extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @NotNull
    @Size(min = 4, max = 30)
    private String firstName;

    @NotNull
    @Size(min = 4, max = 30)
    private String lastName;

    private String gender;

    @Size(min = 4, max = 30)
    private String occupation;

    @Size(min = 10, max = 10)
    private String phoneNumber;

    @Size(min = 3, max = 20)
    private String relation;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "staffs_id", nullable = false)
    private Staffs staffs;

}
