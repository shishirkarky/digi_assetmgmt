package com.peepalsoft.app.user;

import com.fasterxml.jackson.core.filter.FilteringParserDelegate;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.peepalsoft.app.controller.dates.Dates;
import com.peepalsoft.app.controller.dates.DatesRepository;
import com.peepalsoft.app.user.Roles;
import com.peepalsoft.app.user.Users;
import com.peepalsoft.app.user.enums.RolesEnum;
import com.peepalsoft.app.user.repository.OfficeRepo;
import com.peepalsoft.app.user.repository.RolesRepo;
import com.peepalsoft.app.user.repository.UsersRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.*;

@Slf4j
@Component
public class UserStartupComponent {
    @Value("${initial.user.name}")
    private String username;
    @Value("${initial.user.password}")
    private String password;

    private UsersRepo usersRepo;
    private RolesRepo rolesRepo;
    private OfficeRepo officeRepo;
    private DatesRepository datesRepository;

    @Autowired
    public void setUsersRepo(UsersRepo usersRepo) {
        this.usersRepo = usersRepo;
    }


    @Autowired
    public void setRolesRepo(RolesRepo rolesRepo) {
        this.rolesRepo = rolesRepo;
    }

    @Autowired
    public void setOfficeRepo(OfficeRepo officeRepo) {
        this.officeRepo = officeRepo;
    }

    @Autowired
    public void setDatesRepository(DatesRepository datesRepository) {
        this.datesRepository = datesRepository;
    }

    private void saveRoles() {
        if (rolesRepo.findAll().isEmpty()) {
            List<Roles> rolesList = new ArrayList<>();
            for (RolesEnum role : RolesEnum.values()) {
                Roles roles = new Roles();
                roles.setId(role.getId());
                roles.setName(role.getName());
                rolesList.add(roles);
            }
            rolesRepo.saveAll(rolesList);
        }
    }

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {

        saveRoles();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        Optional<Users> usersOptional = usersRepo.findByUsername(username);
        if (!usersOptional.isPresent()) {
            Users user = new Users();
            user.setUsername(username);
            user.setPassword(encoder.encode(password));
            user.setStatus(true);
            Roles roles = new Roles();
            roles.setId(RolesEnum.SUPERADMIN.getId());
            user.setRoles(Collections.singleton(roles));
            usersRepo.save(user);
        }

        this.createOffice();

        this.initConfigDates();
    }

    private void createOffice() {
        if (officeRepo.count() == 0) {
            Office office = new Office();
            office.setAddress("Head Office Address");
            office.setBranchCode("HO001");
            office.setRoCode("HO001");
            office.setHoCode("HO001");
            office.setCreatedAt(new Date());
            office.setUpdatedAt(new Date());
            office.setName("Head Office Kathmandu");
            office.setOfficeLevel("HO");
            officeRepo.save(office);
        }
    }

    private void initConfigDates() {
        if (datesRepository.count() == 0) {
            ObjectMapper mapper = new ObjectMapper();
            try {
                Dates[] dates = mapper.readValue(new ClassPathResource("conf_dates.json").getFile(), Dates[].class);
                List<Dates> datesList = Arrays.asList(dates);
                datesRepository.saveAll(datesList);
            } catch (Exception e) {
                log.error("initConfigDates() : Error initializing config dates", e);
            }
        }
    }
}
