package com.peepalsoft.app.user;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.Set;

@Entity
@Table
@Getter
@Setter
public class Staffs {
		
	@Id
	private String code;

	@NotNull
	@Size(min = 3, max = 30)
	private String firstName;
	
	@NotNull
	@Size(min = 3, max = 30)
	private String lastName;
	
	private String gender;
	
	private String phoneNumber;

	@NotEmpty
	@Size(min = 3, max = 30)
	private String post;
	
	@OneToOne
	private Office office;
	
	@Lob
    @Column(columnDefinition="mediumblob", nullable = true)
    private byte[] pic;
	
	@Transient
	private String base64pic;
	
	
	@JsonManagedReference
	@OneToMany(mappedBy = "staffs",cascade = CascadeType.ALL)
	private Set<StaffsFamily> staffsFamily;

	@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	@Column(updatable = false)
	protected Date createdAt;

	@Temporal(TemporalType.TIMESTAMP)
	@UpdateTimestamp
	protected Date updatedAt;
}
