package com.peepalsoft.app.user.service;

import com.peepalsoft.app.responsemessage.HttpResponses;
import com.peepalsoft.app.user.Office;
import com.peepalsoft.app.user.Staffs;
import com.peepalsoft.app.user.Users;
import com.peepalsoft.app.user.component.UsersComponent;
import com.peepalsoft.app.user.repository.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;
import java.util.Optional;

@Service
public class UsersService {
    private UsersComponent usersComponent;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private UsersRepo usersRepo;

    @Autowired
    public void setUsersComponent(UsersComponent usersComponent) {
        this.usersComponent = usersComponent;
    }

    @Autowired
    public void setbCryptPasswordEncoder(BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Autowired
    public void setUsersRepo(UsersRepo usersRepo) {
        this.usersRepo = usersRepo;
    }

    public ResponseEntity<?> findByCurrentUsersStaffsOffice(String currentUsername) {
        List<Users> list = usersComponent.findUsersByCurrentUsersStaffsOffice(currentUsername);
        return new ResponseEntity<>(HttpResponses.fetched(list), HttpStatus.OK);
    }

    public Users changedPwdUsers(Users users) {
        String password = users.getPassword();
        String confirmPassword = users.getConfirmpassword();

        String encpassword = checkPasswordConfirmPassword(password, confirmPassword);
        if (encpassword.length() > 0) {
            users.setPassword(encpassword);
            users = usersRepo.save(users);
        }
        return users;
    }

    public String checkPasswordConfirmPassword(String password, String confirmPassword) {
        String encpassword = "";
        if (password.equals(confirmPassword)) {
            encpassword = bCryptPasswordEncoder.encode(password);
        }
        return encpassword;
    }

    public String getBranchCode(String username) {
        String branchCode = "";
        Assert.notNull(username, "Username cannot be null.");
        Optional<Users> usersOptional = usersRepo.findByUsername(username);
        if (usersOptional.isPresent()) {
            Users users = usersOptional.get();
            Staffs staffs = users.getStaffs();
            if (staffs != null) {
                Office office = staffs.getOffice();
                if (office != null) {
                    branchCode = office.getBranchCode();
                }
            }
        }
        return branchCode;
    }

}
