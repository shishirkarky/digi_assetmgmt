package com.peepalsoft.app.user.repository;

import com.peepalsoft.app.user.StaffsFamily;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StaffsFamilyRepo extends JpaRepository<StaffsFamily, Integer> {
    List<StaffsFamily> findByStaffsCode(String code);
}
