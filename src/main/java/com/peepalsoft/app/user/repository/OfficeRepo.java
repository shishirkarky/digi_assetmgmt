package com.peepalsoft.app.user.repository;

import com.peepalsoft.app.user.Office;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OfficeRepo extends JpaRepository<Office, Integer> {
}
