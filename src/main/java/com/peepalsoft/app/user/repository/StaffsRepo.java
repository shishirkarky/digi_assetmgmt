package com.peepalsoft.app.user.repository;

import com.peepalsoft.app.user.Office;
import com.peepalsoft.app.user.Staffs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StaffsRepo extends JpaRepository<Staffs, String> {
    List<Staffs> findByOffice(Office office);

    Optional<Staffs> findByCode(String code);

    List<Staffs> findByOfficeBranchCode(String officeId);
}
