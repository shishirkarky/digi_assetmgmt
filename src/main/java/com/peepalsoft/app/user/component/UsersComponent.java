package com.peepalsoft.app.user.component;

import com.peepalsoft.app.component.CrudReturnService;
import com.peepalsoft.app.user.Office;
import com.peepalsoft.app.user.Users;
import com.peepalsoft.app.user.repository.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class UsersComponent {
    private UsersRepo usersRepo;
    private CrudReturnService<Users> crudReturnService;

    @Autowired
    public void setUsersRepo(UsersRepo usersRepo) {
        this.usersRepo = usersRepo;
    }

    @Autowired
    public void setCrudReturnService(CrudReturnService<Users> crudReturnService) {
        this.crudReturnService = crudReturnService;
    }

    public List<Users> findUsersByCurrentUsersStaffsOffice(String currentUsername) {
        Office currentUserOffice = findOfficeByCurrentUsers(currentUsername);
        return usersRepo.findByStaffsOffice(currentUserOffice);
    }

    public Office findOfficeByCurrentUsers(String currentUsername) {
        Optional<Users> currentUserOptional = usersRepo.findByUsername(currentUsername);
        if (currentUserOptional.isPresent()) {
            return currentUserOptional.get().getStaffs().getOffice();
        } else {
            return new Office();
        }
    }
}
