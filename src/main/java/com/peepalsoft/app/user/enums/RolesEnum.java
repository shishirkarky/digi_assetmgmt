package com.peepalsoft.app.user.enums;

import lombok.Getter;

@Getter
public enum RolesEnum {
    SUPERADMIN(1, "ROLE_SUPERADMIN"),
    ADMIN(2, "ROLE_ADMIN"),
    AUTHORIZER(3, "ROLE_AUTHORIZER"),
    INPUTTER(4, "ROLE_INPUTTER");
    int id;
    String name;

    RolesEnum(int id, String name) {
        this.id = id;
        this.name = name;
    }
}
