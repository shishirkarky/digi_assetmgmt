package com.peepalsoft.app.repo.assets.core;

import com.peepalsoft.app.entity.asset.coreentities.AssetDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AssetDetailRepo extends JpaRepository<AssetDetail, Integer> {
    Optional<AssetDetail> findByItemCode(String itemCode);

    List<AssetDetail> findByItemCodeIn(List<String> itemCodeList);
}
