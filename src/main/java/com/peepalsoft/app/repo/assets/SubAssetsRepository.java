package com.peepalsoft.app.repo.assets;

import com.peepalsoft.app.entity.asset.SubAssets;
import com.peepalsoft.app.enums.StatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SubAssetsRepository extends JpaRepository<SubAssets, Integer> {
    List<SubAssets> findAllByItemCode(String itemCode);

    List<SubAssets> findAllByBranchCode(String branchCode);

    List<SubAssets> findAllByBranchCodeAndStatus(String branchCode, StatusEnum status);

    Optional<SubAssets> findFirstBySubItemCode(String subItemCode);
}
