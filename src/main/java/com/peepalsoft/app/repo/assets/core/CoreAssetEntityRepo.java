package com.peepalsoft.app.repo.assets.core;

import com.peepalsoft.app.entity.asset.coreentities.CoreAssetEntity;
import com.peepalsoft.app.enums.AssetTypeEnum;
import com.peepalsoft.app.enums.StatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface CoreAssetEntityRepo extends JpaRepository<CoreAssetEntity, Integer> {
    Optional<CoreAssetEntity> findByItemCode(String itemCode);

    int countAllByAssetType(AssetTypeEnum assetType);

    List<CoreAssetEntity> findByAssetType(AssetTypeEnum assetType);

    List<CoreAssetEntity> findByRefBranchCodeAndStatus(String branchCode, StatusEnum status);

    List<CoreAssetEntity> findByRefBranchCodeAndStatusNotIn(String branchCode, List<StatusEnum> statuses);
}
