package com.peepalsoft.app.repo.assets;

import com.peepalsoft.app.entity.asset.trackitem.ItemTransferIssue;
import com.peepalsoft.app.enums.StatusEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ItemTransferIssueRepository extends JpaRepository<ItemTransferIssue, Integer> {
    @Query("SELECT i FROM ItemTransferIssue i WHERE i.fromBranchCode=?1 OR i.toBranchCode=?1")
    List<ItemTransferIssue> findByBranchCode(String branchCode);

    @Query("SELECT i FROM ItemTransferIssue i WHERE i.toBranchCode=?1 AND i.status=?2")
    List<ItemTransferIssue> findByBranchCodeAndStatus(String branchCode, StatusEnum status);

}
