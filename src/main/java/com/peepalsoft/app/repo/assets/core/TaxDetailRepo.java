package com.peepalsoft.app.repo.assets.core;

import com.peepalsoft.app.entity.asset.TaxDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TaxDetailRepo extends JpaRepository<TaxDetail, Integer> {
    Optional<TaxDetail> findByItemCode(String itemCode);
}
