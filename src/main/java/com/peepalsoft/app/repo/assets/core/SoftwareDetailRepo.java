package com.peepalsoft.app.repo.assets.core;

import com.peepalsoft.app.entity.asset.coreentities.SoftwareDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SoftwareDetailRepo extends JpaRepository<SoftwareDetail, Integer> {
    Optional<SoftwareDetail> findByItemCode(String itemCode);
}
