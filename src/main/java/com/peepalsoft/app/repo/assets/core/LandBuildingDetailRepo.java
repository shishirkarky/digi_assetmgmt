package com.peepalsoft.app.repo.assets.core;

import com.peepalsoft.app.entity.asset.coreentities.LandBuildingDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LandBuildingDetailRepo extends JpaRepository<LandBuildingDetail, Integer> {
    Optional<LandBuildingDetail> findByItemCode(String itemCode);
}
