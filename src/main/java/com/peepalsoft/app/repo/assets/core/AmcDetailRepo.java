package com.peepalsoft.app.repo.assets.core;

import com.peepalsoft.app.entity.asset.AmcDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AmcDetailRepo extends JpaRepository<AmcDetail, Integer> {
    Optional<AmcDetail> findByItemCode(String itemCode);
}
