package com.peepalsoft.app.repo.assets.core;

import com.peepalsoft.app.entity.asset.coreentities.MotorVehicleDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MotorVehicleDetailRepo extends JpaRepository<MotorVehicleDetail, Integer> {
    Optional<MotorVehicleDetail> findByItemCode(String itemCode);
}
