package com.peepalsoft.app.repo.assets.core;

import com.peepalsoft.app.entity.asset.coreentities.HardwareDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface HardwareDetailRepo extends JpaRepository<HardwareDetail, Integer> {
    Optional<HardwareDetail> findByItemCode(String itemCode);
}
