package com.peepalsoft.app.repo.assets.core;

import com.peepalsoft.app.entity.asset.coreentities.MachineryDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MachineryDetailRepo extends JpaRepository<MachineryDetail, Integer> {
    Optional<MachineryDetail> findByItemCode(String itemCode);
}
