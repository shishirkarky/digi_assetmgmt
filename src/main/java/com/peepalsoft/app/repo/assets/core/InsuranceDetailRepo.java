package com.peepalsoft.app.repo.assets.core;

import com.peepalsoft.app.entity.asset.InsuranceDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface InsuranceDetailRepo extends JpaRepository<InsuranceDetail, Integer> {
    Optional<InsuranceDetail> findByItemCode(String itemCode);
}
