package com.peepalsoft.app.repo.assets.core;

import com.peepalsoft.app.entity.asset.WarrantyDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WarrantyDetailRepo extends JpaRepository<WarrantyDetail, Integer> {
    Optional<WarrantyDetail> findByItemCode(String itemCode);
}
