package com.peepalsoft.app.repo.assets.core;

import com.peepalsoft.app.entity.asset.coreentities.GeneratorUpsDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GeneratorUpsDetailRepo extends JpaRepository<GeneratorUpsDetail, Integer> {
    Optional<GeneratorUpsDetail> findByItemCode(String itemCode);
}
