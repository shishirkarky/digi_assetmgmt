package com.peepalsoft.app.repo;

import com.peepalsoft.app.entity.institution.OfficeUnit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OfficeUnitRepo extends JpaRepository<OfficeUnit, Integer> {
    List<OfficeUnit> findByRefBranchCode(String refBranchCode);
}
