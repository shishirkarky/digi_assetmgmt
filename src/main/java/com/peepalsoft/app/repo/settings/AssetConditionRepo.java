package com.peepalsoft.app.repo.settings;

import com.peepalsoft.app.entity.settings.AssetCondition;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AssetConditionRepo extends JpaRepository<AssetCondition, Integer> {
}
