package com.peepalsoft.app.repo.settings;

import com.peepalsoft.app.entity.settings.ReceivedSource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReceivedSourceRepo extends JpaRepository<ReceivedSource, Integer> {
}
