package com.peepalsoft.app.repo.settings;

import com.peepalsoft.app.entity.settings.Company;
import com.peepalsoft.app.enums.CompanyTypeEnum;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyRepo extends JpaRepository<Company, Integer> {
    List<Company> findAllByCompanyType(CompanyTypeEnum companyType);
}
