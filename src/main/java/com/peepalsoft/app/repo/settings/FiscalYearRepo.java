package com.peepalsoft.app.repo.settings;

import com.peepalsoft.app.entity.settings.FiscalYear;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FiscalYearRepo extends JpaRepository<FiscalYear, Integer> {
}
