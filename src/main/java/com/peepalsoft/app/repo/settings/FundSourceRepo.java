package com.peepalsoft.app.repo.settings;

import com.peepalsoft.app.entity.settings.FundSource;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FundSourceRepo extends JpaRepository<FundSource, Integer> {
}
