package com.peepalsoft.app.repo.settings;

import com.peepalsoft.app.entity.asset.groups.GroupFour;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupFourRepo extends JpaRepository<GroupFour, Integer> {
}
