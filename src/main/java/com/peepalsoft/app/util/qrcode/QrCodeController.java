package com.peepalsoft.app.util.qrcode;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.OutputStream;

@RestController
@RequestMapping("qr-code")
public class QrCodeController {
    private QrCodeComponent qrCodeComponent;

    @Autowired
    public void setQrCodeComponent(QrCodeComponent qrCodeComponent) {
        this.qrCodeComponent = qrCodeComponent;
    }

    @GetMapping(value = "{value}")
    public void generateQrCode(@PathVariable("value") String value, HttpServletResponse response) throws Exception {
        response.setContentType("image/png");
        OutputStream outputStream = response.getOutputStream();
        outputStream.write(qrCodeComponent.getQRCodeImage(value, 200, 200));
        outputStream.flush();
        outputStream.close();
    }
}
