package com.peepalsoft.app.util;

import com.peepalsoft.app.user.Staffs;
import com.peepalsoft.app.user.repository.StaffsRepo;
import com.peepalsoft.app.user.repository.UsersRepo;
import net.bytebuddy.asm.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Component
public class UserDetailsUtil {
    @Autowired
    private UsersRepo usersRepo;
    @Autowired
    private StaffsRepo staffsRepo;

    public Map<String, String> getStaffCodeAndBranchCode(String username) {
        Map<String, String> map = new HashMap<>();
        usersRepo.findByUsername(username).ifPresent(users -> {
            Assert.notNull(users.getStaffs(), "User Staff cannot be null");
            Assert.notNull(users.getStaffs().getCode(), "User staffCode cannot be null");
            map.put("staffCode", users.getStaffs().getCode());

            Optional<Staffs> staffsOptional = staffsRepo.findByCode(users.getStaffs().getCode());
            Assert.notNull(staffsOptional.isPresent(), "Staff cannot be null");
            staffsOptional.ifPresent(staffs -> {
                Assert.notNull(staffs.getOffice(), "Staffs Office cannot be null");
                map.put("branchCode", staffs.getOffice().getBranchCode());
            });
        });
        return map;
    }
}
