package com.peepalsoft.app.util;

import com.peepalsoft.app.user.Roles;
import com.peepalsoft.app.user.repository.UsersRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

@Service
public class UtilitiesServiceImpl implements UtilitiesService {
    private UsersRepo usersRepo;

    @Autowired
    public void setUsersRepo(UsersRepo usersRepo) {
        this.usersRepo = usersRepo;
    }

    public List<String> currentUserRoles(Principal principal) {
        Collection<Roles> roles = usersRepo.findByUsername(principal.getName()).get().getRoles();
        List<String> rolesList = new ArrayList<>();
        for (Roles role : roles) {
            rolesList.add(role.getName());
        }
        return rolesList;
    }

    public String currentUsername(Principal principal) {
        return principal.getName();
    }

    public String getDigiUUID() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
