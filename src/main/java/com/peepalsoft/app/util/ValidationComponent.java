package com.peepalsoft.app.util;

import com.peepalsoft.app.exception.ValidationErrorException;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

public class ValidationComponent {
    public static void bindingResultValidation(BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ValidationErrorException(bindingResult);
        }
    }
}
