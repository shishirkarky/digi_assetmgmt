package com.peepalsoft.app.util;

import com.peepalsoft.app.user.Roles;
import com.peepalsoft.app.user.repository.UsersRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Slf4j
@Component
public class PSUtilities {
    private UsersRepo usersRepo;

    @Autowired
    public void setUsersRepo(UsersRepo usersRepo) {
        this.usersRepo = usersRepo;
    }

    public List<String> currentUserRoles(Principal principal) {
        Collection<Roles> roles = usersRepo.findByUsername(principal.getName()).get().getRoles();
        List<String> rolesList = new ArrayList<>();
        for (Roles role : roles) {
            rolesList.add(role.getName());
        }
        return rolesList;
    }

    public String dateFormatter(String pattern, Date date) {
        try {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            return simpleDateFormat.format(date);
        } catch (Exception e) {
            log.error("Err...date formatting failure.");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
            return simpleDateFormat.format(new Date());
        }
    }

}
