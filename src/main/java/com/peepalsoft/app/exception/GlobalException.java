package com.peepalsoft.app.exception;

import com.peepalsoft.app.responsemessage.HttpResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;

@RestControllerAdvice
public class GlobalException extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = CustomValidationException.class)
    public ResponseEntity<Object> customValidationException(CustomValidationException customValidationException) {
        List<String> errorsList = customValidationException.errorMessages;
        return new ResponseEntity<>(HttpResponses.validationError(errorsList), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = ProductNotfoundException.class)
    public ResponseEntity<Object> productNotFoundException(ProductNotfoundException customValidationException) {
        return new ResponseEntity<>(HttpResponses.notfound(), HttpStatus.BAD_REQUEST);
    }
}
