package com.peepalsoft.app.entity.bill;

import com.peepalsoft.app.entity.BaseEntity;
import com.peepalsoft.app.entity.settings.Company;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Reference;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Table
@Entity
public class Bill extends BaseEntity {
    private String billDateNp;
    private String billDateEn;
    private String billNumber;
    @Reference(value = Company.class)
    private int refCompanyId;
    private String billUrl;
}
