package com.peepalsoft.app.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "transaction")
public class Transaction extends BaseEntity{
    private String transactionId;
    private double drAmount;
    private double crAmount;
    private String subItemCode;
}
