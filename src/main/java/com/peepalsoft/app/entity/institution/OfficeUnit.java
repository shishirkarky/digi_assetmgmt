package com.peepalsoft.app.entity.institution;

import com.peepalsoft.app.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Reference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Table(name = "office_unit")
@Entity
public class OfficeUnit extends BaseEntity {
    @Column(unique = true)
    @NotEmpty
    @NotNull
    private String code;
    private String roomNo; //todo: remove this
    @NotEmpty
    @NotNull
    @Size(min = 2,max = 30)
    private String name;
    @NotNull
    @NotEmpty
    private String refBranchCode;
}
