package com.peepalsoft.app.entity.settings;

import com.peepalsoft.app.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "md_fund_source")
public class FundSource extends BaseEntity {

    @NotNull
    @NotEmpty
    @Size(min = 2, max = 30)
    @Column(unique = true)
    private String name;
}
