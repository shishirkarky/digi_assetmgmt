package com.peepalsoft.app.entity.settings;

import com.peepalsoft.app.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@Table(name = "md_received_source")
@Entity
public class ReceivedSource extends BaseEntity {
    @NotEmpty
    @NotNull
    @Size(min = 2,max = 30)
    private String name;
}
