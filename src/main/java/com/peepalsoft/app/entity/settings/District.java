package com.peepalsoft.app.entity.settings;

import com.peepalsoft.app.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "md_district")
public class District extends BaseEntity {
    @Column(unique = true)
    private int code;
    private String name;
}
