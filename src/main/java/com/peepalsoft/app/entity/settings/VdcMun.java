package com.peepalsoft.app.entity.settings;

import com.peepalsoft.app.entity.BaseEntity;
import lombok.Data;
import org.springframework.data.annotation.Reference;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "md_vdc_mun")
public class VdcMun extends BaseEntity {
    @Column(unique = true)
    private String code;
    private String name;
    private int wardNum;
    @Reference(value = District.class)
    private String refDistrictCode;
}
