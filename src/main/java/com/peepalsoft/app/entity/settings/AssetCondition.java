package com.peepalsoft.app.entity.settings;

import com.peepalsoft.app.entity.BaseEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Table(name = "md_asset_condition")
@Entity
@Getter
@Setter
public class AssetCondition extends BaseEntity {
    @NotNull
    @NotEmpty
    @Column(unique = true)
    private String name;
}
