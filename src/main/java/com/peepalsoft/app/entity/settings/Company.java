package com.peepalsoft.app.entity.settings;

import com.peepalsoft.app.entity.BaseEntity;
import com.peepalsoft.app.enums.CompanyTypeEnum;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "md_company")
public class Company extends BaseEntity {
    @NotEmpty
    @NotNull
    @Size(min = 2, max = 30)
    private String vatPan;
    @NotEmpty
    @NotNull
    @Size(min = 2, max = 30)
    private String name;
    private String address;
    private String contactNumber;
    private String email;
    private String contactPerson;
    private CompanyTypeEnum companyType;
}
