package com.peepalsoft.app.entity.settings;

import com.peepalsoft.app.entity.BaseEntity;
import com.peepalsoft.app.enums.YesNoEnum;
import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "md_fiscal_year")
public class FiscalYear extends BaseEntity {
    private String name;
    private String startDateEn;
    private String startDateNp;
    private String endDateEn;
    private String endDateNp;
    @Enumerated(EnumType.STRING)
    private YesNoEnum closed = YesNoEnum.NO;
    @Enumerated(EnumType.STRING)
    private YesNoEnum depreciationCalculated = YesNoEnum.NO;
}
