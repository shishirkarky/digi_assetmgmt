package com.peepalsoft.app.entity.asset.coreentities;

import com.peepalsoft.app.entity.asset.BaseCoreAssetEntity;
import com.peepalsoft.app.entity.asset.groups.GroupFour;
import com.peepalsoft.app.enums.AssetCurrentConditionEnum;
import com.peepalsoft.app.enums.AssetTypeEnum;
import com.peepalsoft.app.enums.StatusEnum;
import com.peepalsoft.app.user.Office;
import com.peepalsoft.app.user.Staffs;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Reference;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Getter
@Setter
@Table(name = "crr_asset_entity")
@Entity
public class CoreAssetEntity extends BaseCoreAssetEntity {
    /**
     * transactionId    unique identifier for every new asset
     * assetType        defines the type of asset, further which entity/collection to look into for this asset details
     * staffCode        staff whom this asset is assigned to
     * officeId         office/branch where this asset is present
     * currentAssetCondition    current condition of asset
     * refGroupFourId   group is not currently used, further this will be associated with depreciation %
     */
    private String transactionId;
    @Reference(AssetTypeEnum.class)
    @Enumerated(EnumType.STRING)
    private AssetTypeEnum assetType;
    @Reference(value = Staffs.class)
    private String staffCode;
    @Reference(value = Office.class)
    private String refBranchCode;
    @Enumerated(EnumType.STRING)
    private AssetCurrentConditionEnum currentAssetCondition = AssetCurrentConditionEnum.NEW;
    @Reference(GroupFour.class)
    private int refGroupFourId;
    @Enumerated(EnumType.STRING)
    private StatusEnum status = StatusEnum.CREATED;
}
