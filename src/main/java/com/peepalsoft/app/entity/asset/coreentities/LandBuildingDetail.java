package com.peepalsoft.app.entity.asset.coreentities;

import com.peepalsoft.app.enums.AreaUnitEnum;
import com.peepalsoft.app.enums.ConstructionTypeEnum;
import com.peepalsoft.app.enums.YesNoEnum;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "dt_land_building")
public class LandBuildingDetail extends BaseLandDetailAsset {
    @Enumerated(EnumType.STRING)
    private AreaUnitEnum buildingAreaUnit;
    private double buildingArea;
    private String constructionDateNp;
    private String constructionDateEn;
    @Enumerated(EnumType.STRING)
    private ConstructionTypeEnum constructionType;
    @Enumerated(EnumType.STRING)
    private YesNoEnum mapApproved;
    private String mapApprovedDateEn;
    private String mapApprovedDateNp;
}
