package com.peepalsoft.app.entity.asset;

import com.peepalsoft.app.entity.BaseEntity;
import com.peepalsoft.app.entity.settings.AssetCondition;
import com.peepalsoft.app.enums.AssetCurrentConditionEnum;
import com.peepalsoft.app.enums.StatusEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Reference;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "sub_assets")
public class SubAssets extends BaseEntity {
    private String itemCode;
    @Column(unique = true)
    private String subItemCode;
    @Reference(AssetCondition.class)
    private String refAssetConditionId;
    @Enumerated(EnumType.STRING)
    private StatusEnum status = StatusEnum.CREATED;
    @Enumerated(EnumType.STRING)
    private AssetCurrentConditionEnum currentAssetCondition;
    @Column(length = 1)
    private int deleteStatus;

    private String branchCode;
    private String staffCode;
}
