package com.peepalsoft.app.entity.asset;

import com.peepalsoft.app.entity.asset.BaseCoreAssetEntity;
import com.peepalsoft.app.entity.settings.Company;
import lombok.Data;
import org.springframework.data.annotation.Reference;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Software, hardware, Motor vehicle, Generator UPS have AMC
 * while saving the above categories amc is also updated.
 */
@Data
@Entity
@Table
public class AmcDetail extends BaseCoreAssetEntity {
    private String amcStartDateEn;
    private String amcStartDateNp;
    private String amcEndDateEn;
    private String amcEndDateNp;
    private double amcCost;
    @Reference(value = Company.class)
    private int amcRefCompanyId;
}
