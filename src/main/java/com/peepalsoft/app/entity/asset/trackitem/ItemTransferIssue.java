package com.peepalsoft.app.entity.asset.trackitem;

import com.peepalsoft.app.entity.BaseEntity;
import com.peepalsoft.app.entity.asset.coreentities.CoreAssetEntity;
import com.peepalsoft.app.enums.StatusEnum;
import com.peepalsoft.app.user.Office;
import com.peepalsoft.app.user.Staffs;
import lombok.Data;
import org.springframework.data.annotation.Reference;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

/**
 * this entity holds data of item in transition state
 * transition state: when user transfer or issue item the data replicates in this entity
 * and on approval or cancel, this item gets deleted from this entity
 */
@Data
@Entity
@Table
public class ItemTransferIssue extends BaseEntity {
    @Reference(value = CoreAssetEntity.class)
    private String itemCode;
    @Reference(value = Staffs.class)
    private String fromStaffCode;
    @Reference(value = Staffs.class)
    private String toStaffCode;
    @Reference(value = Office.class)
    private String fromBranchCode;
    @Reference(value = Office.class)
    private String toBranchCode;

    @Enumerated(EnumType.STRING)
    private StatusEnum status;

}
