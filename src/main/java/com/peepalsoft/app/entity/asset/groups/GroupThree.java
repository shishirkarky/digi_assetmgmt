package com.peepalsoft.app.entity.asset.groups;

import com.peepalsoft.app.entity.BaseEntity;
import lombok.Data;

import javax.persistence.*;

/**
 * Sub group
 */
@Data
@Table
@Entity
public class GroupThree extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String code;
    private String name;
    private int groupTwoId;
}
