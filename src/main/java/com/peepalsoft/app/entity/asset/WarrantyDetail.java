package com.peepalsoft.app.entity.asset;

import com.peepalsoft.app.entity.asset.BaseCoreAssetEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table
public class WarrantyDetail extends BaseCoreAssetEntity {
    private String warrantyStartDateNp;
    private String warrantyStartDateEn;
    private String warrantyEndDateNp;
    private String warrantyEndDateEn;
}
