package com.peepalsoft.app.entity.asset.coreentities;

import com.peepalsoft.app.entity.asset.BaseCoreAssetEntity;
import com.peepalsoft.app.entity.settings.AssetCondition;
import com.peepalsoft.app.entity.settings.Company;
import com.peepalsoft.app.entity.settings.FundSource;
import com.peepalsoft.app.entity.settings.ReceivedSource;
import com.peepalsoft.app.enums.PurchaseTypeEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Reference;

import javax.persistence.*;

@Getter
@Setter
@Table(name = "crr_asset_detail")
@Entity
public class AssetDetail extends BaseCoreAssetEntity {
    private String legacyId;
    private String itemName;
    private String itemModel;
    @Reference(value = ReceivedSource.class)
    private int refReceivedSourceId;
    @Reference(value = FundSource.class)
    private int refFundSourceId;
    private String decisionDateEn;
    private String decisionDateNp;
    private String purchaseDateNp;
    private String purchaseDateEn;
    @Enumerated(EnumType.STRING)
    private PurchaseTypeEnum purchaseType;
    private String billNumber;
    @Reference(AssetCondition.class)
    private String refAssetConditionId;
    private String remarks;
    private double depreciationRate;
    @Reference(value = Company.class)
    private int refSupplierId;
    private int quantity;
    private double rate;
    private double amount;
    @Column(length = 1)
    private int deleteStatus;
}
