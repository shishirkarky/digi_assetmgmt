package com.peepalsoft.app.entity.asset.coreentities;

import com.peepalsoft.app.entity.asset.BaseCoreAssetEntity;
import com.peepalsoft.app.enums.YesNoEnum;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "dt_hardware")
public class HardwareDetail extends BaseCoreAssetEntity {
    private String licenseNumber;
    private String licenseExpiryDateEn;
    private String licenseExpiryDateNp;
    @Enumerated(EnumType.STRING)
    private YesNoEnum securityTested;
    private String macAddress;
}
