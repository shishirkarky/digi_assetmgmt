package com.peepalsoft.app.entity.asset;

import com.peepalsoft.app.entity.BaseEntity;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@Data
@MappedSuperclass
public class BaseCoreAssetEntity extends BaseEntity {
    @Column(unique = true)
    private String itemCode;
}
