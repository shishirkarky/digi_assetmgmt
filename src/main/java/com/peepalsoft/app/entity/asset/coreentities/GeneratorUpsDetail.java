package com.peepalsoft.app.entity.asset.coreentities;

import com.peepalsoft.app.entity.asset.BaseCoreAssetEntity;
import com.peepalsoft.app.enums.FuelTypeEnum;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "dt_generator_ups")
public class GeneratorUpsDetail extends BaseCoreAssetEntity {
    @Enumerated(EnumType.STRING)
    private FuelTypeEnum fuelType;
    private String capacity;
}
