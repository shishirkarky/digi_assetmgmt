package com.peepalsoft.app.entity.asset.coreentities;

import com.peepalsoft.app.entity.asset.BaseCoreAssetEntity;
import com.peepalsoft.app.entity.settings.District;
import com.peepalsoft.app.entity.settings.VdcMun;
import com.peepalsoft.app.enums.AreaUnitEnum;
import com.peepalsoft.app.enums.UsedStatusEnum;
import lombok.Data;
import org.springframework.data.annotation.Reference;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "dt_land")
public class LandDetail extends BaseLandDetailAsset {
}
