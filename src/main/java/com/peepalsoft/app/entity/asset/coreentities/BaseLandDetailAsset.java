package com.peepalsoft.app.entity.asset.coreentities;

import com.peepalsoft.app.entity.asset.BaseCoreAssetEntity;
import com.peepalsoft.app.entity.settings.District;
import com.peepalsoft.app.entity.settings.VdcMun;
import com.peepalsoft.app.enums.AreaUnitEnum;
import com.peepalsoft.app.enums.UsedStatusEnum;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Reference;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

@Getter
@Setter
@MappedSuperclass
public class BaseLandDetailAsset extends BaseCoreAssetEntity {
    @Reference(value = District.class)
    private int refDistrictId;
    @Reference(value = VdcMun.class)
    private int refVdcMunId;
    private int wardNumber;
    private String legacyVdcMun;
    private int legacyWardNumber;
    private String tole;
    private String longitude;
    private String latitude;
    @Enumerated(EnumType.STRING)
    private AreaUnitEnum landSizeUnit;
    private double landSize;
    private String plotNumber;
    private String mothNumber;
    private String sheetNumber;
    @Enumerated(EnumType.STRING)
    private UsedStatusEnum usedStatus;
}
