package com.peepalsoft.app.entity.asset;

import com.peepalsoft.app.entity.asset.BaseCoreAssetEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Motor Vehicle, Land and Building, Land Has tax
 */
@Data
@Entity
@Table
public class TaxDetail extends BaseCoreAssetEntity {
    private int taxPaidYear;
    private String taxPaidDateEn;
    private String taxPaidDateNp;
    private double taxAmount;
    private String dueDateEn;
    private String dueDateNp;

}
