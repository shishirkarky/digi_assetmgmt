package com.peepalsoft.app.entity.asset.coreentities;

import com.peepalsoft.app.entity.asset.BaseCoreAssetEntity;
import com.peepalsoft.app.entity.asset.InsuranceDetail;
import lombok.Data;
import org.springframework.data.annotation.Reference;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "dt_software")
public class SoftwareDetail extends BaseCoreAssetEntity {
    private String licenseNumber;
    private String licenseExpiryDateEn;
    private String licenseExpiryDateNp;
    @Reference(InsuranceDetail.class)
    private int refInsuranceDetailId;
}
