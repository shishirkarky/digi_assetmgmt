package com.peepalsoft.app.entity.asset.coreentities;

import com.peepalsoft.app.entity.asset.BaseCoreAssetEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "dt_machinery")
public class MachineryDetail extends BaseCoreAssetEntity {
    private String type;
}
