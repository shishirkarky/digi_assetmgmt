package com.peepalsoft.app.entity.asset;

import com.peepalsoft.app.entity.asset.BaseCoreAssetEntity;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Land building, Software, hardware, Motor vehicle, Generator UPS have insurance
 * while saving above categories insurance is updated
 */
@Data
@Entity
@Table
public class InsuranceDetail extends BaseCoreAssetEntity {
    private String insuranceStartDateNp;
    private String insuranceStartDateEn;
    private String insuranceEndDateNp;
    private String insuranceEndDateEn;
    private double insuredAmount;
    private double premiumAmount;
}
