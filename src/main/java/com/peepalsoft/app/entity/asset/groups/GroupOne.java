package com.peepalsoft.app.entity.asset.groups;

import com.peepalsoft.app.entity.BaseEntity;
import lombok.Data;

import javax.persistence.*;

/**
 * Main group
 */
@Data
@Table
@Entity
public class GroupOne extends BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String code;
    private String name;
}
