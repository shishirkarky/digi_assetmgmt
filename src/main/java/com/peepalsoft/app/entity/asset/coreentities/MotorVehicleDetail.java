package com.peepalsoft.app.entity.asset.coreentities;

import com.peepalsoft.app.entity.asset.BaseCoreAssetEntity;
import com.peepalsoft.app.enums.VehicleTypeEnum;
import com.peepalsoft.app.enums.WheelTypeEnum;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "dt_motor_vehicle")
public class MotorVehicleDetail extends BaseCoreAssetEntity {
    @Enumerated(EnumType.STRING)
    private VehicleTypeEnum vehicleType;
    @Enumerated(EnumType.STRING)
    private WheelTypeEnum wheelType;
    private String vehicleNumber;
    private String chassisNumber;
    private String engineNumber;
    private String workingCondition;
}
