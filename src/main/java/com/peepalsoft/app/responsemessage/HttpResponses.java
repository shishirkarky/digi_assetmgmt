package com.peepalsoft.app.responsemessage;

import org.springframework.http.HttpStatus;

import java.util.List;

public class HttpResponses {
    private HttpResponses() {
    }

    public static ResponseMessage created(Object object) {
        ResponseMessage m = new ResponseMessage();
        m.setStatus(true);
        m.setStatusCode("201");
        m.setMessage(HttpStatus.CREATED.getReasonPhrase());
        m.setData(object);
        return m;
    }

    public static ResponseMessage accepted() {
        ResponseMessage m = new ResponseMessage();
        m.setStatus(true);
        m.setStatusCode("202");
        m.setMessage(HttpStatus.ACCEPTED.getReasonPhrase());
        return m;
    }

    public static ResponseMessage badRequest() {
        ResponseMessage m = new ResponseMessage();
        m.setStatus(false);
        m.setStatusCode("400");
        m.setMessage(HttpStatus.BAD_REQUEST.getReasonPhrase());
        return m;
    }

    public static ResponseMessage passwordMismatch() {
        ResponseMessage m = new ResponseMessage();
        m.setStatus(false);
        m.setStatusCode("");
        m.setMessage("Passwords Not Matched.");
        return m;
    }

    public static ResponseMessage invalidPassword() {
        ResponseMessage m = new ResponseMessage();
        m.setStatus(false);
        m.setStatusCode("");
        m.setMessage("Invalid Password.");
        return m;
    }

    public static ResponseMessage fetched(Object object) {
        ResponseMessage m = new ResponseMessage();
        m.setStatus(true);
        m.setStatusCode("302");
        m.setData(object);
        m.setMessage(HttpStatus.FOUND.getReasonPhrase());
        return m;
    }

    public static ResponseMessage notfound() {
        ResponseMessage m = new ResponseMessage();
        m.setStatus(false);
        m.setStatusCode("404");
        m.setMessage(HttpStatus.NOT_FOUND.getReasonPhrase());
        return m;
    }

    public static ResponseMessage received() {
        ResponseMessage m = new ResponseMessage();
        m.setStatus(true);
        m.setStatusCode("202");
        m.setMessage(HttpStatus.ACCEPTED.getReasonPhrase());
        return m;
    }

    public static ResponseMessage validationError(List<String> errors) {
        ResponseMessage m = new ResponseMessage();
        m.setStatus(false);
        m.setStatusCode("422");
        m.setMessage(HttpStatus.UNPROCESSABLE_ENTITY.getReasonPhrase());
        m.setErrors(errors);
        return m;
    }
}
