package com.peepalsoft.app.responsemessage;

import com.peepalsoft.app.config.PeepalSoftConstants;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class ResponseMessage {
    private String copyright = PeepalSoftConstants.COPYRIGHT;
    private String statusCode;
    private boolean status;
    private String message;
    private Object data;
    private List<String> errors;
}
