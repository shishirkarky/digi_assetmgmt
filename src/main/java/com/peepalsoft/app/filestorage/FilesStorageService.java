/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* .
*
*/
package com.peepalsoft.app.filestorage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;


public interface FilesStorageService {
    void init();

    void save(MultipartFile file, String fileName);

    Resource load(String filename);

    void deleteAll();

    Stream<Path> loadAll();

    Optional<Documents> saveDocuments(Documents documents);

    Optional<Documents> createDocuments(MultipartFile file, FileInfo fileInfo);

    List<Documents> findDocuments(String type, int refId);

    Optional<Documents> findDocuments(String type, String subType, int refId);
}
