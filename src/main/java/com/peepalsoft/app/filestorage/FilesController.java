/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * .
 */
package com.peepalsoft.app.filestorage;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.peepalsoft.app.responsemessage.HttpResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("impl/api/v1")
public class FilesController {
    private FilesStorageService filesStorageService;
    private DocumentsDtoAssembler documentsDtoAssembler;

    @Autowired
    public void setFilesStorageService(FilesStorageService filesStorageService) {
        this.filesStorageService = filesStorageService;
    }

    @Autowired
    public void setDocumentsDtoAssembler(DocumentsDtoAssembler documentsDtoAssembler) {
        this.documentsDtoAssembler = documentsDtoAssembler;
    }

    @PostMapping("/uploads")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file, @RequestParam(value = "jsondata") String jsonData) {
        log.info("Processing file upload {}", jsonData);
        try {
            FileInfo fileInfo = new ObjectMapper().readValue(jsonData, FileInfo.class);
            return new ResponseEntity<>(HttpResponses.created(filesStorageService.createDocuments(file, fileInfo).get()), HttpStatus.OK);
        } catch (Exception e) {
            log.error("Exception in file upload ", e);
            return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.OK);
        }
    }

    @GetMapping("/uploads/files")
    public ResponseEntity<List<FileInfo>> getListFiles() {
        List<FileInfo> fileInfos = filesStorageService.loadAll().map(path -> {
            String filename = path.getFileName().toString();
            String url = MvcUriComponentsBuilder
                    .fromMethodName(FilesController.class, "getFile", path.getFileName().toString()).build().toString();

            return new FileInfo(filename, url);
        }).collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
    }

    @GetMapping("/uploads/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = filesStorageService.load(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @GetMapping("/uploads/documents/{type}/{refId}")
    @ResponseBody
    public ResponseEntity<?> getDocuments(@PathVariable String type, @PathVariable int refId) {
        List<Documents> documents = filesStorageService.findDocuments(type, refId);
        return new ResponseEntity<>(HttpResponses.fetched(documentsDtoAssembler.toModels(documents)), HttpStatus.OK);
    }
}
