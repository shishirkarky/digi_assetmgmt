package com.peepalsoft.app.config;

public class PeepalSoftConstants {
    public static final String COPYRIGHT = "Peepalsoft";
    public static final String PAGE_TITLE = "pagetitle";
    public static final String BILL_UPLOAD_PATH = "/home/shishir/digi/uploadPathTest/";
    public static final String BILL_IMAGE_PREFIX = "BILL_";
    public static final String UPLOAD_IMAGE_EXTENSION = ".png";
}
