package com.peepalsoft.app.config;

public class ErrorMessageConstants {
    public static final String INVALID_REQUEST = "Invalid request.";
    public static final String UNABLE_TO_SAVE = "Unable to save.";
    public static final String REQUEST_FILE_NOT_FOUND = "Request file not found.";
    public static final String REQUEST_ID_IS_INVALID = "Request id is invalid.";
    public static final String IO_EXCEPTION = "IO exception.";
    public static final String BILL_NOT_FOUND = "Bill not found.";
    public static final String ASSET_NOT_FOUND = "Asset not found.";
    public static final String INVALID_ASSET_TYPE = "Invalid asset type.";
}
