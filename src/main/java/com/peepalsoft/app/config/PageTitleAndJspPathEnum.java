package com.peepalsoft.app.config;

public enum PageTitleAndJspPathEnum {
    ASSETS_DASHBOARD("ASSETS DASHBOARD", "asset/assetDashboard"),
    ASSETS_DETAIL_VIEW_PAGE("ASSETS DETAIL", "asset/assetsDetailView"),
    SUB_ASSETS_DETAIL_VIEW_PAGE("SUB-ASSETS DETAIL", "asset/subassets"),
    ASSETS_MOTOR_VEHICLE_VIEW_PAGE("MOTOR VEHICLE DETAIL", "asset/motor_vehicle/view"),
    ASSETS_LAND_VIEW_PAGE("LAND DETAIL", "asset/land/view"),
    ASSETS_LAND_BUILDING_VIEW_PAGE("LAND BUILDING DETAIL", "asset/land_and_building/view"),
    ASSETS_GENERATOR_UPS_CREATE("GENERATOR UPS", "asset/generatorups/create"),
    ASSETS_HARDWARE_CREATE("HARDWARE", "asset/hardware/create"),
    ASSETS_LAND_CREATE("LAND", "asset/land/create"),
    ASSETS_LAND_BUILDING_CREATE("LAND AND BUILDING", "asset/land_and_building/create"),
    ASSETS_MACHINERY_CREATE("MACHINERY", "asset/machinery/create"),
    ASSETS_MOTOR_VEHICLE_CREATE("MOTOR VEHICLE", "asset/motor_vehicle/create"),
    ASSETS_SOFTWARE_CREATE("SOFTWARE", "asset/software/create"),
    ASSETS_OTHER_CREATE("GENERAL ASSET", "asset/other/create"),
    ASSETS_VIEW("ASSETS VIEW", "asset/view"),
    ASSETS_OTHER_WARRANTY_CREATE("WARRANTY", "asset/other_details/warranty/create"),
    ASSETS_OTHER_INSURANCE_CREATE("INSURANCE", "asset/other_details/insurance/create"),
    ASSETS_OTHER_AMC_CREATE("AMC", "asset/other_details/amc/create"),
    ASSETS_OTHER_TAX_CREATE("AMC", "asset/other_details/tax/create"),
    TRANSFER_ISSUE_VIEW("TRANSFER/ISSUE LOG", "transferIssue/view"),
    TRANSFER_PENDING_VIEW("PENDING TRANSFERS", "transferIssue/pendingtransfers"),
    ISSUE_CREATE("ISSUE", "transferIssue/issues"),
    TRANSFER_CREATE("TRANSFER", "transferIssue/transfers"),
    BILL_CREATE("BILL CREATE", "bill/create"),
    BILL_VIEW("BILL VIEW", "bill/view"),
    SETTINGS_CREATE("SETTINGS", "settings/create"),
    FISCAL_YEAR_CREATE("FISCAL YEAR", "settings/fiscalYear");

    public String pageTitle;
    public String jspPath;

    PageTitleAndJspPathEnum(String pageTitle, String jspPath) {
        this.jspPath = jspPath;
        this.pageTitle = pageTitle;
    }
}
