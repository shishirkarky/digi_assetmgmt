package com.peepalsoft.app.config;

/**
 * This class contains the api paths for all the created apis.
 * The static variables are used every where in controllers.
 */
public class ApiPaths {
    private ApiPaths() {
        throw new IllegalStateException("IllegalStateException: ApiPaths class");
    }

    /**
     * settings api paths
     */
    public static final String SETTINGS = "settings";
    /**
     * company settings api paths
     */
    public static final String SETTINGS_CREATE_PAGE = "/create-page";
    public static final String COMPANY_C = "/company";
    public static final String COMPANY_R_ALL = "/company/get-all";
    public static final String COMPANY_R_BY_ID = "/company";
    public static final String COMPANY_R_BY_TYPE = "/company/type/{companyType}";
    /**
     * fund source settings api paths
     */
    public static final String FUND_SOURCE_C = "/fund-source";
    public static final String FUND_SOURCE_R_ALL = "/fund-source/get-all";
    public static final String FUND_SOURCE_R_BY_ID = "/fund-source";
    /**
     * received source settings api paths
     */
    public static final String RECEIVED_SOURCE_C = "/received-source";
    public static final String RECEIVED_SOURCE_R_ALL = "/received-source/get-all";
    public static final String RECEIVED_SOURCE_R_BY_ID = "/received-source";
    /**
     * group settings api paths
     */
    public static final String GROUP_C = "/group";
    public static final String GROUP_R_ALL = "/group/groups";
    public static final String GROUP_R_BY_ID = "/group";
    /**
     * asset condition api paths
     */
    public static final String ASSET_CONDITION_C = "/asset-condition";
    public static final String ASSET_CONDITION_R_ALL = "/asset-condition/asset-conditions";
    /**
     * Assets api paths
     */
    public static final String ASSETS = "assets";
    public static final String ASSETS_WARRANTY = "/warranty";
    public static final String ASSETS_INSURANCE = "/insurance";
    public static final String ASSETS_AMC = "/amc";
    public static final String ASSETS_TAX = "/tax";
    public static final String ASSETS_FIND_BY_ITEM_CODE = "{itemCode}";
    public static final String ASSETS_C = "/save";
    public static final String ASSETS_R = "/get-all";
    public static final String ASSETS_FIND_BY_ASSET_TYPE = "/get-by-asset-type/{assetType}";

    public static final String SUB_ASSETS_R = "/subassets";
    /**
     * Transfer Issue api paths
     */

    public static final String TRANSFER_ISSUE = "/transfer-issue";
    public static final String TRANSFER_ISSUE_VIEW_PAGE = "/view-page";
    public static final String PENDING_TRANSFER_ISSUE_VIEW_PAGE = "/transfers/pending/view-page";
    public static final String TRANSFER_CREATE_PAGE = "/transfers/create-page";
    public static final String ISSUE_CREATE_PAGE = "/issues/create-page";
    public static final String ISSUE_ITEMS = "/issues";
    public static final String TRANSFER_ITEMS = "/transfers";
    public static final String TRANSFER_APPROVE_ITEMS = "/transfers/items/{itemCode}/approves";
    public static final String TRANSFER_DISAPPROVE_ITEMS = "/transfers/items/{itemCode}/disapproves";
    public static final String PENDING_TRANSFER_ITEMS = "/transfers/pending";
    /**
     * Bill api paths
     */
    public static final String BILL = "bill";
    public static final String BILL_CREATE_PAGE = "/create-page";
    public static final String BILL_VIEW_PAGE = "/view-page";
    public static final String BILL_R = "/bills";
    /**
     * Assets jsp page api paths
     */
    public static final String ASSETS_DASHBOARD_PAGE = "/dashboard";
    public static final String ASSETS_DETAIL_VIEW_PAGE = "/detail";
    public static final String ASSETS_COUNT = "/count";
    public static final String ASSETS_VIEW_PAGE = "/view-page";
    public static final String SUB_ASSETS_VIEW_PAGE = "/subassets/view-page";
    public static final String ASSETS_GENERATOR_UPS_CREATE_PAGE = "/generatorups/create-page";
    public static final String ASSETS_HARDWARE_CREATE_PAGE = "/hardware/create-page";
    public static final String ASSETS_LAND_CREATE_PAGE = "/land/create-page";
    public static final String ASSETS_LAND_AND_BUILDING_CREATE_PAGE = "/land_and_building/create-page";
    public static final String ASSETS_MACHINERY_CREATE_PAGE = "/machinery/create-page";
    public static final String ASSETS_MOTOR_VEHICLE_CREATE_PAGE = "/motor_vehicle/create-page";
    public static final String ASSETS_OTHER_CREATE_PAGE = "/other/create-page";
    public static final String ASSETS_SOFTWARE_CREATE_PAGE = "/software/create-page";
    public static final String ASSETS_WARRANTY_CREATE_PAGE = "/warranty/create-page";
    public static final String ASSETS_INSURANCE_CREATE_PAGE = "/insurance/create-page";
    public static final String ASSETS_AMC_CREATE_PAGE = "/amc/create-page";
    public static final String ASSETS_TAX_CREATE_PAGE = "/tax/create-page";

    /**
     * Hardware api paths
     */
    public static final String OTHER_DETAIL_C = "/other-detail";
    public static final String OTHER_DETAIL_R_ALL = "/other-detail/get-all";
    public static final String OTHER_DETAIL_R_BY_ID = "/other-detail";

    /**
     * Fiscal year api paths
     */
    public static final String FISCAL_YEAR = "/fiscalYears";
    public static final String FISCAL_YEAR_CREATE_PAGE = "/create-page";
}
