package com.peepalsoft.app.dto;

import com.peepalsoft.app.entity.settings.FundSource;
import lombok.Data;

@Data
public class FundSourceFormDto {
    private FundSource fundSource;
}
