package com.peepalsoft.app.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.peepalsoft.app.entity.asset.coreentities.AssetDetail;
import com.peepalsoft.app.entity.asset.coreentities.LandBuildingDetail;
import com.peepalsoft.app.entity.asset.coreentities.LandDetail;
import com.peepalsoft.app.entity.asset.coreentities.MotorVehicleDetail;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssetsTableDto {
    private List<AssetDetail> assetDetail;
    private List<LandBuildingDetail> landBuildingDetail;
    private List<LandDetail> landDetail;
    private List<MotorVehicleDetail> motorVehicleDetail;
}
