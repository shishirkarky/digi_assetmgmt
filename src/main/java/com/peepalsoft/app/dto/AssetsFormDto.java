package com.peepalsoft.app.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.peepalsoft.app.entity.asset.*;
import com.peepalsoft.app.entity.asset.coreentities.*;
import com.peepalsoft.app.enums.AssetTypeEnum;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AssetsFormDto {
    private CoreAssetEntity coreDetail;
    private AssetDetail assetDetail;
    private GeneratorUpsDetail generatorUpsDetail;
    private HardwareDetail hardwareDetail;
    private LandBuildingDetail landBuildingDetail;
    private LandDetail landDetail;
    private MachineryDetail machineryDetail;
    private MotorVehicleDetail motorVehicleDetail;
    private SoftwareDetail softwareDetail;
    private AmcDetail amcDetail;
    private InsuranceDetail insuranceDetail;
    private WarrantyDetail warrantyDetail;
    private TaxDetail taxDetail;
    private AssetTypeEnum assetType;
    private String itemCode;
    private String principalUsername;
    private List<SubAssets> subAssets;
}
