package com.peepalsoft.app.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import java.util.List;

@Getter
@Setter
public class IssueDto {
    @NotEmpty
    private List<String> itemCodes;
    @NotEmpty
    private String toStaffCode;
}
