package com.peepalsoft.app.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StaffsSelectDto {
    private String code;
    private String firstName;
    private String lastName;
}
