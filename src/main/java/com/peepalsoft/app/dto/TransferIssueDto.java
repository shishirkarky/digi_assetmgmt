package com.peepalsoft.app.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class TransferIssueDto {
    @NotEmpty
    private List<String> itemCodes;
    @NotEmpty
    private String toStaffCode;
    private String toBranchCode;
}
