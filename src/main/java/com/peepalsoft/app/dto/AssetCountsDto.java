package com.peepalsoft.app.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AssetCountsDto {
    private int generalCount;
    private int generatorUpsCount;
    private int landCount;
    private int landBuildingCount;
    private int buildingCount;
    private int hardwareCount;
    private int softwareCount;
    private int motorVehicleCount;
    private int machineryCount;
}
