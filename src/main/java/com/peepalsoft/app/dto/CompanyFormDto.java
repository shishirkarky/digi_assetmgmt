package com.peepalsoft.app.dto;

import com.peepalsoft.app.entity.settings.Company;
import lombok.Data;

@Data
public class CompanyFormDto {
    private Company company;
}
