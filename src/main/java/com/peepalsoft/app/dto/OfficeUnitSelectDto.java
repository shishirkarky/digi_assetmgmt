package com.peepalsoft.app.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OfficeUnitSelectDto {
    private int id;
    private String code;
    private String name;
}
