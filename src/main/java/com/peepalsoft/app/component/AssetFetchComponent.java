package com.peepalsoft.app.component;

import com.peepalsoft.app.config.ErrorMessageConstants;
import com.peepalsoft.app.dto.AssetsFormDto;
import com.peepalsoft.app.entity.asset.coreentities.*;
import com.peepalsoft.app.enums.AssetTypeEnum;
import com.peepalsoft.app.repo.assets.core.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@Slf4j
public class AssetFetchComponent {
    private CoreAssetEntityRepo coreAssetEntityRepo;
    private AssetDetailRepo assetDetailRepo;
    private LandDetailRepo landDetailRepo;
    private LandBuildingDetailRepo landBuildingDetailRepo;
    private GeneratorUpsDetailRepo generatorUpsDetailRepo;
    private HardwareDetailRepo hardwareDetailRepo;
    private MachineryDetailRepo machineryDetailRepo;
    private MotorVehicleDetailRepo motorVehicleDetailRepo;
    private SoftwareDetailRepo softwareDetailRepo;

    @Autowired
    public void setAssetDetailRepo(AssetDetailRepo assetDetailRepo) {
        this.assetDetailRepo = assetDetailRepo;
    }

    @Autowired
    public void setCoreAssetEntityRepo(CoreAssetEntityRepo coreAssetEntityRepo) {
        this.coreAssetEntityRepo = coreAssetEntityRepo;
    }

    @Autowired
    public void setLandDetailRepo(LandDetailRepo landDetailRepo) {
        this.landDetailRepo = landDetailRepo;
    }

    @Autowired
    public void setLandBuildingDetailRepo(LandBuildingDetailRepo landBuildingDetailRepo) {
        this.landBuildingDetailRepo = landBuildingDetailRepo;
    }

    @Autowired
    public void setGeneratorUpsDetailRepo(GeneratorUpsDetailRepo generatorUpsDetailRepo) {
        this.generatorUpsDetailRepo = generatorUpsDetailRepo;
    }

    @Autowired
    public void setHardwareDetailRepo(HardwareDetailRepo hardwareDetailRepo) {
        this.hardwareDetailRepo = hardwareDetailRepo;
    }

    @Autowired
    public void setMachineryDetailRepo(MachineryDetailRepo machineryDetailRepo) {
        this.machineryDetailRepo = machineryDetailRepo;
    }

    @Autowired
    public void setMotorVehicleDetailRepo(MotorVehicleDetailRepo motorVehicleDetailRepo) {
        this.motorVehicleDetailRepo = motorVehicleDetailRepo;
    }

    @Autowired
    public void setSoftwareDetailRepo(SoftwareDetailRepo softwareDetailRepo) {
        this.softwareDetailRepo = softwareDetailRepo;
    }

    public Optional<AssetsFormDto> findByItemCode(String itemCode) {
        AssetsFormDto assetsFormDto = new AssetsFormDto();
        assetsFormDto.setItemCode(itemCode);
        Optional<CoreAssetEntity> coreAssetEntityOptional = coreAssetEntityRepo.findByItemCode(itemCode);
        coreAssetEntityOptional.ifPresent(coreAssetEntity -> {
            assetsFormDto.setCoreDetail(coreAssetEntity);
            assetsFormDto.setAssetType(coreAssetEntity.getAssetType());
            Optional<AssetDetail> assetDetailOptional = assetDetailRepo.findByItemCode(itemCode);
            assetDetailOptional.ifPresent(assetsFormDto::setAssetDetail);
            setAssetDetailsByItemCodeAndAssetType(itemCode, coreAssetEntity.getAssetType(), assetsFormDto);
        });
        return Optional.of(assetsFormDto);
    }

    private void setAssetDetailsByItemCodeAndAssetType(String itemCode, AssetTypeEnum assetType, AssetsFormDto assetsFormDto) {
        switch (assetType) {
            case LAND:
                assetsFormDto.setLandDetail(findLandDetailByItemCode(itemCode));
                break;
            case OTHER:
                break;
            case HARDWARE:
                assetsFormDto.setHardwareDetail(findHardwareDetailByItemCode(itemCode));
                break;
            case SOFTWARE:
                assetsFormDto.setSoftwareDetail(findSoftwareDetailByItemCode(itemCode));
                break;
            case MACHINERY:
                assetsFormDto.setMachineryDetail(findMachineryDetailByItemCode(itemCode));
                break;
            case GENERATOR_UPS:
                assetsFormDto.setGeneratorUpsDetail(findGeneratorUpsDetailByItemCode(itemCode));
                break;
            case LAND_BUILDING:
                assetsFormDto.setLandBuildingDetail(findLandBuildingDetailByItemCode(itemCode));
                break;
            case MOTOR_VEHICLE:
                assetsFormDto.setMotorVehicleDetail(findMotorVehicleDetailByItemCode(itemCode));
                break;
            default:
                log.error(ErrorMessageConstants.INVALID_ASSET_TYPE.concat(":" + assetType));
        }
    }

    private LandDetail findLandDetailByItemCode(String itemCode) {
        Optional<LandDetail> landDetailOptional = landDetailRepo.findByItemCode(itemCode);
        return landDetailOptional.orElseGet(LandDetail::new);
    }

    private LandBuildingDetail findLandBuildingDetailByItemCode(String itemCode) {
        Optional<LandBuildingDetail> landBuildingDetailOptional = landBuildingDetailRepo.findByItemCode(itemCode);
        return landBuildingDetailOptional.orElseGet(LandBuildingDetail::new);
    }

    private HardwareDetail findHardwareDetailByItemCode(String itemCode) {
        Optional<HardwareDetail> hardwareDetailOptional = hardwareDetailRepo.findByItemCode(itemCode);
        return hardwareDetailOptional.orElseGet(HardwareDetail::new);
    }

    private SoftwareDetail findSoftwareDetailByItemCode(String itemCode) {
        Optional<SoftwareDetail> softwareDetailOptional = softwareDetailRepo.findByItemCode(itemCode);
        return softwareDetailOptional.orElseGet(SoftwareDetail::new);
    }

    private MachineryDetail findMachineryDetailByItemCode(String itemCode) {
        Optional<MachineryDetail> machineryDetailOptional = machineryDetailRepo.findByItemCode(itemCode);
        return machineryDetailOptional.orElseGet(MachineryDetail::new);
    }

    private GeneratorUpsDetail findGeneratorUpsDetailByItemCode(String itemCode) {
        Optional<GeneratorUpsDetail> generatorUpsDetailOptional = generatorUpsDetailRepo.findByItemCode(itemCode);
        return generatorUpsDetailOptional.orElseGet(GeneratorUpsDetail::new);
    }

    private MotorVehicleDetail findMotorVehicleDetailByItemCode(String itemCode) {
        Optional<MotorVehicleDetail> motorVehicleDetailOptional = motorVehicleDetailRepo.findByItemCode(itemCode);
        return motorVehicleDetailOptional.orElseGet(MotorVehicleDetail::new);
    }

}
