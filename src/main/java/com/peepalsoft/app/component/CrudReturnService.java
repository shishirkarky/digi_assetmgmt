package com.peepalsoft.app.component;

import com.peepalsoft.app.responsemessage.HttpResponses;
import com.peepalsoft.app.responsemessage.ResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CrudReturnService<T> {

    public ResponseEntity<ResponseMessage> returnListObject(List<T> list) {
        if (list != null && !list.isEmpty()) {
            return new ResponseEntity<>(HttpResponses.fetched(list), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpResponses.notfound(), HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<ResponseMessage> updateReturn(Object object) {
        if (object != null) {
            return new ResponseEntity<>(HttpResponses.created(object), HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST);
        }
    }

}
