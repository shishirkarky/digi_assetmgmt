package com.peepalsoft.app.component;

import com.peepalsoft.app.dto.AssetsFormDto;
import com.peepalsoft.app.entity.asset.*;
import com.peepalsoft.app.entity.asset.coreentities.*;
import com.peepalsoft.app.enums.AssetTypeEnum;
import com.peepalsoft.app.repo.assets.core.*;
import com.peepalsoft.app.service.assets.SubAssetsService;
import com.peepalsoft.app.util.UserDetailsUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.transaction.Transactional;
import java.util.*;

@Slf4j
@Component
@Transactional
public class AssetSaveComponent {
    private String principalUser;
    private String itemCode;
    private AssetTypeEnum assetType;

    /*
     * SET AUTOWIRED FOR REPOSITORY
     */
    private LandDetailRepo landDetailRepo;
    private LandBuildingDetailRepo landBuildingDetailRepo;
    private GeneratorUpsDetailRepo generatorUpsDetailRepo;
    private HardwareDetailRepo hardwareDetailRepo;
    private MachineryDetailRepo machineryDetailRepo;
    private MotorVehicleDetailRepo motorVehicleDetailRepo;
    private SoftwareDetailRepo softwareDetailRepo;
    private AssetDetailRepo assetDetailRepo;
    private AmcDetailRepo amcDetailRepo;
    private InsuranceDetailRepo insuranceDetailRepo;
    private WarrantyDetailRepo warrantyDetailRepo;
    private CoreAssetEntityRepo coreAssetEntityRepo;
    private TaxDetailRepo taxDetailRepo;
    private SubAssetsService subAssetsService;
    private UserDetailsUtil userDetailsUtil;

    @Autowired
    public void setCoreAssetEntityRepo(CoreAssetEntityRepo coreAssetEntityRepo) {
        this.coreAssetEntityRepo = coreAssetEntityRepo;
    }

    @Autowired
    public void setMachineryDetailRepo(MachineryDetailRepo machineryDetailRepo) {
        this.machineryDetailRepo = machineryDetailRepo;
    }

    @Autowired
    public void setLandDetailRepo(LandDetailRepo landDetailRepo) {
        this.landDetailRepo = landDetailRepo;
    }

    @Autowired
    public void setLandBuildingDetailRepo(LandBuildingDetailRepo landBuildingDetailRepo) {
        this.landBuildingDetailRepo = landBuildingDetailRepo;
    }

    @Autowired
    public void setGeneratorUpsDetailRepo(GeneratorUpsDetailRepo generatorUpsDetailRepo) {
        this.generatorUpsDetailRepo = generatorUpsDetailRepo;
    }

    @Autowired
    public void setHardwareDetailRepo(HardwareDetailRepo hardwareDetailRepo) {
        this.hardwareDetailRepo = hardwareDetailRepo;
    }

    @Autowired
    public void setMotorVehicleDetailRepo(MotorVehicleDetailRepo motorVehicleDetailRepo) {
        this.motorVehicleDetailRepo = motorVehicleDetailRepo;
    }

    @Autowired
    public void setSoftwareDetailRepo(SoftwareDetailRepo softwareDetailRepo) {
        this.softwareDetailRepo = softwareDetailRepo;
    }

    @Autowired
    public void setAssetDetailRepo(AssetDetailRepo assetDetailRepo) {
        this.assetDetailRepo = assetDetailRepo;
    }

    @Autowired
    public void setAmcDetailRepo(AmcDetailRepo amcDetailRepo) {
        this.amcDetailRepo = amcDetailRepo;
    }

    @Autowired
    public void setInsuranceDetailRepo(InsuranceDetailRepo insuranceDetailRepo) {
        this.insuranceDetailRepo = insuranceDetailRepo;
    }

    @Autowired
    public void setWarrantyDetailRepo(WarrantyDetailRepo warrantyDetailRepo) {
        this.warrantyDetailRepo = warrantyDetailRepo;
    }

    @Autowired
    public void setTaxDetailRepo(TaxDetailRepo taxDetailRepo) {
        this.taxDetailRepo = taxDetailRepo;
    }

    @Autowired
    public void setSubAssetsService(SubAssetsService subAssetsService) {
        this.subAssetsService = subAssetsService;
    }

    @Autowired
    public void setUserDetailsUtil(UserDetailsUtil userDetailsUtil) {
        this.userDetailsUtil = userDetailsUtil;
    }

    /*
     *  START OF METHODS
     */
    public Optional<AssetsFormDto> saveAssetDetails(AssetsFormDto assetsFormDto) {
        this.assetType = assetsFormDto.getAssetType();
        this.itemCode = getItemCode(assetsFormDto.getItemCode());
        this.principalUser = assetsFormDto.getPrincipalUsername();

        Map<String, String> map = userDetailsUtil.getStaffCodeAndBranchCode(this.principalUser);
        final String branchCode = map.get("branchCode");
        final String staffCode = map.get("staffCode");

        AssetsFormDto savedAsset = new AssetsFormDto();
        savedAsset.setItemCode(this.itemCode);
        savedAsset.setPrincipalUsername(this.principalUser);
        savedAsset.setAssetType(this.assetType);

        /*
         * SAVE ASSET TYPE RELATED DOMAIN DETAILS
         */
        switch (assetType) {
            case OTHER:
                log.info("OTHER asset type (general)");
                break;
            case MACHINERY:
                MachineryDetail machineryDetail = setMachineryDetail(assetsFormDto.getMachineryDetail());
                savedAsset.setMachineryDetail(machineryDetailRepo.save(machineryDetail));
                break;
            case LAND:
                LandDetail landDetail = setLandDetail(assetsFormDto.getLandDetail());
                savedAsset.setLandDetail(landDetailRepo.save(landDetail));
                break;
            case HARDWARE:
                HardwareDetail hardwareDetail = setHardwareDetail(assetsFormDto.getHardwareDetail());
                savedAsset.setHardwareDetail(hardwareDetailRepo.save(hardwareDetail));
                break;
            case SOFTWARE:
                SoftwareDetail softwareDetail = setSoftwareDetail(assetsFormDto.getSoftwareDetail());
                savedAsset.setSoftwareDetail(softwareDetailRepo.save(softwareDetail));
                break;
            case GENERATOR_UPS:
                GeneratorUpsDetail generatorUpsDetail = setGeneratorUpsDetail(assetsFormDto.getGeneratorUpsDetail());
                savedAsset.setGeneratorUpsDetail(generatorUpsDetailRepo.save(generatorUpsDetail));
                break;
            case MOTOR_VEHICLE:
                MotorVehicleDetail motorVehicleDetail = setMotorVehicleDetail(assetsFormDto.getMotorVehicleDetail());
                savedAsset.setMotorVehicleDetail(motorVehicleDetailRepo.save(motorVehicleDetail));
                break;
            case LAND_BUILDING:
                LandBuildingDetail landBuildingDetail = setLandBuildingDetail(assetsFormDto.getLandBuildingDetail());
                savedAsset.setLandBuildingDetail(landBuildingDetailRepo.save(landBuildingDetail));
                break;
            default:
                log.error("Err...INVALID ASSET TYPE");
        }
        /*
         *SAVE ASSET DETAIL
         */
        AssetDetail assetDetail = assetsFormDto.getAssetDetail();
        this.saveOtherDetail(assetDetail, savedAsset);
        /*
         *SAVE CORE ENTITY
         */
        CoreAssetEntity coreAssetEntity = new CoreAssetEntity();
        BeanUtils.copyProperties(assetsFormDto.getCoreDetail(), coreAssetEntity);
        this.setCoreAssetEntity(coreAssetEntity, branchCode, staffCode);

        savedAsset.setCoreDetail(coreAssetEntityRepo.save(coreAssetEntity));
        /*
         * SAVE SUB ASSETS
         */
        savedAsset.setSubAssets(this.saveSubAssets(assetsFormDto, assetDetail.getQuantity(), staffCode, branchCode));

        return Optional.of(savedAsset);
    }

    private List<SubAssets> saveSubAssets(AssetsFormDto assetsFormDto, int quantity, String staffCode, String branchCode) {
        if (quantity > 0) {
            List<SubAssets> subAssetsList = new ArrayList<>();
            for (int count = 1; count <= quantity; count++) {
                SubAssets subAssets = new SubAssets();
                subAssets.setSubItemCode(UUID.randomUUID().toString());

                subAssets.setCurrentAssetCondition(assetsFormDto.getCoreDetail().getCurrentAssetCondition());
                subAssets.setDeleteStatus(0);
                subAssets.setItemCode(itemCode);
                subAssets.setRefAssetConditionId(assetsFormDto.getAssetDetail().getRefAssetConditionId());
                subAssets.setBranchCode(branchCode);
                subAssets.setStaffCode(staffCode);
                subAssets.setLastUpdatedBy(this.principalUser);

                subAssetsList.add(subAssets);
            }

            return subAssetsService.saveSubAssets(subAssetsList);
        }
        return new ArrayList<>();
    }

    private void saveOtherDetail(AssetDetail assetDetail, AssetsFormDto savedAsset) {
        if (assetDetail != null) {
            savedAsset.setAssetDetail(assetDetailRepo.save(setOtherDetail(assetDetail)));
        }
    }

    public LandDetail setLandDetail(LandDetail sourceObject) {
        this.assetType = AssetTypeEnum.LAND;
        LandDetail landDetail = new LandDetail();
        try {
            BeanUtils.copyProperties(sourceObject, landDetail);
        } catch (Exception e) {
            log.error("Err...LandDetail sourceObject null found");
        }
        landDetailRepo.findByItemCode(this.itemCode).ifPresent(l -> landDetail.setId(l.getId()));
        landDetail.setLastUpdatedBy(this.principalUser);
        landDetail.setItemCode(this.itemCode);
        return landDetail;
    }


    public LandBuildingDetail setLandBuildingDetail(LandBuildingDetail sourceObject) {
        this.assetType = AssetTypeEnum.LAND_BUILDING;
        LandBuildingDetail landBuildingDetail = new LandBuildingDetail();
        try {
            BeanUtils.copyProperties(sourceObject, landBuildingDetail);
        } catch (Exception e) {
            log.error("Err...LandBuildingDetail sourceObject null found");
        }
        landBuildingDetailRepo.findByItemCode(this.itemCode).ifPresent(l -> landBuildingDetail.setId(l.getId()));
        landBuildingDetail.setLastUpdatedBy(this.principalUser);
        landBuildingDetail.setItemCode(this.itemCode);
        return landBuildingDetail;
    }


    public GeneratorUpsDetail setGeneratorUpsDetail(GeneratorUpsDetail sourceObject) {
        this.assetType = AssetTypeEnum.GENERATOR_UPS;
        GeneratorUpsDetail generatorUpsDetail = new GeneratorUpsDetail();
        try {
            BeanUtils.copyProperties(sourceObject, generatorUpsDetail);

        } catch (Exception e) {
            log.error("Err...GeneratorUpsDetail sourceObject null found");
        }
        generatorUpsDetailRepo.findByItemCode(this.itemCode).ifPresent(g -> generatorUpsDetail.setId(g.getId()));
        generatorUpsDetail.setItemCode(this.itemCode);
        generatorUpsDetail.setLastUpdatedBy(this.principalUser);
        return generatorUpsDetail;
    }


    public HardwareDetail setHardwareDetail(HardwareDetail sourceObject) {
        this.assetType = AssetTypeEnum.HARDWARE;
        HardwareDetail hardwareDetail = new HardwareDetail();
        try {
            BeanUtils.copyProperties(sourceObject, hardwareDetail);
        } catch (Exception e) {
            log.error("Err...HardwareDetail sourceObject null found");
        }
        hardwareDetailRepo.findByItemCode(this.itemCode).ifPresent(h -> hardwareDetail.setId(h.getId()));
        hardwareDetail.setItemCode(this.itemCode);
        hardwareDetail.setLastUpdatedBy(this.principalUser);
        return hardwareDetail;
    }

    public MachineryDetail setMachineryDetail(MachineryDetail sourceObject) {
        this.assetType = AssetTypeEnum.MACHINERY;
        MachineryDetail machineryDetail = new MachineryDetail();
        try {
            BeanUtils.copyProperties(sourceObject, machineryDetail);
        } catch (Exception e) {
            log.error("Err...MachineryDetail sourceObject null found");
        }
        machineryDetailRepo.findByItemCode(this.itemCode).ifPresent(m -> machineryDetail.setId(m.getId()));
        machineryDetail.setItemCode(this.itemCode);
        machineryDetail.setLastUpdatedBy(this.principalUser);
        return machineryDetail;
    }


    public MotorVehicleDetail setMotorVehicleDetail(MotorVehicleDetail sourceObject) {
        this.assetType = AssetTypeEnum.MOTOR_VEHICLE;
        MotorVehicleDetail motorVehicleDetail = new MotorVehicleDetail();
        try {
            BeanUtils.copyProperties(sourceObject, motorVehicleDetail);
        } catch (Exception e) {
            log.error("Err...MotorVehicleDetail sourceObject null found");
        }
        motorVehicleDetailRepo.findByItemCode(this.itemCode).ifPresent(m -> motorVehicleDetail.setId(m.getId()));
        motorVehicleDetail.setLastUpdatedBy(this.principalUser);
        motorVehicleDetail.setItemCode(this.itemCode);
        return motorVehicleDetail;
    }

    private CoreAssetEntity setCoreAssetEntity(CoreAssetEntity coreAssetEntity, String branchCode, String staffCode) {
        Optional<CoreAssetEntity> coreAssetEntityOptional = coreAssetEntityRepo.findByItemCode(this.itemCode);
        if (coreAssetEntityOptional.isPresent()) {
            coreAssetEntity.setId(coreAssetEntityOptional.get().getId());
        } else {
            coreAssetEntity.setStaffCode(staffCode);
            coreAssetEntity.setRefBranchCode(branchCode);
        }
        coreAssetEntity.setItemCode(this.itemCode);
        coreAssetEntity.setLastUpdatedBy(this.principalUser);
        coreAssetEntity.setAssetType(this.assetType);
        return coreAssetEntity;
    }

    public SoftwareDetail setSoftwareDetail(SoftwareDetail sourceObject) {
        this.assetType = AssetTypeEnum.SOFTWARE;
        SoftwareDetail softwareDetail = new SoftwareDetail();
        try {
            BeanUtils.copyProperties(sourceObject, softwareDetail);
        } catch (Exception e) {
            log.error("Err...SoftwareDetail sourceObject null found");
        }
        softwareDetailRepo.findByItemCode(this.itemCode).ifPresent(s -> softwareDetail.setId(s.getId()));
        softwareDetail.setLastUpdatedBy(this.principalUser);
        softwareDetail.setItemCode(this.itemCode);
        return softwareDetail;
    }

    private AssetDetail setOtherDetail(AssetDetail sourceObject) {
        AssetDetail assetDetail = new AssetDetail();
        try {
            BeanUtils.copyProperties(sourceObject, assetDetail);
        } catch (Exception e) {
            log.error("Err...AssetDetail sourceObject null found");
        }
        assetDetailRepo.findByItemCode(itemCode).ifPresent(a -> assetDetail.setId(a.getId()));
        assetDetail.setItemCode(this.itemCode);
        assetDetail.setLastUpdatedBy(this.principalUser);
        return assetDetail;
    }

    private WarrantyDetail setWarrantyDetail(WarrantyDetail sourceObject) {
        WarrantyDetail warrantyDetail = new WarrantyDetail();
        try {
            BeanUtils.copyProperties(sourceObject, warrantyDetail);
        } catch (Exception e) {
            log.error("Err...WarrantyDetail sourceObject null found");
        }
        warrantyDetailRepo.findByItemCode(this.itemCode).ifPresent(o -> warrantyDetail.setId(o.getId()));
        warrantyDetail.setItemCode(this.itemCode);
        warrantyDetail.setLastUpdatedBy(this.principalUser);
        return warrantyDetail;
    }

    private TaxDetail setTaxDetail(TaxDetail sourceObject) {
        TaxDetail taxDetail = new TaxDetail();
        try {
            BeanUtils.copyProperties(sourceObject, taxDetail);
        } catch (Exception e) {
            log.error("Err...TaxDetail sourceObject null found");
        }
        taxDetailRepo.findByItemCode(this.itemCode).ifPresent(t -> taxDetail.setId(t.getId()));
        taxDetail.setItemCode(this.itemCode);
        taxDetail.setLastUpdatedBy(this.principalUser);
        return taxDetail;
    }

    private InsuranceDetail setInsuranceDetail(InsuranceDetail sourceObject) {
        InsuranceDetail insuranceDetail = new InsuranceDetail();
        try {
            BeanUtils.copyProperties(sourceObject, insuranceDetail);
        } catch (Exception e) {
            log.error("Err...InsuranceDetail sourceObject null found");
        }
        insuranceDetailRepo.findByItemCode(this.itemCode).ifPresent(i -> insuranceDetail.setId(i.getId()));
        insuranceDetail.setItemCode(this.itemCode);
        insuranceDetail.setLastUpdatedBy(this.principalUser);
        return insuranceDetail;
    }

    private AmcDetail setAmcDetail(AmcDetail sourceObject) {
        AmcDetail amcDetail = new AmcDetail();
        try {
            BeanUtils.copyProperties(sourceObject, amcDetail);
        } catch (Exception e) {
            log.error("Err...AmcDetail sourceObject null found");
        }
        amcDetailRepo.findByItemCode(this.itemCode).ifPresent(a -> amcDetail.setId(a.getId()));
        amcDetail.setItemCode(this.itemCode);
        amcDetail.setLastUpdatedBy(this.principalUser);
        return amcDetail;
    }

    private String getItemCode(String sourceItemCode) {
        if (StringUtils.isEmpty(sourceItemCode)) {
            return UUID.randomUUID().toString();
        } else {
            return sourceItemCode;
        }
    }


}
