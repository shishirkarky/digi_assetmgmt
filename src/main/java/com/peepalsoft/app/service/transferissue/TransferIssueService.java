package com.peepalsoft.app.service.transferissue;

import com.peepalsoft.app.dto.IssueDto;
import com.peepalsoft.app.dto.TransferIssueDto;
import com.peepalsoft.app.entity.asset.trackitem.ItemTransferIssue;
import com.peepalsoft.app.enums.StatusEnum;

import java.util.List;

public interface TransferIssueService {
    void makeItemsTransferPending(TransferIssueDto transferIssueDto, String username);

    void issueItems(IssueDto issueDto, String username);

    List<ItemTransferIssue> findAllTransferIssue();

    List<ItemTransferIssue> findAllTransferIssueByBranchCodeAndStatus(String toBranchCode, StatusEnum status);

    List<ItemTransferIssue> findTransferIssueLogByBranchCode(String branchCode);
}
