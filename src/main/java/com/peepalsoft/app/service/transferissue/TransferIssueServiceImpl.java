package com.peepalsoft.app.service.transferissue;

import com.peepalsoft.app.dto.IssueDto;
import com.peepalsoft.app.dto.TransferIssueDto;
import com.peepalsoft.app.entity.asset.SubAssets;
import com.peepalsoft.app.entity.asset.coreentities.CoreAssetEntity;
import com.peepalsoft.app.entity.asset.trackitem.ItemTransferIssue;
import com.peepalsoft.app.enums.StatusEnum;
import com.peepalsoft.app.repo.assets.ItemTransferIssueRepository;
import com.peepalsoft.app.repo.assets.core.CoreAssetEntityRepo;
import com.peepalsoft.app.service.assets.SubAssetsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Transactional
@Service
public class TransferIssueServiceImpl implements TransferIssueService {
    private CoreAssetEntityRepo coreAssetEntityRepo;
    private ItemTransferIssueRepository itemTransferIssueRepository;
    private SubAssetsService subAssetsService;

    @Autowired
    public void setCoreAssetEntityRepo(CoreAssetEntityRepo coreAssetEntityRepo) {
        this.coreAssetEntityRepo = coreAssetEntityRepo;
    }

    @Autowired
    public void setItemTransferIssueRepository(ItemTransferIssueRepository itemTransferIssueRepository) {
        this.itemTransferIssueRepository = itemTransferIssueRepository;
    }

    @Autowired
    public void setSubAssetsService(SubAssetsService subAssetsService) {
        this.subAssetsService = subAssetsService;
    }

    @Override
    public void makeItemsTransferPending(TransferIssueDto transferIssueDto, String username) {
        List<String> itemCodes = transferIssueDto.getItemCodes();
        if (!itemCodes.isEmpty()) {
            List<ItemTransferIssue> itemTransferIssueList = new ArrayList<>();
            List<SubAssets> subAssets = new ArrayList<>();
            itemCodes.forEach(itemCode -> {
                Optional<SubAssets> subAssetsOptional = subAssetsService.getSubAssetsBySubItemCode(itemCode);
                subAssetsOptional.ifPresent(subAsset -> {
                    ItemTransferIssue itemTransferIssue = new ItemTransferIssue();
                    itemTransferIssue.setItemCode(itemCode);
                    itemTransferIssue.setFromBranchCode(subAsset.getBranchCode());
                    itemTransferIssue.setFromStaffCode(subAsset.getStaffCode());
                    itemTransferIssue.setToStaffCode(transferIssueDto.getToStaffCode());
                    itemTransferIssue.setToBranchCode(transferIssueDto.getToBranchCode());
                    itemTransferIssue.setLastUpdatedBy(username);
                    itemTransferIssue.setStatus(StatusEnum.TRANSFER_PENDING);
                    itemTransferIssueList.add(itemTransferIssue);

                    subAsset.setLastUpdatedBy(username);
                    subAsset.setStatus(StatusEnum.TRANSFER_PENDING);
                    subAssets.add(subAsset);
                });
            });

            itemTransferIssueRepository.saveAll(itemTransferIssueList);
            subAssetsService.saveSubAssets(subAssets);
        }
    }

    @Override
    public void issueItems(IssueDto issueDto, String username) {
        List<String> itemCodes = issueDto.getItemCodes();
        if (!itemCodes.isEmpty()) {
            List<ItemTransferIssue> itemTransferIssueList = new ArrayList<>();
            List<SubAssets> subAssets = new ArrayList<>();
            itemCodes.forEach(itemCode -> {
                Optional<SubAssets> subAssetsOptional = subAssetsService.getSubAssetsBySubItemCode(itemCode);
                subAssetsOptional.ifPresent(subAsset -> {
                    ItemTransferIssue itemTransferIssue = new ItemTransferIssue();
                    itemTransferIssue.setItemCode(itemCode);
                    itemTransferIssue.setFromBranchCode(subAsset.getBranchCode());
                    itemTransferIssue.setFromStaffCode(subAsset.getStaffCode());
                    itemTransferIssue.setToStaffCode(issueDto.getToStaffCode());
                    itemTransferIssue.setToBranchCode(subAsset.getBranchCode());
                    itemTransferIssue.setLastUpdatedBy(username);
                    itemTransferIssue.setStatus(StatusEnum.ISSUED);
                    itemTransferIssueList.add(itemTransferIssue);

                    subAsset.setStaffCode(issueDto.getToStaffCode());
                    subAsset.setLastUpdatedBy(username);
                    subAsset.setStatus(StatusEnum.ISSUED);
                    subAssets.add(subAsset);
                });
            });

            itemTransferIssueRepository.saveAll(itemTransferIssueList);
            subAssetsService.saveSubAssets(subAssets);
        }
    }

    @Override
    public List<ItemTransferIssue> findAllTransferIssue() {
        return itemTransferIssueRepository.findAll();
    }

    @Override
    public List<ItemTransferIssue> findAllTransferIssueByBranchCodeAndStatus(String toBranchCode, StatusEnum status) {
        return itemTransferIssueRepository.findByBranchCodeAndStatus(toBranchCode, status);
    }

    @Override
    public List<ItemTransferIssue> findTransferIssueLogByBranchCode(String branchCode) {
        return itemTransferIssueRepository.findByBranchCode(branchCode);
    }
}
