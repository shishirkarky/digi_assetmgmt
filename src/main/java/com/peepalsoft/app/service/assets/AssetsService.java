package com.peepalsoft.app.service.assets;

import com.peepalsoft.app.dto.AssetCountsDto;
import com.peepalsoft.app.dto.AssetsFormDto;
import com.peepalsoft.app.dto.AssetsTableDto;
import com.peepalsoft.app.entity.asset.coreentities.AssetDetail;
import com.peepalsoft.app.entity.asset.coreentities.CoreAssetEntity;
import com.peepalsoft.app.enums.AssetTypeEnum;
import com.peepalsoft.app.enums.StatusEnum;

import java.util.List;
import java.util.Optional;

public interface AssetsService {
    Optional<AssetsFormDto> save(AssetsFormDto assetsFormDto);

    Optional<AssetsFormDto> findByItemCode(String itemCode);

    AssetsTableDto findByAssetType(AssetTypeEnum assetTypeEnum);

    AssetCountsDto findAllAssetTypeCount();

    List<AssetDetail> findAllAssetDetailByAssetType(AssetTypeEnum assetType);

    Optional<AssetsFormDto> saveWarrantyDetail(AssetsFormDto assetsFormDto);

    Optional<AssetsFormDto> saveInsurance(AssetsFormDto assetsFormDto);

    Optional<AssetsFormDto> saveAmc(AssetsFormDto assetsFormDto);

    Optional<AssetsFormDto> saveTax(AssetsFormDto assetsFormDto);

    boolean validateSaveAction(String user,String itemCode);

    Optional<CoreAssetEntity> changeAssetStatus(String itemCode, StatusEnum status, String username);

}
