package com.peepalsoft.app.service.assets;

import com.peepalsoft.app.entity.asset.SubAssets;
import com.peepalsoft.app.enums.StatusEnum;
import com.peepalsoft.app.repo.assets.SubAssetsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SubAssetsServiceImpl implements SubAssetsService {
    private SubAssetsRepository subAssetsRepository;

    @Autowired
    public void setSubAssetsRepository(SubAssetsRepository subAssetsRepository) {
        this.subAssetsRepository = subAssetsRepository;
    }

    @Override
    public Optional<SubAssets> saveSubAsset(SubAssets subAssets) {
        return Optional.of(subAssetsRepository.save(subAssets));
    }

    @Override
    public List<SubAssets> saveSubAssets(List<SubAssets> subAssetsList) {
        if (!subAssetsList.isEmpty()) {
            return subAssetsRepository.saveAll(subAssetsList);
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List<SubAssets> getSubAssetsByItemCode(String itemCode) {
        return subAssetsRepository.findAllByItemCode(itemCode);
    }

    @Override
    public Optional<SubAssets> getSubAssetsBySubItemCode(String subItemCode) {
        return subAssetsRepository.findFirstBySubItemCode(subItemCode);
    }

    @Override
    public List<SubAssets> getAllSubAssetsForBranch(String branchCode) {
        return subAssetsRepository.findAllByBranchCode(branchCode);
    }

    @Override
    public List<SubAssets> getAllSubAssetsByBranchAndStatus(String branchCode, StatusEnum status) {
        return subAssetsRepository.findAllByBranchCodeAndStatus(branchCode, status);
    }
}
