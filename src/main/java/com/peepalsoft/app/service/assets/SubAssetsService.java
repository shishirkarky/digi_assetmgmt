package com.peepalsoft.app.service.assets;

import com.peepalsoft.app.entity.asset.SubAssets;
import com.peepalsoft.app.enums.StatusEnum;

import java.util.List;
import java.util.Optional;

public interface SubAssetsService {
    Optional<SubAssets> saveSubAsset(SubAssets subAssets);

    List<SubAssets> saveSubAssets(List<SubAssets> subAssetsList);

    List<SubAssets> getSubAssetsByItemCode(String itemCode);

    Optional<SubAssets> getSubAssetsBySubItemCode(String subItemCode);

    List<SubAssets> getAllSubAssetsForBranch(String branchCode);

    List<SubAssets> getAllSubAssetsByBranchAndStatus(String branchCode, StatusEnum status);
}
