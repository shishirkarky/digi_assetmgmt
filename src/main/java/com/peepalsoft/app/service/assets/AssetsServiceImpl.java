package com.peepalsoft.app.service.assets;

import com.peepalsoft.app.component.AssetFetchComponent;
import com.peepalsoft.app.component.AssetSaveComponent;
import com.peepalsoft.app.config.ErrorMessageConstants;
import com.peepalsoft.app.dto.AssetCountsDto;
import com.peepalsoft.app.dto.AssetsFormDto;
import com.peepalsoft.app.dto.AssetsTableDto;
import com.peepalsoft.app.entity.asset.AmcDetail;
import com.peepalsoft.app.entity.asset.InsuranceDetail;
import com.peepalsoft.app.entity.asset.TaxDetail;
import com.peepalsoft.app.entity.asset.WarrantyDetail;
import com.peepalsoft.app.entity.asset.coreentities.AssetDetail;
import com.peepalsoft.app.entity.asset.coreentities.CoreAssetEntity;
import com.peepalsoft.app.enums.AssetTypeEnum;
import com.peepalsoft.app.enums.StatusEnum;
import com.peepalsoft.app.exception.CustomValidationException;
import com.peepalsoft.app.repo.assets.core.*;
import com.peepalsoft.app.user.service.UsersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class AssetsServiceImpl implements AssetsService {
    private AssetSaveComponent assetSaveComponent;
    private AssetFetchComponent assetFetchComponent;
    private LandDetailRepo landDetailRepo;
    private MotorVehicleDetailRepo motorVehicleDetailRepo;
    private AssetDetailRepo assetDetailRepo;
    private LandBuildingDetailRepo landBuildingDetailRepo;
    private CoreAssetEntityRepo coreAssetEntityRepo;
    private WarrantyDetailRepo warrantyDetailRepo;
    private InsuranceDetailRepo insuranceDetailRepo;
    private AmcDetailRepo amcDetailRepo;
    private TaxDetailRepo taxDetailRepo;
    private UsersService usersService;

    @Autowired
    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    @Autowired
    public void setTaxDetailRepo(TaxDetailRepo taxDetailRepo) {
        this.taxDetailRepo = taxDetailRepo;
    }

    @Autowired
    public void setWarrantyDetailRepo(WarrantyDetailRepo warrantyDetailRepo) {
        this.warrantyDetailRepo = warrantyDetailRepo;
    }

    @Autowired
    public void setInsuranceDetailRepo(InsuranceDetailRepo insuranceDetailRepo) {
        this.insuranceDetailRepo = insuranceDetailRepo;
    }

    @Autowired
    public void setAmcDetailRepo(AmcDetailRepo amcDetailRepo) {
        this.amcDetailRepo = amcDetailRepo;
    }

    @Autowired
    public void setAssetFetchComponent(AssetFetchComponent assetFetchComponent) {
        this.assetFetchComponent = assetFetchComponent;
    }

    @Autowired
    public void setCoreAssetEntityRepo(CoreAssetEntityRepo coreAssetEntityRepo) {
        this.coreAssetEntityRepo = coreAssetEntityRepo;
    }

    @Autowired
    public void setLandBuildingDetailRepo(LandBuildingDetailRepo landBuildingDetailRepo) {
        this.landBuildingDetailRepo = landBuildingDetailRepo;
    }

    @Autowired
    public void setMotorVehicleDetailRepo(MotorVehicleDetailRepo motorVehicleDetailRepo) {
        this.motorVehicleDetailRepo = motorVehicleDetailRepo;
    }

    @Autowired
    public void setAssetDetailRepo(AssetDetailRepo assetDetailRepo) {
        this.assetDetailRepo = assetDetailRepo;
    }


    @Autowired
    public void setLandDetailRepo(LandDetailRepo landDetailRepo) {
        this.landDetailRepo = landDetailRepo;
    }


    @Autowired
    public void setAssetSaveComponent(AssetSaveComponent assetSaveComponent) {
        this.assetSaveComponent = assetSaveComponent;
    }

    @Override
    public Optional<AssetsFormDto> save(AssetsFormDto assetsFormDto) {
        return assetSaveComponent.saveAssetDetails(assetsFormDto);
    }

    @Override
    public Optional<AssetsFormDto> findByItemCode(String itemCode) {
        return assetFetchComponent.findByItemCode(itemCode);
    }

    @Override
    public AssetsTableDto findByAssetType(AssetTypeEnum assetTypeEnum) {
        AssetsTableDto assetsTableDto = new AssetsTableDto();
        switch (assetTypeEnum) {
            case LAND_BUILDING:
                assetsTableDto.setLandBuildingDetail(landBuildingDetailRepo.findAll());
                break;
            case MOTOR_VEHICLE:
                assetsTableDto.setMotorVehicleDetail(motorVehicleDetailRepo.findAll());
                break;
            case LAND:
                assetsTableDto.setLandDetail(landDetailRepo.findAll());
                break;
            case OTHER:
                assetsTableDto.setAssetDetail(findAllAssetDetailByAssetType(assetTypeEnum));
                break;
            case HARDWARE:
                assetsTableDto.setAssetDetail(findAllAssetDetailByAssetType(assetTypeEnum));
                break;
            case SOFTWARE:
                assetsTableDto.setAssetDetail(findAllAssetDetailByAssetType(assetTypeEnum));
                break;
            case MACHINERY:
                assetsTableDto.setAssetDetail(findAllAssetDetailByAssetType(assetTypeEnum));
                break;
            case GENERATOR_UPS:
                assetsTableDto.setAssetDetail(findAllAssetDetailByAssetType(assetTypeEnum));
                break;
            default:
                log.error("Err...Invalid AssetType: findByAssetType");
        }
        return assetsTableDto;
    }

    @Override
    public AssetCountsDto findAllAssetTypeCount() {
        AssetCountsDto assetCountsDto = new AssetCountsDto();
        int generatorUps = coreAssetEntityRepo.countAllByAssetType(AssetTypeEnum.GENERATOR_UPS);
        assetCountsDto.setGeneratorUpsCount(generatorUps);
        int hardware = coreAssetEntityRepo.countAllByAssetType(AssetTypeEnum.HARDWARE);
        assetCountsDto.setHardwareCount(hardware);
        int software = coreAssetEntityRepo.countAllByAssetType(AssetTypeEnum.SOFTWARE);
        assetCountsDto.setSoftwareCount(software);
        int machinery = coreAssetEntityRepo.countAllByAssetType(AssetTypeEnum.MACHINERY);
        assetCountsDto.setMachineryCount(machinery);
        int motorVehicle = coreAssetEntityRepo.countAllByAssetType(AssetTypeEnum.MOTOR_VEHICLE);
        assetCountsDto.setMotorVehicleCount(motorVehicle);
        int land = coreAssetEntityRepo.countAllByAssetType(AssetTypeEnum.LAND);
        assetCountsDto.setLandCount(land);
        int landBuilding = coreAssetEntityRepo.countAllByAssetType(AssetTypeEnum.LAND_BUILDING);
        assetCountsDto.setLandBuildingCount(landBuilding);
        int other = coreAssetEntityRepo.countAllByAssetType(AssetTypeEnum.OTHER);
        assetCountsDto.setGeneralCount(other);
        return assetCountsDto;
    }

    @Override
    public List<AssetDetail> findAllAssetDetailByAssetType(AssetTypeEnum assetType) {
        List<CoreAssetEntity> coreAssetEntityList = coreAssetEntityRepo.findByAssetType(assetType);
        List<String> itemCodes = new ArrayList<>();
        coreAssetEntityList.forEach(coreAssetEntity -> itemCodes.add(coreAssetEntity.getItemCode()));
        return assetDetailRepo.findByItemCodeIn(itemCodes);
    }

    @Override
    public Optional<AssetsFormDto> saveWarrantyDetail(AssetsFormDto assetsFormDto) {
        if (!StringUtils.isEmpty(assetsFormDto.getItemCode()) && coreAssetEntityRepo.findByItemCode(assetsFormDto.getItemCode()).isPresent()) {
            Optional<WarrantyDetail> warrantyDetailOptional = warrantyDetailRepo.findByItemCode(assetsFormDto.getItemCode());
            WarrantyDetail warrantyDetail = new WarrantyDetail();
            BeanUtils.copyProperties(assetsFormDto.getWarrantyDetail(), warrantyDetail);
            warrantyDetailOptional.ifPresent(detail -> warrantyDetail.setId(detail.getId()));
            warrantyDetail.setItemCode(assetsFormDto.getItemCode());
            warrantyDetail.setLastUpdatedBy(assetsFormDto.getPrincipalUsername());
            assetsFormDto.setWarrantyDetail(warrantyDetailRepo.save(warrantyDetail));
            return Optional.of(assetsFormDto);
        }
        return Optional.empty();
    }

    @Override
    public Optional<AssetsFormDto> saveInsurance(AssetsFormDto assetsFormDto) {
        if (!StringUtils.isEmpty(assetsFormDto.getItemCode()) && coreAssetEntityRepo.findByItemCode(assetsFormDto.getItemCode()).isPresent()) {
            Optional<InsuranceDetail> insuranceDetailOptional = insuranceDetailRepo.findByItemCode(assetsFormDto.getItemCode());
            InsuranceDetail insuranceDetail = new InsuranceDetail();
            BeanUtils.copyProperties(assetsFormDto.getInsuranceDetail(), insuranceDetail);
            insuranceDetailOptional.ifPresent(detail -> insuranceDetail.setId(detail.getId()));
            insuranceDetail.setItemCode(assetsFormDto.getItemCode());
            insuranceDetail.setLastUpdatedBy(assetsFormDto.getPrincipalUsername());
            assetsFormDto.setInsuranceDetail(insuranceDetailRepo.save(insuranceDetail));
            return Optional.of(assetsFormDto);
        }
        return Optional.empty();
    }

    @Override
    public Optional<AssetsFormDto> saveAmc(AssetsFormDto assetsFormDto) {
        if (!StringUtils.isEmpty(assetsFormDto.getItemCode()) && coreAssetEntityRepo.findByItemCode(assetsFormDto.getItemCode()).isPresent()) {
            Optional<AmcDetail> amcDetailOptional = amcDetailRepo.findByItemCode(assetsFormDto.getItemCode());
            AmcDetail amcDetail = new AmcDetail();
            BeanUtils.copyProperties(assetsFormDto.getAmcDetail(), amcDetail);
            amcDetailOptional.ifPresent(detail -> amcDetail.setId(detail.getId()));
            amcDetail.setItemCode(assetsFormDto.getItemCode());
            amcDetail.setLastUpdatedBy(assetsFormDto.getPrincipalUsername());
            assetsFormDto.setAmcDetail(amcDetailRepo.save(amcDetail));
            return Optional.of(assetsFormDto);
        }
        return Optional.empty();
    }

    @Override
    public Optional<AssetsFormDto> saveTax(AssetsFormDto assetsFormDto) {
        if (!StringUtils.isEmpty(assetsFormDto.getItemCode()) && coreAssetEntityRepo.findByItemCode(assetsFormDto.getItemCode()).isPresent()) {
            Optional<TaxDetail> taxDetailOptional = taxDetailRepo.findByItemCode(assetsFormDto.getItemCode());
            TaxDetail taxDetail = new TaxDetail();
            BeanUtils.copyProperties(assetsFormDto.getTaxDetail(), taxDetail);
            taxDetailOptional.ifPresent(detail -> taxDetail.setId(detail.getId()));
            taxDetail.setItemCode(assetsFormDto.getItemCode());
            taxDetail.setLastUpdatedBy(assetsFormDto.getPrincipalUsername());
            assetsFormDto.setTaxDetail(taxDetailRepo.save(taxDetail));
            return Optional.of(assetsFormDto);
        }
        return Optional.empty();
    }

    @Override
    public boolean validateSaveAction(String username, String itemCode) {
        if (!StringUtils.isEmpty(itemCode)) {
            String branchCode = usersService.getBranchCode(username);
            Optional<CoreAssetEntity> coreAssetEntityOptional = coreAssetEntityRepo.findByItemCode(itemCode);
            if (coreAssetEntityOptional.isPresent()) {
                CoreAssetEntity coreAssetEntity = coreAssetEntityOptional.get();
                return branchCode.equals(coreAssetEntity.getRefBranchCode()) && !StatusEnum.TRANSFER_PENDING.equals(coreAssetEntity.getStatus());
            }
        }
        return true;
    }

    @Override
    public Optional<CoreAssetEntity> changeAssetStatus(String itemCode, StatusEnum status, String username) {
        Optional<CoreAssetEntity> coreAssetEntityOptional = coreAssetEntityRepo.findByItemCode(itemCode);
        if (coreAssetEntityOptional.isPresent()) {
            CoreAssetEntity coreAssetEntity = coreAssetEntityOptional.get();
            coreAssetEntity.setStatus(status);
            coreAssetEntity.setLastUpdatedBy(username);
            return Optional.of(coreAssetEntityRepo.save(coreAssetEntity));
        } else {
            throw new CustomValidationException(Collections.singletonList(ErrorMessageConstants.ASSET_NOT_FOUND));
        }
    }
}
