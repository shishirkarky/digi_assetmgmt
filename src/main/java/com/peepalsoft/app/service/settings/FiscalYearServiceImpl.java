package com.peepalsoft.app.service.settings;

import com.peepalsoft.app.entity.settings.FiscalYear;
import com.peepalsoft.app.repo.settings.FiscalYearRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FiscalYearServiceImpl implements FiscalYearService {
    @Autowired
    private FiscalYearRepo fiscalYearRepo;

    @Override
    public List<FiscalYear> getAll() {
        return fiscalYearRepo.findAll();
    }

    @Override
    public Optional<FiscalYear> save(FiscalYear fiscalYear) {
        return Optional.of(fiscalYearRepo.save(fiscalYear));
    }
}
