package com.peepalsoft.app.service.settings;

import com.peepalsoft.app.entity.asset.groups.GroupFour;
import com.peepalsoft.app.entity.settings.AssetCondition;
import com.peepalsoft.app.entity.settings.Company;
import com.peepalsoft.app.entity.settings.FundSource;
import com.peepalsoft.app.entity.settings.ReceivedSource;
import com.peepalsoft.app.enums.CompanyTypeEnum;
import com.peepalsoft.app.repo.settings.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SettingsServiceImpl implements SettingsService {
    private CompanyRepo companyRepo;
    private FundSourceRepo fundSourceRepo;
    private ReceivedSourceRepo receivedSourceRepo;
    private GroupFourRepo groupFourRepo;
    private AssetConditionRepo assetConditionRepo;

    @Autowired
    public void setAssetConditionRepo(AssetConditionRepo assetConditionRepo) {
        this.assetConditionRepo = assetConditionRepo;
    }

    @Autowired
    public void setGroupFourRepo(GroupFourRepo groupFourRepo) {
        this.groupFourRepo = groupFourRepo;
    }

    @Autowired
    public void setReceivedSourceRepo(ReceivedSourceRepo receivedSourceRepo) {
        this.receivedSourceRepo = receivedSourceRepo;
    }

    @Autowired
    public void setCompanyRepo(CompanyRepo companyRepo) {
        this.companyRepo = companyRepo;
    }

    @Autowired
    public void setFundSourceRepo(FundSourceRepo fundSourceRepo) {
        this.fundSourceRepo = fundSourceRepo;
    }

    @Override
    public Optional<Company> companySave(Company company) {
        return Optional.of(companyRepo.save(company));
    }

    @Override
    public List<Company> companyFindAll() {
        return companyRepo.findAll();
    }

    @Override
    public List<Company> companyFindAllByCompanyType(CompanyTypeEnum companyType) {
        return companyRepo.findAllByCompanyType(companyType);
    }

    @Override
    public Optional<Company> companyFindById(int id) {
        return companyRepo.findById(id);
    }

    @Override
    public Optional<FundSource> fundSourceSave(FundSource fundSource) {
        return Optional.of(fundSourceRepo.save(fundSource));
    }

    @Override
    public List<FundSource> fundSourceFindAll() {
        return fundSourceRepo.findAll();
    }

    @Override
    public Optional<FundSource> fundSourceFindById(int id) {
        return fundSourceRepo.findById(id);
    }

    @Override
    public Optional<ReceivedSource> saveReceivedSource(ReceivedSource receivedSource) {
        return Optional.of(receivedSourceRepo.save(receivedSource));
    }

    @Override
    public List<ReceivedSource> findAllReceivedSource() {
        return receivedSourceRepo.findAll();
    }

    @Override
    public Optional<GroupFour> saveGroupFour(GroupFour groupFour) {
        return Optional.of(groupFourRepo.save(groupFour));
    }

    @Override
    public List<GroupFour> findAllGroupFour() {
        return groupFourRepo.findAll();
    }

    @Override
    public Optional<AssetCondition> assetConditionSave(AssetCondition assetCondition) {
        return Optional.of(assetConditionRepo.save(assetCondition));
    }

    @Override
    public List<AssetCondition> findAllAssetCondition() {
        return assetConditionRepo.findAll();
    }
}
