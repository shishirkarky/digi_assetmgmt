package com.peepalsoft.app.service.settings;

import com.peepalsoft.app.entity.asset.groups.GroupFour;
import com.peepalsoft.app.entity.settings.AssetCondition;
import com.peepalsoft.app.entity.settings.Company;
import com.peepalsoft.app.entity.settings.FundSource;
import com.peepalsoft.app.entity.settings.ReceivedSource;
import com.peepalsoft.app.enums.CompanyTypeEnum;

import java.util.List;
import java.util.Optional;

public interface SettingsService {
    Optional<Company> companySave(Company company);

    List<Company> companyFindAll();

    List<Company> companyFindAllByCompanyType(CompanyTypeEnum companyType);

    Optional<Company> companyFindById(int id);

    Optional<FundSource> fundSourceSave(FundSource fundSource);

    List<FundSource> fundSourceFindAll();

    Optional<FundSource> fundSourceFindById(int id);

    Optional<ReceivedSource> saveReceivedSource(ReceivedSource receivedSource);

    List<ReceivedSource> findAllReceivedSource();

    Optional<GroupFour> saveGroupFour(GroupFour groupFour);

    List<GroupFour> findAllGroupFour();

    Optional<AssetCondition> assetConditionSave(AssetCondition assetCondition);

    List<AssetCondition> findAllAssetCondition();
}
