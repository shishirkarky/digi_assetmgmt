package com.peepalsoft.app.service.settings;

import com.peepalsoft.app.entity.settings.FiscalYear;

import java.util.List;
import java.util.Optional;

public interface FiscalYearService {
    List<FiscalYear> getAll();

    Optional<FiscalYear> save(FiscalYear fiscalYear);
}
