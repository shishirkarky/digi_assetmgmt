package com.peepalsoft.app.service.bill;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.peepalsoft.app.config.ErrorMessageConstants;
import com.peepalsoft.app.config.PeepalSoftConstants;
import com.peepalsoft.app.entity.bill.Bill;
import com.peepalsoft.app.exception.CustomValidationException;
import com.peepalsoft.app.repo.bill.BillRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class BillServiceImpl implements BillService {
    private BillRepo billRepo;

    @Autowired
    public void setBillRepo(BillRepo billRepo) {
        this.billRepo = billRepo;
    }

    @Override
    public boolean uploadBill(Optional<MultipartFile> file, int id) {
        if (!StringUtils.isEmpty(id) || id > 0) {
            if (file.isPresent()) {
                try {
                    Optional<Bill> billOptional = billRepo.findById(id);
                    if (billOptional.isPresent()) {
                        Bill bill = billOptional.get();

                        String uploadUrl = PeepalSoftConstants.BILL_UPLOAD_PATH.concat(PeepalSoftConstants.BILL_IMAGE_PREFIX).concat(String.valueOf(id)).concat(PeepalSoftConstants.UPLOAD_IMAGE_EXTENSION);
                        bill.setBillUrl(uploadUrl);

                        byte[] bytes = file.get().getBytes();
                        Path path = Paths.get(uploadUrl);
                        Files.write(path, bytes);

                        billRepo.save(bill);
                        return true;
                    }
                } catch (IOException e) {
                    log.error("Err...Exception\n" + e);
                }
            }
        }
        return false;
    }

    @Override
    public List<Bill> findAll() {
        return billRepo.findAll();
    }

    @Override
    public Bill save(Bill bill) {
        return billRepo.save(bill);
    }

    @Override
    public Bill saveBillWithFileUpload(Optional<MultipartFile> file, String billJson) {
        List<String> err = new ArrayList<>();
        try {
            Bill bill = new ObjectMapper().readValue(billJson, Bill.class);
            if (bill != null) {
                Bill savedBill = save(bill);
                boolean uploadStatus = uploadBill(file, savedBill.getId());
                if (uploadStatus) {
                    log.info("Bill upload successful.");
                } else {
                    log.info("Bill upload failure.");
                }
                return savedBill;
            } else {
                err.add(ErrorMessageConstants.INVALID_REQUEST);
            }
        } catch (Exception e) {
            err.add(ErrorMessageConstants.INVALID_REQUEST);
        }
        throw new CustomValidationException(err);
    }


    @Override
    public Optional<Bill> findById(int id) {
        return billRepo.findById(id);
    }
}
