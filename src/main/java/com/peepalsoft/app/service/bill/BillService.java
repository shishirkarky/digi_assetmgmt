package com.peepalsoft.app.service.bill;

import com.peepalsoft.app.entity.bill.Bill;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

public interface BillService {
    Bill saveBillWithFileUpload(Optional<MultipartFile> file, String billJson);
    boolean uploadBill(Optional<MultipartFile> file, int id);

    List<Bill> findAll();

    Bill save(Bill bill);

    Optional<Bill> findById(int id);
}
