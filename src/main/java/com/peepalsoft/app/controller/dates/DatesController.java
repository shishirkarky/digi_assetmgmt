/**
 * Description: (description of source file content)
 *
 * @author Shishir Karki
 * @version 1.0.0
 * <p>
 * <p>
 * Copyright (c) 2021, Facet Technology
 * <p>
 * All rights reserved.
 * This information contained herein may not be used in
 * whole or in part without the express written consent of
 * .
 */
package com.peepalsoft.app.controller.dates;

import com.peepalsoft.app.responsemessage.HttpResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("impl/api/v1")
public class DatesController {
    private DatesRepository datesRepository;
    private DateUtility dateUtility;

    @Autowired
    public void setDatesRepository(DatesRepository datesRepository) {
        this.datesRepository = datesRepository;
    }

    @Autowired
    public void setDateUtility(DateUtility dateUtility) {
        this.dateUtility = dateUtility;
    }

    @GetMapping(value = "getalldates")
    public ResponseEntity<?> getallDates(){
        List<Dates> datesList = datesRepository.findAll();
        return new ResponseEntity<>(datesList, HttpStatus.OK);
    }

    @GetMapping(value = "conversions/eton/{englishDate}")
    public ResponseEntity<?> convertEnglishToNepaliDate(@PathVariable String englishDate) {
        Dates dates = datesRepository.findByEnglishdate(englishDate);
        if (dates != null) {
            return new ResponseEntity<>(HttpResponses.fetched(dates.getNepalidate()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpResponses.notfound(), HttpStatus.OK);
    }

    @GetMapping(value = "conversions/ntoe/{nepaliDate}")
    public ResponseEntity<?> convertNepaliToEnglishDate(@PathVariable String nepaliDate) {
        Dates dates = datesRepository.findByNepalidate(nepaliDate);
        if (dates != null) {
            return new ResponseEntity<>(HttpResponses.fetched(dates.getEnglishdate()), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpResponses.notfound(), HttpStatus.OK);
    }

    @GetMapping(value = "/dates/todays/nepali")
    public ResponseEntity<?> getCurrentNepaliDate() {
        return new ResponseEntity<>(HttpResponses.fetched(dateUtility.todayNepaliDate()), HttpStatus.OK);
    }

    @GetMapping(value = "/dates/todays/english")
    public ResponseEntity<?> getCurrentEnglishDate() {
        return new ResponseEntity<>(HttpResponses.fetched(dateUtility.todayEnglishDate()), HttpStatus.OK);
    }

}
