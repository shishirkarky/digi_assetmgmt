/**
* Description: (description of source file content)
* @author  Shishir Karki
* @version  1.0.0
*
*
* Copyright (c) 2021, Facet Technology
*
* All rights reserved.
* This information contained herein may not be used in
* whole or in part without the express written consent of
* .
*
*/
package com.peepalsoft.app.controller.dates;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "md_dates")
@Getter
@Setter
public class Dates {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String englishdate;
    private String nepalidate;
}
