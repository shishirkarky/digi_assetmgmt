package com.peepalsoft.app.controller.asset;

import com.peepalsoft.app.config.ApiPaths;
import com.peepalsoft.app.config.ErrorMessageConstants;
import com.peepalsoft.app.config.PageTitleAndJspPathEnum;
import com.peepalsoft.app.dto.AssetsFormDto;
import com.peepalsoft.app.dto.AssetsTableDto;
import com.peepalsoft.app.entity.asset.AmcDetail;
import com.peepalsoft.app.entity.asset.InsuranceDetail;
import com.peepalsoft.app.entity.asset.TaxDetail;
import com.peepalsoft.app.entity.asset.WarrantyDetail;
import com.peepalsoft.app.entity.asset.coreentities.CoreAssetEntity;
import com.peepalsoft.app.enums.AssetTypeEnum;
import com.peepalsoft.app.enums.StatusEnum;
import com.peepalsoft.app.repo.assets.core.*;
import com.peepalsoft.app.responsemessage.HttpResponses;
import com.peepalsoft.app.responsemessage.ResponseMessage;
import com.peepalsoft.app.service.assets.AssetsService;
import com.peepalsoft.app.service.assets.SubAssetsService;
import com.peepalsoft.app.user.service.UsersService;
import com.peepalsoft.app.util.UserDetailsUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping(ApiPaths.ASSETS)
public class AssetController {
    private static final String PAGE_TITLE = "pagetitle";
    private UsersService usersService;
    private AssetsService assetsService;
    private CoreAssetEntityRepo coreAssetEntityRepo;
    private WarrantyDetailRepo warrantyDetailRepo;
    private InsuranceDetailRepo insuranceDetailRepo;
    private AmcDetailRepo amcDetailRepo;
    private TaxDetailRepo taxDetailRepo;
    private SubAssetsService subAssetsService;
    private UserDetailsUtil userDetailsUtil;

    @Autowired
    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    @Autowired
    public void setInsuranceDetailRepo(InsuranceDetailRepo insuranceDetailRepo) {
        this.insuranceDetailRepo = insuranceDetailRepo;
    }

    @Autowired
    public void setAmcDetailRepo(AmcDetailRepo amcDetailRepo) {
        this.amcDetailRepo = amcDetailRepo;
    }

    @Autowired
    public void setTaxDetailRepo(TaxDetailRepo taxDetailRepo) {
        this.taxDetailRepo = taxDetailRepo;
    }

    @Autowired
    public void setWarrantyDetailRepo(WarrantyDetailRepo warrantyDetailRepo) {
        this.warrantyDetailRepo = warrantyDetailRepo;
    }

    @Autowired
    public void setCoreAssetEntityRepo(CoreAssetEntityRepo coreAssetEntityRepo) {
        this.coreAssetEntityRepo = coreAssetEntityRepo;
    }

    @Autowired
    public void setAssetsService(AssetsService assetsService) {
        this.assetsService = assetsService;
    }

    @Autowired
    public void setSubAssetsService(SubAssetsService subAssetsService) {
        this.subAssetsService = subAssetsService;
    }

    @Autowired
    public void setUserDetailsUtil(UserDetailsUtil userDetailsUtil) {
        this.userDetailsUtil = userDetailsUtil;
    }

    /**
     * This method requires objects inside assetsFormDto and assetType (mandatory)
     * asset will be saved into category depending upon assetType
     *
     * @param assetsFormDto
     * @return
     */
    @PostMapping(value = ApiPaths.ASSETS_C)
    public final ResponseEntity<ResponseMessage> save(@RequestBody AssetsFormDto assetsFormDto, Principal principal) {
        if (assetsFormDto != null) {
            String principalUser = principal.getName();
            boolean validationStatus = assetsService.validateSaveAction(principalUser, assetsFormDto.getItemCode());
            if (validationStatus) {
                assetsFormDto.setPrincipalUsername(principalUser);
                Optional<AssetsFormDto> generatorUpsDetailOptional = assetsService.save(assetsFormDto);
                return generatorUpsDetailOptional.map(o -> new ResponseEntity<>(HttpResponses.created(o), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST));
            }
        }
        return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST);
    }

    @GetMapping(value = ApiPaths.ASSETS_R)
    public final ResponseEntity<?> getByBranchCode(Principal principal) {
        String branchCode = usersService.getBranchCode(principal.getName());
        List<StatusEnum> statuses = new ArrayList<>();
        statuses.add(StatusEnum.TRANSFER_PENDING);
        statuses.add(StatusEnum.DELETED);
        List<CoreAssetEntity> coreAssetEntity = coreAssetEntityRepo.findByRefBranchCodeAndStatusNotIn(branchCode, statuses);
        return new ResponseEntity<>(HttpResponses.fetched(coreAssetEntity), HttpStatus.OK);
    }

    @GetMapping(value = ApiPaths.SUB_ASSETS_R)
    public final ResponseEntity<?> getSubAssets(String itemCode, Principal principal) {
        if (null != itemCode && !itemCode.isEmpty()) {
            return new ResponseEntity<>(HttpResponses.fetched(
                    subAssetsService.getSubAssetsByItemCode(itemCode)
            ), HttpStatus.OK);
        } else {
            Map<String, String> map = userDetailsUtil.getStaffCodeAndBranchCode(principal.getName());

            return new ResponseEntity<>(HttpResponses.fetched(
                    subAssetsService.getAllSubAssetsForBranch(map.get("branchCode"))
            ), HttpStatus.OK);
        }
    }

    @GetMapping(value = ApiPaths.ASSETS_FIND_BY_ITEM_CODE)
    public final ResponseEntity<?> findByItemCode(@PathVariable String itemCode) {
        Optional<AssetsFormDto> assetDetails = assetsService.findByItemCode(itemCode);
        return assetDetails.map(assetsFormDto -> new ResponseEntity<>(HttpResponses.fetched(assetsFormDto), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpResponses.notfound(), HttpStatus.BAD_REQUEST));
    }

    @GetMapping(path = ApiPaths.ASSETS_FIND_BY_ASSET_TYPE)
    public ResponseEntity<?> findAllByAssetType(@PathVariable AssetTypeEnum assetType) {
        AssetsTableDto assets = assetsService.findByAssetType(assetType);
        if (assets != null) {
            return new ResponseEntity<>(HttpResponses.fetched(assets), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpResponses.notfound(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping(path = ApiPaths.ASSETS_WARRANTY)
    public ResponseEntity<?> saveWarranty(@RequestBody AssetsFormDto assetsFormDto, Principal principal) {
        if (!StringUtils.isEmpty(assetsFormDto.getItemCode())) {
            assetsFormDto.setPrincipalUsername(principal.getName());
            Optional<AssetsFormDto> warrantyDetail = assetsService.saveWarrantyDetail(assetsFormDto);
            if (warrantyDetail.isPresent()) {
                return new ResponseEntity<>(HttpResponses.created(warrantyDetail.get()), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST);
    }

    @GetMapping(path = ApiPaths.ASSETS_WARRANTY)
    public ResponseEntity<?> findWarrantyByItemCode(@RequestParam String itemCode) {
        Optional<WarrantyDetail> warrantyDetailOptional = warrantyDetailRepo.findByItemCode(itemCode);
        AssetsFormDto assetsFormDto = new AssetsFormDto();
        assetsFormDto.setItemCode(itemCode);
        warrantyDetailOptional.ifPresent(assetsFormDto::setWarrantyDetail);
        return new ResponseEntity<>(HttpResponses.fetched(assetsFormDto), HttpStatus.OK);
    }


    @PostMapping(path = ApiPaths.ASSETS_INSURANCE)
    public ResponseEntity<?> saveInsurance(@RequestBody AssetsFormDto assetsFormDto, Principal principal) {
        if (!StringUtils.isEmpty(assetsFormDto.getItemCode())) {
            assetsFormDto.setPrincipalUsername(principal.getName());
            Optional<AssetsFormDto> insuranceDetail = assetsService.saveInsurance(assetsFormDto);
            if (insuranceDetail.isPresent()) {
                return new ResponseEntity<>(HttpResponses.created(insuranceDetail.get()), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST);
    }

    @GetMapping(path = ApiPaths.ASSETS_INSURANCE)
    public ResponseEntity<?> findInsuranceByItemCode(@RequestParam String itemCode) {
        Optional<InsuranceDetail> insuranceDetailOptional = insuranceDetailRepo.findByItemCode(itemCode);
        AssetsFormDto assetsFormDto = new AssetsFormDto();
        assetsFormDto.setItemCode(itemCode);
        insuranceDetailOptional.ifPresent(assetsFormDto::setInsuranceDetail);
        return new ResponseEntity<>(HttpResponses.fetched(assetsFormDto), HttpStatus.OK);
    }

    @PostMapping(path = ApiPaths.ASSETS_AMC)
    public ResponseEntity<?> saveAmc(@RequestBody AssetsFormDto assetsFormDto, Principal principal) {
        if (!StringUtils.isEmpty(assetsFormDto.getItemCode())) {
            assetsFormDto.setPrincipalUsername(principal.getName());
            Optional<AssetsFormDto> amcDetail = assetsService.saveAmc(assetsFormDto);
            if (amcDetail.isPresent()) {
                return new ResponseEntity<>(HttpResponses.created(amcDetail.get()), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = ApiPaths.ASSETS_AMC)
    public ResponseEntity<?> findAmcByItemCode(@RequestParam String itemCode) {
        Optional<AmcDetail> amcDetailOptional = amcDetailRepo.findByItemCode(itemCode);
        AssetsFormDto assetsFormDto = new AssetsFormDto();
        assetsFormDto.setItemCode(itemCode);
        amcDetailOptional.ifPresent(assetsFormDto::setAmcDetail);
        return new ResponseEntity<>(HttpResponses.fetched(assetsFormDto), HttpStatus.OK);
    }

    @PostMapping(path = ApiPaths.ASSETS_TAX)
    public ResponseEntity<?> saveTax(@RequestBody AssetsFormDto assetsFormDto, Principal principal) {
        if (!StringUtils.isEmpty(assetsFormDto.getItemCode())) {
            assetsFormDto.setPrincipalUsername(principal.getName());
            Optional<AssetsFormDto> amcDetail = assetsService.saveTax(assetsFormDto);
            if (amcDetail.isPresent()) {
                return new ResponseEntity<>(HttpResponses.created(amcDetail.get()), HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = ApiPaths.ASSETS_TAX)
    public ResponseEntity<?> findTaxByItemCode(@RequestParam String itemCode) {
        Optional<TaxDetail> taxDetailOptional = taxDetailRepo.findByItemCode(itemCode);
        AssetsFormDto assetsFormDto = new AssetsFormDto();
        assetsFormDto.setItemCode(itemCode);
        taxDetailOptional.ifPresent(assetsFormDto::setTaxDetail);
        return new ResponseEntity<>(HttpResponses.fetched(assetsFormDto), HttpStatus.OK);
    }

    /* ROUTING TO DISPLAYING JSP PAGES
     */
    @GetMapping(path = ApiPaths.SUB_ASSETS_VIEW_PAGE)
    public ModelAndView subAssetsViewPage(String itemCode) {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.SUB_ASSETS_DETAIL_VIEW_PAGE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.SUB_ASSETS_DETAIL_VIEW_PAGE.pageTitle);
        model.addObject("itemCode", itemCode);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_GENERATOR_UPS_CREATE_PAGE)
    public ModelAndView generatorCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_GENERATOR_UPS_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_GENERATOR_UPS_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_HARDWARE_CREATE_PAGE)
    public ModelAndView hardwareCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_HARDWARE_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_HARDWARE_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_LAND_CREATE_PAGE)
    public ModelAndView landCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_LAND_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_LAND_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_LAND_AND_BUILDING_CREATE_PAGE)
    public ModelAndView landAndBuildingCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_LAND_BUILDING_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_LAND_BUILDING_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_MACHINERY_CREATE_PAGE)
    public ModelAndView machineryCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_MACHINERY_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_MACHINERY_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_MOTOR_VEHICLE_CREATE_PAGE)
    public ModelAndView motorVehicleCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_MOTOR_VEHICLE_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_MOTOR_VEHICLE_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_SOFTWARE_CREATE_PAGE)
    public ModelAndView softwareCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_SOFTWARE_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_SOFTWARE_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_OTHER_CREATE_PAGE)
    public ModelAndView otherCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_OTHER_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_OTHER_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_VIEW_PAGE)
    public ModelAndView assetsViewPage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_VIEW.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_VIEW.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_WARRANTY_CREATE_PAGE)
    public ModelAndView assetsWarrantyCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_OTHER_WARRANTY_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_OTHER_WARRANTY_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_INSURANCE_CREATE_PAGE)
    public ModelAndView assetsInsuranceCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_OTHER_INSURANCE_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_OTHER_INSURANCE_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_AMC_CREATE_PAGE)
    public ModelAndView assetsAmcCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_OTHER_AMC_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_OTHER_AMC_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_TAX_CREATE_PAGE)
    public ModelAndView assetsTaxCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_OTHER_TAX_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_OTHER_TAX_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_DASHBOARD_PAGE)
    public ModelAndView assetsDashboardPage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_DASHBOARD.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_DASHBOARD.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_DETAIL_VIEW_PAGE)
    public ModelAndView assetsDetailViewPage(Optional<AssetTypeEnum> assetType, Optional<String> itemCode) {
        ModelAndView model = null;
        if (assetType.isPresent() && !itemCode.isPresent()) {
            if (assetType.get().equals(AssetTypeEnum.MOTOR_VEHICLE)) {
                model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_MOTOR_VEHICLE_VIEW_PAGE.jspPath);
                model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_MOTOR_VEHICLE_VIEW_PAGE.pageTitle);
            } else if (assetType.get().equals(AssetTypeEnum.LAND)) {
                model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_LAND_VIEW_PAGE.jspPath);
                model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_LAND_VIEW_PAGE.pageTitle);
            } else if (assetType.get().equals(AssetTypeEnum.LAND_BUILDING)) {
                model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_LAND_BUILDING_VIEW_PAGE.jspPath);
                model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_LAND_BUILDING_VIEW_PAGE.pageTitle);
            } else {
                model = new ModelAndView(PageTitleAndJspPathEnum.ASSETS_DETAIL_VIEW_PAGE.jspPath);
                model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ASSETS_DETAIL_VIEW_PAGE.pageTitle);
                model.addObject("assetType", assetType.get());
            }
        } else if (itemCode.isPresent() && !assetType.isPresent()) {
            Optional<CoreAssetEntity> coreAssetEntityOptional = coreAssetEntityRepo.findByItemCode(itemCode.get());
            if (coreAssetEntityOptional.isPresent()) {
                AssetTypeEnum assetTypeEnum = coreAssetEntityOptional.get().getAssetType();
                switch (assetTypeEnum) {
                    case MOTOR_VEHICLE:
                        model = motorVehicleCreatePage();
                        break;
                    case LAND_BUILDING:
                        model = landAndBuildingCreatePage();
                        break;
                    case GENERATOR_UPS:
                        model = generatorCreatePage();
                        break;
                    case MACHINERY:
                        model = machineryCreatePage();
                        break;
                    case SOFTWARE:
                        model = softwareCreatePage();
                        break;
                    case HARDWARE:
                        model = hardwareCreatePage();
                        break;
                    case OTHER:
                        model = otherCreatePage();
                        break;
                    case LAND:
                        model = landCreatePage();
                        break;
                    default:
                        log.error(ErrorMessageConstants.INVALID_ASSET_TYPE);
                }
            }
            assert model != null;
            model.addObject("itemCode", itemCode.get());
        }
        return model;
    }

    @GetMapping(path = ApiPaths.ASSETS_COUNT)
    public ResponseEntity<?> getAssetsCount() {
        return new ResponseEntity<>(HttpResponses.fetched(assetsService.findAllAssetTypeCount()), HttpStatus.OK);
    }
}