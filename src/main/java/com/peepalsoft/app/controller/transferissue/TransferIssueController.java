package com.peepalsoft.app.controller.transferissue;

import com.peepalsoft.app.config.ApiPaths;
import com.peepalsoft.app.config.PageTitleAndJspPathEnum;
import com.peepalsoft.app.dto.IssueDto;
import com.peepalsoft.app.dto.TransferIssueDto;
import com.peepalsoft.app.entity.asset.SubAssets;
import com.peepalsoft.app.entity.asset.coreentities.CoreAssetEntity;
import com.peepalsoft.app.entity.asset.trackitem.ItemTransferIssue;
import com.peepalsoft.app.enums.StatusEnum;
import com.peepalsoft.app.exception.ValidationErrorException;
import com.peepalsoft.app.repo.assets.core.CoreAssetEntityRepo;
import com.peepalsoft.app.responsemessage.HttpResponses;
import com.peepalsoft.app.service.assets.AssetsService;
import com.peepalsoft.app.service.assets.SubAssetsService;
import com.peepalsoft.app.service.transferissue.TransferIssueService;
import com.peepalsoft.app.user.service.UsersService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping(ApiPaths.TRANSFER_ISSUE)
public class TransferIssueController {
    private static final String PAGE_TITLE = "pagetitle";
    private TransferIssueService transferIssueService;
    private UsersService usersService;
    private CoreAssetEntityRepo coreAssetEntityRepo;
    private AssetsService assetsService;
    private SubAssetsService subAssetsService;

    @Autowired
    public void setAssetsService(AssetsService assetsService) {
        this.assetsService = assetsService;
    }

    @Autowired
    public void setCoreAssetEntityRepo(CoreAssetEntityRepo coreAssetEntityRepo) {
        this.coreAssetEntityRepo = coreAssetEntityRepo;
    }

    @Autowired
    public void setUsersService(UsersService usersService) {
        this.usersService = usersService;
    }

    @Autowired
    public void setAssetsService(TransferIssueService transferIssueService) {
        this.transferIssueService = transferIssueService;
    }

    @Autowired
    public void setSubAssetsService(SubAssetsService subAssetsService) {
        this.subAssetsService = subAssetsService;
    }

    @GetMapping(ApiPaths.TRANSFER_CREATE_PAGE)
    public ModelAndView transfersCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.TRANSFER_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.TRANSFER_CREATE.pageTitle);
        return model;
    }

    @GetMapping(ApiPaths.ISSUE_CREATE_PAGE)
    public ModelAndView issuesCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.ISSUE_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.ISSUE_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.TRANSFER_ISSUE_VIEW_PAGE)
    public ModelAndView transferIssueViewPage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.TRANSFER_ISSUE_VIEW.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.TRANSFER_ISSUE_VIEW.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.PENDING_TRANSFER_ISSUE_VIEW_PAGE)
    public ModelAndView getPendingTransfersView() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.TRANSFER_PENDING_VIEW.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.TRANSFER_PENDING_VIEW.pageTitle);
        return model;
    }

    @PutMapping(ApiPaths.TRANSFER_ITEMS)
    public ResponseEntity<?> transferPending(@Validated @RequestBody TransferIssueDto transferIssueDto, BindingResult bindingResult, Principal principal) {
        if (bindingResult.hasErrors()) {
            throw new ValidationErrorException(bindingResult);
        }
        try {
            transferIssueService.makeItemsTransferPending(transferIssueDto, principal.getName());
            return new ResponseEntity<>(HttpResponses.created(transferIssueDto), HttpStatus.OK);
        } catch (Exception e) {
            log.info("Err..error in transfer issue\n" + e);
        }
        return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST);
    }

    @PutMapping(ApiPaths.ISSUE_ITEMS)
    public ResponseEntity<?> issue(@Validated @RequestBody IssueDto issueDto, BindingResult bindingResult, Principal principal) {
        if (bindingResult.hasErrors()) {
            throw new ValidationErrorException(bindingResult);
        }
        try {
            if (principal.getName() != null) {
                transferIssueService.issueItems(issueDto, principal.getName());
                return new ResponseEntity<>(HttpResponses.created(issueDto), HttpStatus.OK);
            }
        } catch (Exception e) {
            log.info("Err..error in issue\n" + e);
        }
        return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST);
    }

    @GetMapping
    public ResponseEntity<?> findAllByBranchCode(Principal principal) {
        String branchCode = usersService.getBranchCode(principal.getName());
        List<ItemTransferIssue> transferIssueList = transferIssueService.findTransferIssueLogByBranchCode(branchCode);
        if (!transferIssueList.isEmpty()) {
            return new ResponseEntity<>(HttpResponses.fetched(transferIssueList), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpResponses.fetched(new ArrayList<>()), HttpStatus.OK);
        }
    }

    @GetMapping(ApiPaths.PENDING_TRANSFER_ITEMS)
    public ResponseEntity<?> getPendingTransferItems(Principal principal) {
        String branchCode = usersService.getBranchCode(principal.getName());
        List<ItemTransferIssue> items = transferIssueService.findAllTransferIssueByBranchCodeAndStatus(branchCode, StatusEnum.TRANSFER_PENDING);
        if (!items.isEmpty()) {
            return new ResponseEntity<>(HttpResponses.fetched(items), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpResponses.fetched(new ArrayList<>()), HttpStatus.OK);
        }
    }

    @PatchMapping(ApiPaths.TRANSFER_APPROVE_ITEMS)
    public ResponseEntity<?> approveTransfer(@NotNull @PathVariable String itemCode, Principal principal) {
        Optional<CoreAssetEntity> coreAssetEntityOptional = assetsService.changeAssetStatus(itemCode, StatusEnum.TRANSFER_ACCEPTED, principal.getName());
        if (coreAssetEntityOptional.isPresent()) {
            return new ResponseEntity<>(HttpResponses.accepted(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST);
        }
    }

    @PatchMapping(ApiPaths.TRANSFER_DISAPPROVE_ITEMS)
    public ResponseEntity<?> disapproveTransfer(@NotNull @PathVariable String itemCode, Principal principal) {
        Optional<CoreAssetEntity> coreAssetEntityOptional = assetsService.changeAssetStatus(itemCode, StatusEnum.TRANSFER_DISAPPROVED, principal.getName());
        if (coreAssetEntityOptional.isPresent()) {
            return new ResponseEntity<>(HttpResponses.accepted(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST);
        }
    }
}
