package com.peepalsoft.app.controller;

import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;


@RestController
public class HomeController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ModelAndView index(Principal principal) {
        if (principal != null) {
            return dashboard();
        } else {
            return login("Please Login", "");
        }
    }

    //set url value to enter login page
    @RequestMapping(value = "/login-page", method = RequestMethod.GET)
    public ModelAndView login(String error, String logout) {
        ModelAndView model = new ModelAndView("login");
        if (!StringUtils.isEmpty(error)) {
            model.addObject("msg", error);
        }
        if (!StringUtils.isEmpty(logout)) {
            model.addObject("msg", logout);
        }
        return model;
    }

    @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
    public ModelAndView dashboard() {
        ModelAndView model = new ModelAndView("dashboard");
        model.addObject("pagetitle", "DASHBOARD");
        return model;
    }


}
