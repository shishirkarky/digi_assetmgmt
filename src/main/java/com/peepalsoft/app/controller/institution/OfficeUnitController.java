package com.peepalsoft.app.controller.institution;

import com.peepalsoft.app.assembler.OfficeUnitAssemblerService;
import com.peepalsoft.app.dto.OfficeUnitSelectDto;
import com.peepalsoft.app.entity.institution.OfficeUnit;
import com.peepalsoft.app.repo.OfficeUnitRepo;
import com.peepalsoft.app.responsemessage.HttpResponses;
import com.peepalsoft.app.util.ValidationComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("office-unit")
public class OfficeUnitController {
    private OfficeUnitRepo officeUnitRepo;
    private OfficeUnitAssemblerService officeUnitAssemblerService;

    @Autowired
    public void setOfficeUnitAssemblerService(OfficeUnitAssemblerService officeUnitAssemblerService) {
        this.officeUnitAssemblerService = officeUnitAssemblerService;
    }

    @Autowired
    public void setOfficeUnitRepo(OfficeUnitRepo officeUnitRepo) {
        this.officeUnitRepo = officeUnitRepo;
    }

    @GetMapping
    public ResponseEntity<?> findAll() {
        return new ResponseEntity<>(HttpResponses.fetched(officeUnitRepo.findAll()), HttpStatus.OK);
    }

    @GetMapping("/branches/{branchCode}/selects")
    public ResponseEntity<?> getOfficeUnits(@NotNull @PathVariable String branchCode) {
        List<OfficeUnit> officeUnits = officeUnitRepo.findByRefBranchCode(branchCode);
        List<OfficeUnitSelectDto> officeUnitSelectDtos = officeUnitAssemblerService.toSelectDtos(officeUnits);
        return new ResponseEntity<>(HttpResponses.fetched(officeUnitSelectDtos), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> save(@RequestBody @Validated OfficeUnit officeUnit, BindingResult bindingResult, Principal principal) {
        ValidationComponent.bindingResultValidation(bindingResult);
        if (principal.getName() != null) {
            officeUnit.setLastUpdatedBy(principal.getName());
            OfficeUnit savedOfficeUnit = officeUnitRepo.save(officeUnit);
            return new ResponseEntity<>(HttpResponses.created(savedOfficeUnit), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST);
        }
    }
}
