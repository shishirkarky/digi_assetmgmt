package com.peepalsoft.app.controller.bill;

import com.peepalsoft.app.config.ApiPaths;
import com.peepalsoft.app.config.ErrorMessageConstants;
import com.peepalsoft.app.config.PageTitleAndJspPathEnum;
import com.peepalsoft.app.entity.bill.Bill;
import com.peepalsoft.app.exception.CustomValidationException;
import com.peepalsoft.app.exception.ProductNotfoundException;
import com.peepalsoft.app.responsemessage.HttpResponses;
import com.peepalsoft.app.service.bill.BillService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping(ApiPaths.BILL)
public class BillController {
    private static final String PAGE_TITLE = "pagetitle";
    private BillService billService;

    @Autowired
    public void setBillService(BillService billService) {
        this.billService = billService;
    }

    @GetMapping(value = ApiPaths.BILL_R)
    public ResponseEntity<?> getAll() {
        return new ResponseEntity<>(HttpResponses.fetched(billService.findAll()), HttpStatus.OK);
    }

    @PostMapping(consumes = {"multipart/form-data"})
    public ResponseEntity<?> save(@RequestParam(value = "file") Optional<MultipartFile> file, @RequestParam(required = true, value = "billJson") String billJson) {
        List<String> err = new ArrayList<>();
        try {
            Bill savedBill = billService.saveBillWithFileUpload(file, billJson);
            if (savedBill != null) return new ResponseEntity<>(HttpResponses.created(savedBill), HttpStatus.OK);
            err.add(ErrorMessageConstants.UNABLE_TO_SAVE);
        } catch (Exception e) {
            log.error("Err...Bill Json invalid exception\n" + e);
            err.add(ErrorMessageConstants.INVALID_REQUEST);
        }
        throw new CustomValidationException(err);
    }

    @GetMapping
    public ResponseEntity<?> findById(int id) {
        List<String> err = new ArrayList<>();
        if (id > 0) {
            Optional<Bill> billOptional = billService.findById(id);
            if (billOptional.isPresent()) {
                return new ResponseEntity<>(HttpResponses.fetched(billOptional.get()), HttpStatus.OK);
            } else {
                throw new ProductNotfoundException();
            }
        } else {
            err.add(ErrorMessageConstants.INVALID_REQUEST);
            throw new CustomValidationException(err);
        }
    }

    @GetMapping(path = ApiPaths.BILL_CREATE_PAGE)
    public ModelAndView billCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.BILL_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.BILL_CREATE.pageTitle);
        return model;
    }

    @GetMapping(path = ApiPaths.BILL_VIEW_PAGE)
    public ModelAndView billViewPage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.BILL_VIEW.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.BILL_VIEW.pageTitle);
        return model;
    }
}
