package com.peepalsoft.app.controller.settings;

import com.peepalsoft.app.config.ApiPaths;
import com.peepalsoft.app.config.PageTitleAndJspPathEnum;
import com.peepalsoft.app.entity.settings.FiscalYear;
import com.peepalsoft.app.responsemessage.HttpResponses;
import com.peepalsoft.app.service.settings.FiscalYearService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;

@Slf4j
@Controller
@RequestMapping(ApiPaths.FISCAL_YEAR)
public class FiscalYearController {
    private static final String PAGE_TITLE = "pagetitle";
    @Autowired
    private FiscalYearService fiscalYearService;

    @GetMapping
    public final ResponseEntity<?> findAllFiscalYears() {
        return new ResponseEntity<>(HttpResponses.fetched(fiscalYearService.getAll()), HttpStatus.OK);
    }

    @PostMapping
    public final ResponseEntity<?> saveFiscalYears(@RequestBody FiscalYear fiscalYear) {
        Optional<FiscalYear> fiscalYearOptional = fiscalYearService.save(fiscalYear);
        return fiscalYearOptional.map(year -> new ResponseEntity<>(HttpResponses.created(year), HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.OK));
    }

    @GetMapping(path = ApiPaths.FISCAL_YEAR_CREATE_PAGE)
    public ModelAndView generatorCreatePage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.FISCAL_YEAR_CREATE.jspPath);
        model.addObject(PAGE_TITLE, PageTitleAndJspPathEnum.FISCAL_YEAR_CREATE.pageTitle);
        return model;
    }
}
