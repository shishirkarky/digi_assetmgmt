package com.peepalsoft.app.controller.settings;

import com.peepalsoft.app.component.CrudReturnService;
import com.peepalsoft.app.config.ApiPaths;
import com.peepalsoft.app.config.ErrorMessageConstants;
import com.peepalsoft.app.config.PageTitleAndJspPathEnum;
import com.peepalsoft.app.config.PeepalSoftConstants;
import com.peepalsoft.app.entity.asset.groups.GroupFour;
import com.peepalsoft.app.entity.settings.AssetCondition;
import com.peepalsoft.app.entity.settings.Company;
import com.peepalsoft.app.entity.settings.FundSource;
import com.peepalsoft.app.entity.settings.ReceivedSource;
import com.peepalsoft.app.enums.CompanyTypeEnum;
import com.peepalsoft.app.exception.CustomValidationException;
import com.peepalsoft.app.responsemessage.HttpResponses;
import com.peepalsoft.app.responsemessage.ResponseMessage;
import com.peepalsoft.app.service.settings.SettingsService;
import com.peepalsoft.app.util.ValidationComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping(ApiPaths.SETTINGS)
public class SettingsController {

    private SettingsService settingsService;
    private CrudReturnService<Object> crudReturnService;

    @Autowired
    public void setSettingsService(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    @PostMapping(value = ApiPaths.COMPANY_C)
    public final ResponseEntity<ResponseMessage> save(@RequestBody @Validated Company company, BindingResult bindingResult, Principal principal) {
        ValidationComponent.bindingResultValidation(bindingResult);
        company.setLastUpdatedBy(principal.getName());
        Optional<Company> companyOptional = settingsService.companySave(company);
        if (companyOptional.isPresent()) {
            return new ResponseEntity<>(HttpResponses.created(companyOptional.get()), HttpStatus.OK);
        } else {
            throw new CustomValidationException(Collections.singletonList(ErrorMessageConstants.UNABLE_TO_SAVE));
        }
    }

    @GetMapping(value = ApiPaths.COMPANY_R_BY_ID)
    public final ResponseEntity<Company> findById(int id) {
        Optional<Company> companyOptional = settingsService.companyFindById(id);
        return companyOptional.map(company -> new ResponseEntity<>(company, HttpStatus.OK)).orElseGet(()
                -> new ResponseEntity<>(new Company(), HttpStatus.NOT_FOUND));
    }

    @GetMapping(value = ApiPaths.COMPANY_R_BY_TYPE)
    public final ResponseEntity<?> findCompanySupplier(@PathVariable CompanyTypeEnum companyType) {
        List<Company> companyList = settingsService.companyFindAllByCompanyType(companyType);
        return new ResponseEntity<>(HttpResponses.fetched(companyList), HttpStatus.OK);
    }

    @GetMapping(value = ApiPaths.COMPANY_R_ALL)
    public final ResponseEntity<?> findAll() {
        return new ResponseEntity<>(HttpResponses.fetched(settingsService.companyFindAll()), HttpStatus.OK);
    }

    @PostMapping(value = ApiPaths.FUND_SOURCE_C)
    public final ResponseEntity<?> fundSourceSave(@RequestBody @Validated FundSource fundSource, BindingResult bindingResult, Principal principal) {
        ValidationComponent.bindingResultValidation(bindingResult);
        fundSource.setLastUpdatedBy(principal.getName());
        Optional<FundSource> fundSourceOptional = settingsService.fundSourceSave(fundSource);
        return fundSourceOptional.map(source -> new ResponseEntity<>(HttpResponses.created(source), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST));
    }

    @GetMapping(value = ApiPaths.FUND_SOURCE_R_ALL)
    public final ResponseEntity<?> fundSourceFindAll() {
        return new ResponseEntity<>(HttpResponses.fetched(settingsService.fundSourceFindAll()), HttpStatus.OK);
    }

    @GetMapping(value = ApiPaths.FUND_SOURCE_R_BY_ID)
    public final ResponseEntity<?> fundSourceFindById(int id) {
        Optional<FundSource> fundSourceOptional = settingsService.fundSourceFindById(id);
        return fundSourceOptional.map(fundSource -> new ResponseEntity<>(HttpResponses.fetched(fundSource), HttpStatus.OK)).orElseGet(()
                -> new ResponseEntity<>(HttpResponses.notfound(), HttpStatus.NOT_FOUND));
    }

    @PostMapping(value = ApiPaths.RECEIVED_SOURCE_C)
    public final ResponseEntity<?> saveReceivedSource(@RequestBody @Validated ReceivedSource receivedSource, BindingResult bindingResult, Principal principal) {
        ValidationComponent.bindingResultValidation(bindingResult);
        receivedSource.setLastUpdatedBy(principal.getName());
        receivedSource.setLastUpdatedBy(principal.getName());
        Optional<ReceivedSource> receivedSourceOptional = settingsService.saveReceivedSource(receivedSource);
        return receivedSourceOptional.map(source -> new ResponseEntity<>(HttpResponses.created(source), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST));
    }

    @GetMapping(value = ApiPaths.RECEIVED_SOURCE_R_ALL)
    public final ResponseEntity<?> findAllReceivedSource() {
        return new ResponseEntity<>(HttpResponses.fetched(settingsService.findAllReceivedSource()), HttpStatus.OK);
    }

    @PostMapping(value = ApiPaths.GROUP_C)
    public final ResponseEntity<?> saveGroup(@RequestBody @Validated GroupFour groupFour, BindingResult bindingResult, Principal principal) {
        ValidationComponent.bindingResultValidation(bindingResult);
        groupFour.setLastUpdatedBy(principal.getName());
        groupFour.setLastUpdatedBy(principal.getName());
        Optional<GroupFour> groupFourOptional = settingsService.saveGroupFour(groupFour);
        return groupFourOptional.map(source -> new ResponseEntity<>(HttpResponses.created(source), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST));
    }

    @GetMapping(value = ApiPaths.GROUP_R_ALL)
    public final ResponseEntity<?> findAllGroups() {
        return new ResponseEntity<>(HttpResponses.fetched(settingsService.findAllGroupFour()), HttpStatus.OK);
    }

    @PostMapping(value = ApiPaths.ASSET_CONDITION_C)
    public final ResponseEntity<?> saveAssetCondition(@RequestBody @Validated AssetCondition assetCondition, BindingResult bindingResult, Principal principal) {
        ValidationComponent.bindingResultValidation(bindingResult);
        assetCondition.setLastUpdatedBy(principal.getName());
        Optional<AssetCondition> assetConditionOptional = settingsService.assetConditionSave(assetCondition);
        return assetConditionOptional.map(source -> new ResponseEntity<>(HttpResponses.created(source), HttpStatus.OK)).orElseGet(() -> new ResponseEntity<>(HttpResponses.badRequest(), HttpStatus.BAD_REQUEST));
    }

    @GetMapping(value = ApiPaths.ASSET_CONDITION_R_ALL)
    public final ResponseEntity<?> findAllAssetCondition() {
        return new ResponseEntity<>(HttpResponses.fetched(settingsService.findAllAssetCondition()), HttpStatus.OK);
    }

    @GetMapping(path = ApiPaths.SETTINGS_CREATE_PAGE)
    public ModelAndView assetsViewPage() {
        ModelAndView model = new ModelAndView(PageTitleAndJspPathEnum.SETTINGS_CREATE.jspPath);
        model.addObject(PeepalSoftConstants.PAGE_TITLE, PageTitleAndJspPathEnum.SETTINGS_CREATE.pageTitle);
        return model;
    }

}
