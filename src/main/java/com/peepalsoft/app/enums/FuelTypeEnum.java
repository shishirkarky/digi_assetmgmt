package com.peepalsoft.app.enums;

public enum FuelTypeEnum {
    PETROL,
    DIESEL,
    OTHER
}
