package com.peepalsoft.app.enums;

public enum WheelTypeEnum {
    TWO_WHEEL,
    FOUR_WHEEL,
    OTHER
}
