package com.peepalsoft.app.enums;

public enum AreaUnitEnum {
    SQ_METER,
    BIGHA,
    ROPANI,
    SQ_FEET,
    OTHER
}
