package com.peepalsoft.app.enums;

public enum UsedStatusEnum {
    SELF_USED,
    RENTED,
    PUBLIC_CAPTURED,
    GOVERNMENT_CAPTURED,
    OTHER
}
