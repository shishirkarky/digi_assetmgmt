package com.peepalsoft.app.enums;

public enum AssetTypeEnum {
    LAND,
    LAND_BUILDING,
    GENERATOR_UPS,
    HARDWARE,
    MACHINERY,
    MOTOR_VEHICLE,
    SOFTWARE,
    OTHER
}
