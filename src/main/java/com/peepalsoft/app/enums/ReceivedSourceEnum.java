package com.peepalsoft.app.enums;

public enum ReceivedSourceEnum {
    OTHER,
    GRANT,
    PURCHASED
}
