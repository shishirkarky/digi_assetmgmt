package com.peepalsoft.app.enums;

public enum AssetCurrentConditionEnum {
    NEW,
    MAINTENANCE
}
