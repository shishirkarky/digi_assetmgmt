package com.peepalsoft.app.enums;

public enum VehicleTypeEnum {
    CAR,
    VAN,
    JEEP,
    MOTORCYCLE,
    TRUCK,
    CASH_VAN,
    OTHER
}
