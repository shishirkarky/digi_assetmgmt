package com.peepalsoft.app.enums;

public enum CompanyTypeEnum {
    SUPPLIER("SUPPLIER"),
    COMPANY("COMPANY");
    public String value;

    CompanyTypeEnum(String value) {
        this.value = value;
    }
}
