package com.peepalsoft.app.enums;

public enum ConstructionTypeEnum {
    MUDDY_AND_BRICK,
    FRAME_STRUCTURE,
    WALL_STRUCTURE,
    SHADE_STRUCTURE,
    OTHER
}
