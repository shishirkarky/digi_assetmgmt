package com.peepalsoft.app.enums;

import lombok.Getter;

@Getter
public enum StatusEnum {
    CREATED("CREATED"),
    UPDATED("UPDATED"),
    DELETED("DELETED"),
    TRANSFER_PENDING("TRANSFER_PENDING"),
    TRANSFER_ACCEPTED("TRANSFER_ACCEPTED"),
    TRANSFER_DISAPPROVED("TRANSFER_DISAPPROVED"),
    ISSUED("ISSUED");
    String value;

    StatusEnum(String value) {
        this.value = value;
    }
}
