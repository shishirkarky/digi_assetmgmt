package com.peepalsoft.app.enums;

public enum PurchaseTypeEnum {
    OTHER,
    PURCHASED,
    LEASED,
    GAIN_SHARING_BASIS,
    RENTED
}
