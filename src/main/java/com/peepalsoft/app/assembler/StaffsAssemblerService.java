package com.peepalsoft.app.assembler;

import com.peepalsoft.app.dto.StaffsSelectDto;
import com.peepalsoft.app.user.Staffs;

import java.util.List;

public interface StaffsAssemblerService {
    List<StaffsSelectDto> toSelectDtos(List<Staffs> staffs);
}
