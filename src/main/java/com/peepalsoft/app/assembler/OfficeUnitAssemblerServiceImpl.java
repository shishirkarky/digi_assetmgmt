package com.peepalsoft.app.assembler;

import com.peepalsoft.app.dto.OfficeUnitSelectDto;
import com.peepalsoft.app.entity.institution.OfficeUnit;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class OfficeUnitAssemblerServiceImpl implements OfficeUnitAssemblerService {
    @Override
    public List<OfficeUnitSelectDto> toSelectDtos(List<OfficeUnit> officeUnits) {
        List<OfficeUnitSelectDto> officeUnitSelectDtos = new ArrayList<>();
        officeUnits.forEach(officeUnit -> {
            OfficeUnitSelectDto officeUnitSelectDto = new OfficeUnitSelectDto();
            BeanUtils.copyProperties(officeUnit, officeUnitSelectDto);
            officeUnitSelectDtos.add(officeUnitSelectDto);
        });
        return officeUnitSelectDtos;
    }
}
