package com.peepalsoft.app.assembler;

import com.peepalsoft.app.dto.StaffsSelectDto;
import com.peepalsoft.app.user.Staffs;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class StaffsAssemblerServiceImpl implements StaffsAssemblerService{
    @Override
    public List<StaffsSelectDto> toSelectDtos(List<Staffs> staffs) {
        List<StaffsSelectDto> staffsSelectDtos = new ArrayList<>();
        staffs.forEach(staff -> {
            StaffsSelectDto staffsSelectDto = new StaffsSelectDto();
            staffsSelectDto.setCode(staff.getCode());
            staffsSelectDto.setFirstName(staff.getFirstName());
            staffsSelectDto.setLastName(staff.getLastName());
            staffsSelectDtos.add(staffsSelectDto);
        });
        return staffsSelectDtos;
    }
}
