package com.peepalsoft.app.assembler;

import com.peepalsoft.app.dto.OfficeUnitSelectDto;
import com.peepalsoft.app.entity.institution.OfficeUnit;

import java.util.List;

public interface OfficeUnitAssemblerService {
    List<OfficeUnitSelectDto> toSelectDtos(List<OfficeUnit> officeUnits);
}
